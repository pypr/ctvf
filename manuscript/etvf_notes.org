* Mathematical equations of continuous media

** General equations
** TVF addition


* SPH basics
** Derivative approximation
** Function approximation

* Discrete formulation of the governing equations in SPH


* PST

** SUN 2019 PST
** IPST
** Free surface Identification
** Free surface correction


* Differences between GTVF and ETVF


* Results

** Uniaxial compression
** Rings
** Oscillating plate
** High velocity impact
** Cantilever bending


* Conclusions
