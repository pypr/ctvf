#!/usr/bin/env python
import os
import matplotlib.pyplot as plt

from itertools import cycle, product
import json
from automan.api import PySPHProblem as Problem
from automan.api import Automator, Simulation, filter_by_name
from automan.jobs import free_cores
from pysph.solver.utils import load, get_files
from automan.api import (Automator, Simulation, filter_cases, filter_by_name)

import numpy as np
import matplotlib
matplotlib.use('agg')
from cycler import cycler
from matplotlib import rc, patches, colors
from matplotlib.collections import PatchCollection

rc('font', **{'family': 'Helvetica', 'size': 12})
rc('legend', fontsize='medium')
rc('axes', grid=True, linewidth=1.2)
rc('axes.grid', which='both', axis='both')
# rc('axes.formatter', limits=(1, 2), use_mathtext=True, min_exponent=1)
rc('grid', linewidth=0.5, linestyle='--')
rc('xtick', direction='in', top=True)
rc('ytick', direction='in', right=True)
rc('savefig', format='pdf', bbox='tight', pad_inches=0.05,
   transparent=False, dpi=300)
rc('lines', linewidth=1.5)
rc('axes', prop_cycle=(
    cycler('color', ['tab:blue', 'tab:green', 'tab:red',
                     'tab:orange', 'm', 'tab:purple',
                     'tab:pink', 'tab:gray']) +
    cycler('linestyle', ['-.', '--', '-', ':',
                         (0, (3, 1, 1, 1)), (0, (3, 1, 1, 1, 1, 1)),
                         (0, (3, 2, 1, 1)), (0, (3, 2, 2, 1, 1, 1)),
                         ])
))


# matplotlib.use('pdf')
n_core = 4
n_thread = 4 * 2
backend = ' --openmp '


def pretty(d, indent=0):
    for key, value in d.items():
        print('\t' * indent + str(key))
        if isinstance(value, dict):
            pretty(value, indent + 1)
        else:
            print('\t' * (indent + 1) + str(value))


def scheme_opts(params):
    if isinstance(params, tuple):
        return params[0]
    return params


def get_files_at_given_times(files, times):
    from pysph.solver.utils import load
    result = []
    count = 0
    for f in files:
        data = load(f)
        t = data['solver_data']['t']
        if count >= len(times):
            break
        if abs(t - times[count]) < t * 1e-8:
            result.append(f)
            count += 1
        elif t > times[count]:
            result.append(f)
            count += 1
    return result


def get_files_at_given_times_from_log(files, times, logfile):
    import re
    result = []
    time_pattern = r"output at time\ (\d+(?:\.\d+)?)"
    file_count, time_count = 0, 0
    with open(logfile, 'r') as f:
        for line in f:
            if time_count >= len(times):
                break
            t = re.findall(time_pattern, line)
            if t:
                if float(t[0]) in times:
                    result.append(files[file_count])
                    time_count += 1
                elif float(t[0]) > times[time_count]:
                    result.append(files[file_count])
                    time_count += 1
                file_count += 1
    return result


class UniaxialCompression(Problem):
    def get_name(self):
        return 'uniaxial_compression'

    def setup(self):
        get_path = self.input_path

        cmd = 'python code/uniaxial_compression.py' + backend

        # length = [1., 2., 3., 4.]
        # height = [0.1]
        # pfreq = 500

        # Base case info
        self.case_info = {
            'etvf_sun2019_dx_low': (dict(
                pst='sun2019',
                no_uhat_velgrad=None,
                uhat_cont=None,
                continuity_tvf_correction=None,
                shear_stress_tvf_correction=None,
                edac=None,
                no_surf_p_zero=None,
                dx=2e-3,
                pfreq=30), r'CTVF SPST, $\Delta x=2 \times 10^{-3}$ m'),

            'gtvf_dx_low': (dict(
                pst='gtvf',
                uhat_velgrad=None,
                uhat_cont=None,
                no_continuity_tvf_correction=None,
                no_shear_stress_tvf_correction=None,
                no_edac=None,
                no_surf_p_zero=None,
                dx=2e-3,
                pfreq=30), 'GTVF'),

            'etvf_sun2019_dx_medium': (dict(
                pst='sun2019',
                no_uhat_velgrad=None,
                uhat_cont=None,
                continuity_tvf_correction=None,
                shear_stress_tvf_correction=None,
                edac=None,
                no_surf_p_zero=None,
                dx=1e-3,
                pfreq=30), r'CTVF SPST, $\Delta x=1 \times 10^{-3}$ m'),

            'gtvf_dx_medium': (dict(
                pst='gtvf',
                uhat_velgrad=None,
                uhat_cont=None,
                no_continuity_tvf_correction=None,
                no_shear_stress_tvf_correction=None,
                no_edac=None,
                no_surf_p_zero=None,
                dx=1e-3,
                pfreq=30), 'GTVF'),

            'etvf_sun2019_dx_high': (dict(
                pst='sun2019',
                no_uhat_velgrad=None,
                uhat_cont=None,
                continuity_tvf_correction=None,
                shear_stress_tvf_correction=None,
                edac=None,
                no_surf_p_zero=None,
                dx=0.5 * 1e-3,
                pfreq=30), r'CTVF SPST, $\Delta x=5 \times 10^{-4}$ m'),

            'gtvf_dx_high': (dict(
                pst='gtvf',
                uhat_velgrad=None,
                uhat_cont=None,
                no_continuity_tvf_correction=None,
                no_shear_stress_tvf_correction=None,
                no_edac=None,
                no_surf_p_zero=None,
                dx=0.5 * 1e-3,
                pfreq=30), 'GTVF'),

            'etvf_sun2019_dx_very_high': (dict(
                pst='sun2019',
                no_uhat_velgrad=None,
                uhat_cont=None,
                continuity_tvf_correction=None,
                shear_stress_tvf_correction=None,
                edac=None,
                no_surf_p_zero=None,
                dx=0.25 * 1e-3,
                pfreq=30), r'CTVF SPST, $\Delta x=25 \times 10^{-5}$ m'),

            # 'gray': (dict(pst='gray',
            #               no_uhat_velgrad=None,
            #               no_uhat_cont=None,
            #               no_continuity_tvf_correction=None,
            #               no_shear_stress_tvf_correction=None,
            #               no_edac=None,
            #               no_surf_p_zero=None,
            #               pfreq=pfreq,
            #               material='steel',
            #               tf=tf,), 'GRAY'),
        }

        self.cases = [
            Simulation(get_path(name), cmd,
                       job_info=dict(n_core=n_core,
                                     n_thread=n_thread), cache_nnps=None,
                       **scheme_opts(self.case_info[name][0]))
            for name in self.case_info
        ]

    def run(self):
        self.make_output_dir()
        self.plot_von_mises()

    def plot_von_mises(self):
        rc('axes', prop_cycle=(
            cycler('color', ['tab:blue', 'tab:green', 'tab:purple',
                             'tab:orange', 'm', 'tab:red']) +
            cycler('linestyle', ['-.', '--', '-', '-', '-', '-']) +
            cycler('marker', ['None', 'None','None', 's', 's', 's'])
        ))
        import matplotlib.pyplot as plt

        data = {}
        for name in self.case_info:
            data[name] = np.load(self.input_path(name, 'results.npz'))

        # =======================================
        # Low resolution figure
        # =======================================
        plt.figure(figsize=(8, 7))

        name = 'gtvf_dx_medium'
        t_sph_A = data[name]['t_sph_A']
        t_fem_A = data[name]['t_fem_A']
        von_mises_sph_A = data[name]['von_mises_sph_A']
        von_mises_fem_A = data[name]['von_mises_fem_A']
        plt.plot(t_fem_A, von_mises_fem_A, #"--g",
                 label='FEM')
        plt.plot(t_sph_A, von_mises_sph_A, #"--b",
                 label='SPH Das')

        # GTVF results
        t_current = data[name]['t_current']
        von_mises_A_current = data[name]['von_mises_A_current']

        plt.plot(t_current, von_mises_A_current, #"-sc",
                 label=self.case_info[name][1] + r" $\Delta x=1 \times 10^{-3}$ m")

        i = 0
        color = ['r', 'y', 'k']
        for name in self.case_info:
            if self.case_info[name][0]['dx'] == 0.5e-3 and self.case_info[name][0]['pst'] == 'sun2019':
                t_current = data[name]['t_current']
                von_mises_A_current = data[name]['von_mises_A_current']

                plt.plot(t_current, von_mises_A_current, #"-s"+color[i],
                         label=self.case_info[name][1])
                i = i + 1

            if self.case_info[name][0]['dx'] == 1e-3 and self.case_info[name][0]['pst'] == 'sun2019':
                t_current = data[name]['t_current']
                von_mises_A_current = data[name]['von_mises_A_current']

                plt.plot(t_current, von_mises_A_current, #"-s"+color[i],
                         label=self.case_info[name][1])
                i = i + 1

            if self.case_info[name][0]['dx'] == 2e-3 and self.case_info[name][0]['pst'] == 'sun2019':
                t_current = data[name]['t_current']
                von_mises_A_current = data[name]['von_mises_A_current']

                plt.plot(t_current, von_mises_A_current, #"-s"+color[i],
                         label=self.case_info[name][1])

                i = i + 1

        plt.xlabel('time (ms)')
        plt.ylabel('von Mises')
        plt.legend(loc='upper left')
        # plt.tight_layout(pad=0)
        plt.savefig(self.output_path('von_mises_A.pdf'))
        plt.clf()
        plt.close()


class CantileverBending(Problem):
    def get_name(self):
        return 'cantilever_bending'

    def setup(self):
        get_path = self.input_path

        cmd = 'python code/cantilever_bending.py' + backend

        pfreq = 100
        tf = 10e-03

        self.cases = [
            Simulation(
                get_path('deflection_0_005'),
                cmd,
                job_info=dict(n_core=n_core, n_thread=n_thread),
                cache_nnps=None,
                # my arguments
                pst='sun2019',
                no_uhat_velgrad=None,
                no_shear_stress_tvf_correction=None,
                no_edac=None,
                no_surf_p_zero=None,
                uhat_cont=None,
                continuity_tvf_correction=None,
                material='steel',
                deflection=0.005,
                pfreq=pfreq,
                tf=tf),

            Simulation(
                get_path('deflection_1e_minus_4'),
                cmd,
                job_info=dict(n_core=n_core, n_thread=n_thread),
                cache_nnps=None,
                # my arguments
                pst='sun2019',
                no_uhat_velgrad=None,
                no_shear_stress_tvf_correction=None,
                no_edac=None,
                no_surf_p_zero=None,
                uhat_cont=None,
                continuity_tvf_correction=None,
                material='steel',
                deflection=1e-4,
                pfreq=pfreq,
                tf=tf)
        ]

    def run(self):
        self.make_output_dir()
        # self.plot_deflection()
        self.plot_particles()

    def plot_particles(self):
        import matplotlib.pyplot as plt

        data = {}
        for case in self.cases:
            # import pudb
            # pudb.set_trace()
            name = case.name
            files = get_files(self.input_path(name))
            data = load(files[-1])
            pa = data['arrays']['plate']
            tf = data['solver_data']['t']

            plt.figure()
            plt.scatter(pa.x, pa.y, c=pa.y, s=1)
            plt.colorbar()

            plt.savefig(self.output_path(name+'_particles.pdf'))
            plt.clf()
            plt.close()


class Rings(Problem):
    def get_name(self):
        return 'rings'

    def setup(self):
        get_path = self.input_path

        cmd = 'python code/rings.py' + backend

        pfreq = 300
        tf = 0.016

        poisson_ratio_values = [0.3975, 0.47]

        # Base case info
        _case_info = {
            'etvf_sun2019': (dict(
                pst='sun2019',
                no_uhat_velgrad=None,
                shear_stress_tvf_correction=None,
                edac=None,
                no_surf_p_zero=None,
                uhat_cont=None,
                continuity_tvf_correction=None,
                pfreq=pfreq,
                tf=tf,
            ), 'ETVF SUN2019 PST'),

            'etvf_ipst': (dict(
                pst='ipst',
                no_uhat_velgrad=None,
                shear_stress_tvf_correction=None,
                edac=None,
                no_surf_p_zero=None,
                uhat_cont=None,
                continuity_tvf_correction=None,
                ipst_min_iterations=5,
                ipst_max_iterations=10,
                ipst_interval=1,
                pfreq=pfreq,
                tf=tf,
            ), 'ETVF IPST PST'),

            'gtvf': (dict(
                pst='gtvf',
                uhat_velgrad=None,
                no_shear_stress_tvf_correction=None,
                no_edac=None,
                no_surf_p_zero=None,
                uhat_cont=None,
                no_continuity_tvf_correction=None,
                pfreq=pfreq,
                tf=tf,
            ), 'GTVF'),

            'gray_alpha_1': (dict(
                pst='gray',
                no_uhat_velgrad=None,
                no_shear_stress_tvf_correction=None,
                no_edac=None,
                no_surf_p_zero=None,
                no_uhat_cont=None,
                no_continuity_tvf_correction=None,
                artificial_vis_alpha=1.,
                pfreq=pfreq,
                tf=tf,
            ), 'GRAY'),

            'gray_alpha_2': (dict(
                pst='gray',
                no_uhat_velgrad=None,
                no_shear_stress_tvf_correction=None,
                no_edac=None,
                no_surf_p_zero=None,
                no_uhat_cont=None,
                artificial_vis_alpha=2.,
                no_continuity_tvf_correction=None,
                pfreq=pfreq,
                tf=tf,
            ), 'GRAY'),

            'gray_alpha_0_5': (dict(
                pst='gray',
                no_uhat_velgrad=None,
                no_shear_stress_tvf_correction=None,
                no_edac=None,
                no_surf_p_zero=None,
                no_uhat_cont=None,
                artificial_vis_alpha=0.5,
                no_continuity_tvf_correction=None,
                pfreq=pfreq,
                tf=tf,
            ), 'GRAY')
        }

        # Main case info
        self.case_info = {
            f'{s}_poisson_ratio_{i}': dict(poisson_ratio=i, kernel_choice=1,
                                           **_case_info[s][0])
            for s, i in product(_case_info, poisson_ratio_values)
        }
        # print(self.case_info)

        self.labels = {
            f'{s}_poisson_ratio_{i}': _case_info[s][1]
            for s, i in product(_case_info, poisson_ratio_values)
        }

        self.cases = [
            Simulation(get_path(name), cmd,
                       job_info=dict(n_core=n_core,
                                     n_thread=n_thread), cache_nnps=None,
                       **scheme_opts(self.case_info[name]))
            for name in self.case_info
        ]

    def run(self):
        self.make_output_dir()
        self.move_figures()
        self.plot_A_and_B()

    def plot_A_and_B(self):
        import matplotlib.pyplot as plt

        data = {}
        for name in self.case_info:
            data[name] = np.load(self.input_path(name, 'results.npz'))

        # first get the data from Gray...
        x_A_gray = data['gray_alpha_1_poisson_ratio_0.3975']['x_A_2299']
        y_A_gray = data['gray_alpha_1_poisson_ratio_0.3975']['y_A_2299']

        plt.figure(figsize=(6, 5))

        # ETVF sun 0.3975 data
        name = 'etvf_sun2019_poisson_ratio_0.3975'
        t = data[name]['t']
        x_A_sun = data[name]['x_A_2299']
        y_A_sun = data[name]['y_A_2299']

        # ETVF sun 0.3975 data
        name = 'etvf_ipst_poisson_ratio_0.3975'
        t = data[name]['t']
        x_A_ipst = data[name]['x_A_2299']
        y_A_ipst = data[name]['y_A_2299']
        plt.plot(t, x_A_sun, "-s", label=r'$x_A$ SPST')
        plt.plot(t, x_A_ipst, "-s", label=r'$x_A$ IPST')
        plt.plot(t, x_A_gray, "-", label=r'$x_A$ GRAY')

        plt.xlabel('time')
        # plt.ylabel('von Mises')
        plt.legend()
        # plt.tight_layout(pad=0)
        plt.savefig(self.output_path('sun_vs_ipst_vs_gray_a_b_0_3975_x.pdf'))
        plt.clf()
        plt.close()

        plt.figure(figsize=(6, 5))
        plt.plot(t, y_A_sun, "-s", label=r'$y_A$ SPST')
        plt.plot(t, y_A_ipst, "-s", label=r'$y_A$ IPST')
        plt.plot(t, y_A_gray, "-", label=r'$y_A$ GRAY')
        plt.ylim((-0.1, 0.1))
        plt.xlabel('time')
        plt.legend()
        plt.savefig(self.output_path('sun_vs_ipst_vs_gray_a_b_0_3975_y.pdf'))
        plt.clf()
        plt.close()

    def move_figures(self):
        import shutil
        import os

        for name in self.case_info:
            source = self.input_path(name)

            target_dir = "manuscript/figures/" + source[8:] + "/"
            os.makedirs(target_dir)
            # print(target_dir)

            file_names = os.listdir(source)

            for file_name in file_names:
                # print(file_name)
                if file_name.endswith((".jpg", ".pdf", ".png")):
                    # print(target_dir)
                    shutil.copy(os.path.join(source, file_name), target_dir)


class OscillatingPlate(Problem):
    def get_name(self):
        return 'oscillating_plate'

    def setup(self):
        get_path = self.input_path

        cmd = 'python code/oscillating_plate.py' + backend

        pfreq = 500

        # Tensile instability cases
        _cases_1 = {
            # this case is to show that the basic SPH equations result in an
            # artificial fracture of the oscillating plate at maximum tensile
            # stress
            'sph_gtvf_sun2019': (dict(
                pst='sun2019',
                uhat_velgrad=None,
                no_shear_stress_tvf_correction=None,
                no_edac=None,
                no_surf_p_zero=None,
                uhat_cont=None,
                no_continuity_tvf_correction=None,
                pfreq=1000,
                tf=0.1,
            ), 'SPH GTVF SUN2019'),
            'etvf_sun2019_length_0_2_height_0_02_time_0_22': (dict(
                pst='sun2019',
                no_uhat_velgrad=None,
                shear_stress_tvf_correction=None,
                edac=None,
                no_surf_p_zero=None,
                uhat_cont=None,
                continuity_tvf_correction=None,
                ipst_min_iterations=5,
                ipst_max_iterations=10,
                ipst_interval=1,
                length=0.2,
                height=0.02,
                N=10,
                pfreq=1000,
                tf=0.22,
            ), 'ETVF Sun2019 PST NoEDAC length 0.2 height 0.02 Time 0.22'),
            'etvf_sun2019_length_0_2_height_0_01_time_0_51': (dict(
                pst='sun2019',
                no_uhat_velgrad=None,
                shear_stress_tvf_correction=None,
                edac=None,
                no_surf_p_zero=None,
                uhat_cont=None,
                continuity_tvf_correction=None,
                ipst_min_iterations=5,
                ipst_max_iterations=10,
                ipst_interval=1,
                length=0.2,
                height=0.01,
                N=10,
                pfreq=1000,
                tf=0.51,
            ), 'ETVF Sun2019 PST NoEDAC length 0.2 height 0.01 Time 0.51'),

            'etvf_sun2019_length_0_2_height_0_02_N_10': (dict(
                pst='sun2019',
                no_uhat_velgrad=None,
                shear_stress_tvf_correction=None,
                edac=None,
                no_surf_p_zero=None,
                uhat_cont=None,
                continuity_tvf_correction=None,
                ipst_min_iterations=5,
                ipst_max_iterations=10,
                ipst_interval=1,
                length=0.2,
                height=0.02,
                N=10,
                pfreq=500,
                tf=1.,
            ), 'Sun2019 N=10'),

            'etvf_sun2019_length_0_2_height_0_02_N_20': (dict(
                pst='sun2019',
                no_uhat_velgrad=None,
                shear_stress_tvf_correction=None,
                edac=None,
                no_surf_p_zero=None,
                uhat_cont=None,
                continuity_tvf_correction=None,
                ipst_min_iterations=5,
                ipst_max_iterations=10,
                ipst_interval=1,
                length=0.2,
                height=0.02,
                N=20,
                pfreq=500,
                tf=1.,
            ), 'Sun2019 N=20'),

            # 'etvf_sun2019_length_0_2_height_0_02_N_30': (dict(
            #     pst='sun2019',
            #     no_uhat_velgrad=None,
            #     shear_stress_tvf_correction=None,
            #     edac=None,
            #     no_surf_p_zero=None,
            #     uhat_cont=None,
            #     continuity_tvf_correction=None,
            #     ipst_min_iterations=5,
            #     ipst_max_iterations=10,
            #     ipst_interval=1,
            #     length=0.2,
            #     height=0.02,
            #     N=30,
            #     pfreq=500,
            #     tf=1.,
            # ), 'Sun2019 N=30'),

            'etvf_ipst_length_0_2_height_0_02_N_10': (dict(
                pst='ipst',
                no_uhat_velgrad=None,
                shear_stress_tvf_correction=None,
                edac=None,
                no_surf_p_zero=None,
                uhat_cont=None,
                continuity_tvf_correction=None,
                ipst_min_iterations=5,
                ipst_max_iterations=10,
                ipst_interval=1,
                length=0.2,
                height=0.02,
                N=10,
                pfreq=500,
                tf=1.,
            ), 'IPST N=10'),

            'etvf_ipst_length_0_2_height_0_02_N_20': (dict(
                pst='ipst',
                no_uhat_velgrad=None,
                shear_stress_tvf_correction=None,
                edac=None,
                no_surf_p_zero=None,
                uhat_cont=None,
                continuity_tvf_correction=None,
                ipst_min_iterations=5,
                ipst_max_iterations=10,
                ipst_interval=1,
                length=0.2,
                height=0.02,
                N=20,
                pfreq=500,
                tf=1.,
            ), 'IPST N=20'),

            'etvf_ipst_length_0_2_height_0_02_N_30': (dict(
                pst='ipst',
                no_uhat_velgrad=None,
                shear_stress_tvf_correction=None,
                edac=None,
                no_surf_p_zero=None,
                uhat_cont=None,
                continuity_tvf_correction=None,
                ipst_min_iterations=5,
                ipst_max_iterations=10,
                ipst_interval=1,
                length=0.2,
                height=0.02,
                N=30,
                pfreq=500,
                tf=1.,
            ), 'IPST N=30'),

            # 'gtvf': (dict(pst='gtvf',
            #               uhat_velgrad=None,
            #               uhat_cont=None,
            #               no_continuity_tvf_correction=None,
            #               no_shear_stress_tvf_correction=None,
            #               no_edac=None,
            #               no_surf_p_zero=None,
            #               pfreq=pfreq,
            #               material='steel',
            #               tf=tf,), 'GTVF'),
            # 'gray': (dict(pst='gray',
            #               no_uhat_velgrad=None,
            #               no_uhat_cont=None,
            #               no_continuity_tvf_correction=None,
            #               no_shear_stress_tvf_correction=None,
            #               no_edac=None,
            #               no_surf_p_zero=None,
            #               pfreq=pfreq,
            #               material='steel',
            #               tf=tf,), 'GRAY'),
        }

        # Main case info
        self.case_info_1 = {
            f'{s}': dict(kernel_choice=1, **_cases_1[s][0])
            for s in _cases_1
        }

        self.labels_1 = {f'{s}': _cases_1[s][1] for s in _cases_1}

        # Base case info
        _case_info = {
            'etvf_sun2019_height_0_02_tf_0_35': (dict(
                pst='sun2019',
                no_uhat_velgrad=None,
                shear_stress_tvf_correction=None,
                edac=None,
                no_surf_p_zero=None,
                uhat_cont=None,
                continuity_tvf_correction=None,
                N=10,
                pfreq=pfreq,
                height=0.02,
                tf=0.35,
            ), 'ETVF SUN2019 PST Height 0.02'),
            'etvf_sun2019_height_0_01_tf_0_6': (dict(
                pst='sun2019',
                no_uhat_velgrad=None,
                shear_stress_tvf_correction=None,
                edac=None,
                no_surf_p_zero=None,
                uhat_cont=None,
                continuity_tvf_correction=None,
                N=10,
                pfreq=pfreq,
                height=0.01,
                tf=0.6,
            ), 'ETVF SUN2019 PST Height 0.01'),
        }

        vf_values = [0.001, 0.01, 0.03, 0.05]

        # Main case info
        self.case_info = {
            f'{s}_vf_{i}_length_0_2': dict(Vf=i, kernel_choice=1, length=0.2,
                                           **_case_info[s][0])
            for s, i in product(_case_info, vf_values)
        }

        self.labels = {
            f'{s}_vf_{i}_length_0_2': _case_info[s][1]
            for s, i in product(_case_info, vf_values)
        }

        self.case_info = {**self.case_info, **self.case_info_1}
        self.labels = {**self.labels, **self.labels_1}

        self.cases = [
            Simulation(get_path(name), cmd,
                       job_info=dict(n_core=n_core,
                                     n_thread=n_thread), cache_nnps=None,
                       **scheme_opts(self.case_info[name]))
            for name in self.case_info
        ]

    def run(self):
        self.make_output_dir()
        self.plot_particles()

    def plot_particles(self):

        for name in self.case_info:
            if name == "sph_gtvf_sun2019":
                source = self.input_path(name)
                files = get_files(source)
                # print(files)
                data = load(files[5])
                arrays = data['arrays']
                plate = arrays['plate']
                wall = arrays['wall']

                plt.figure()
                plt.scatter(wall.x, wall.y, c=wall.m, s=0.1)
                plt.scatter(plate.x, plate.y, c=plate.sigma00, s=0.1)
                plt.grid()
                plt.axes().set_aspect('equal')
                plt.colorbar(format='%.0e')

                plt.savefig(self.output_path('gtvf_sun2019.pdf'))
                plt.clf()
                plt.close()

            if name == "etvf_sun2019_length_0_2_height_0_02_time_0_22":
                source = self.input_path(name)
                files = get_files(source)
                data = load(files[-1])
                arrays = data['arrays']
                plate = arrays['plate']
                wall = arrays['wall']

                plt.figure()
                plt.scatter(wall.x, wall.y, c=wall.m, s=0.1)
                plt.scatter(plate.x, plate.y, c=plate.sigma00, s=0.1)
                plt.grid()
                plt.ylim(min(plate.y), -min(plate.y))
                plt.axes().set_aspect('equal')
                plt.colorbar(format='%.0e')
                plt.savefig(self.output_path('etvf_sun2019_l_0_2_h_0_02.pdf'))
                plt.clf()
                plt.close()

            if name == "etvf_sun2019_length_0_2_height_0_01_time_0_51":
                source = self.input_path(name)
                files = get_files(source)
                # print(files)
                data = load(files[-1])
                arrays = data['arrays']
                plate = arrays['plate']
                wall = arrays['wall']

                plt.figure()
                plt.scatter(wall.x, wall.y, c=wall.m, s=0.1)
                plt.scatter(plate.x, plate.y, c=plate.sigma00, s=0.1)
                plt.grid()
                plt.ylim(min(plate.y), -min(plate.y))
                plt.axes().set_aspect('equal')
                plt.colorbar(format='%.0e')

                plt.savefig(self.output_path('etvf_sun2019_l_0_2_h_0_01.pdf'))
                plt.clf()
                plt.close()

            # Convergence plots
            plt.figure()

            name = "etvf_ipst_length_0_2_height_0_02_N_10"
            data = np.load(self.input_path(name, 'results.npz'))
            plt.plot(data['t'], data['amplitude'], label=f'N=10')

            name = "etvf_ipst_length_0_2_height_0_02_N_20"
            data = np.load(self.input_path(name, 'results.npz'))
            plt.plot(data['t'], data['amplitude'], label=f'N=20')

            name = "etvf_ipst_length_0_2_height_0_02_N_30"
            data = np.load(self.input_path(name, 'results.npz'))
            plt.plot(data['t'], data['amplitude'], label=f'N=30')

            plt.legend()
            plt.xlabel("Time (seconds)")
            plt.ylabel("Amplitude")

            plt.savefig(self.output_path('ipst_convergence_plot.pdf'))
            plt.clf()
            plt.close()


class HighVelocityImpact(Problem):
    def get_name(self):
        return 'high_velocity_impact'

    def setup(self):
        get_path = self.input_path

        cmd = 'python code/hvi.py' + backend

        # length = [1., 2., 3., 4.]
        # height = [0.1]
        pfreq = 2**32

        # Base case info
        _case_info = {
            'etvf_sun2019': (dict(
                pst='sun2019',
                no_uhat_velgrad=None,
                uhat_cont=None,
                continuity_tvf_correction=None,
                shear_stress_tvf_correction=None,
                no_edac=None,
                no_surf_p_zero=None,
                pfreq=pfreq,
            ), 'ETVF SUN2019 PST'),
            'etvf_ipst': (dict(
                pst='ipst',
                no_uhat_velgrad=None,
                uhat_cont=None,
                continuity_tvf_correction=None,
                shear_stress_tvf_correction=None,
                no_edac=None,
                no_surf_p_zero=None,
                ipst_min_iterations=5,
                ipst_max_iterations=10,
                ipst_interval=1,
                pfreq=pfreq,
            ), 'ETVF ipst PST'),
            # 'gtvf': (dict(
            #     pst='gtvf',
            #     uhat_velgrad=None,
            #     uhat_cont=None,
            #     no_continuity_tvf_correction=None,
            #     no_shear_stress_tvf_correction=None,
            #     no_edac=None,
            #     no_surf_p_zero=None,
            #     pfreq=pfreq
            # ), 'GTVF')
        }

        # Main case info
        self.case_info = {
            f'{s}': dict(kernel_choice=1, **_case_info[s][0])
            for s in _case_info
        }
        # print(self.case_info)

        self.labels = {f'{s}': _case_info[s][1] for s in _case_info}

        self.cases = [
            Simulation(get_path(name), cmd,
                       job_info=dict(n_core=n_core,
                                     n_thread=n_thread), cache_nnps=None,
                       **scheme_opts(self.case_info[name]))
            for name in self.case_info
        ]

    def run(self):
        self.make_output_dir()
        self.move_figures()

    def move_figures(self):
        import shutil
        import os

        for name in self.case_info:
            source = self.input_path(name)

            target_dir = "manuscript/figures/" + source[8:] + "/"
            os.makedirs(target_dir)
            # print(target_dir)

            file_names = os.listdir(source)

            for file_name in file_names:
                # print(file_name)
                if file_name.endswith((".jpg", ".pdf", ".png")):
                    # print(target_dir)
                    shutil.copy(os.path.join(source, file_name), target_dir)


class Cavity(Problem):
    def get_name(self):
        return 'cavity'

    def setup(self):
        get_path = self.input_path

        cmd = 'python code/lid_driven_cavity.py' + backend

        # length = [1., 2., 3., 4.]
        # height = [0.1]
        # pfreq = 500

        self.re = [100, 1000]
        # Base case info
        _case_info = {
            'etvf_sun2019_re_100_nx_50': (dict(
                integrator="gtvf",
                internal_flow=None,
                pst='sun2019',
                edac=None,
                summation=None,
                re=100,
                nx=50,
                tf=25,
                pfreq=300,
            ), 'CTVF SPST 50 x 50'),

            'etvf_sun2019_re_100_nx_100': (dict(
                integrator="gtvf",
                internal_flow=None,
                pst='sun2019',
                edac=None,
                summation=None,
                re=100,
                nx=100,
                tf=25,
                pfreq=300,
            ), 'CTVF SPST 100 x 100'),

            'etvf_sun2019_re_100_nx_150': (dict(
                integrator="gtvf",
                internal_flow=None,
                pst='sun2019',
                edac=None,
                summation=None,
                re=100,
                nx=150,
                tf=25,
                pfreq=300,
            ), 'CTVF SPST 150 x 150'),

            'etvf_sun2019_re_1000_nx_50': (dict(
                integrator="gtvf",
                internal_flow=None,
                pst='sun2019',
                edac=None,
                summation=None,
                re=1000,
                nx=50,
                tf=60,
                pfreq=500,
            ), 'CTVF SPST 50 x 50'),

            'etvf_sun2019_re_1000_nx_100': (dict(
                integrator="gtvf",
                internal_flow=None,
                pst='sun2019',
                edac=None,
                summation=None,
                re=1000,
                nx=100,
                tf=60,
                pfreq=500,
            ), 'CTVF SPST 100 x 100'),

            'etvf_sun2019_re_1000_nx_200': (dict(
                integrator="gtvf",
                internal_flow=None,
                pst='sun2019',
                edac=None,
                summation=None,
                re=1000,
                nx=200,
                tf=60,
                pfreq=500,
            ), 'CTVF SPST 200 x 200'),
        }

        # Main case info
        self.case_info = {
            f'{s}': dict(kernel_choice=1, **_case_info[s][0])
            for s in _case_info
        }
        # print(self.case_info)

        self.labels = {f'{s}': _case_info[s][1] for s in _case_info}

        self.cases = [
            Simulation(get_path(name), cmd,
                       job_info=dict(n_core=n_core,
                                     n_thread=n_thread), cache_nnps=None,
                       **scheme_opts(self.case_info[name]))
            for name in self.case_info
        ]

    def run(self):
        self.make_output_dir()
        self._plot_u_profile()

    def _plot_u_profile(self):
        import matplotlib.pyplot as plt
        from pysph.examples.ghia_cavity_data import get_u_vs_y, get_v_vs_x

        # Re 100
        # u vs y
        y, exp_u = get_u_vs_y()
        x, exp_v = get_v_vs_x()

        for re in self.re:
            fig, ax = plt.subplots(2, 1, figsize=(7, 10))
            fig.subplots_adjust(hspace=0)
            ax[0].plot(exp_u[re], y, 'ko', fillstyle='none',
                       label=f'Ghia et al. (Re={re})')
            ax[0].set_xlabel('$u$')
            ax[0].set_ylabel('$y$')
            ax[1].plot(x, exp_v[re], 'ko', fillstyle='none',
                       label=f'Ghia et al. (Re={re})')
            ax[1].set_xlabel('$x$')
            ax[1].set_ylabel('$v$')

            for name in self.case_info:
                if self.case_info[name]['re'] == re:
                    data = np.load(self.input_path(name, 'results.npz'))
                    ax[0].plot(data['u_c'], data['x'],
                               label=self.labels[name])
                    ax[1].plot(data['x'], data['v_c'],
                               label=self.labels[name])
            ax[0].legend(loc='best')
            ax[1].legend(loc='best')
            ax[0].set_aspect('equal', 'box')
            ax[1].set_aspect('equal', 'box')
            fig.savefig(self.output_path(f'uv_re{re}.pdf'),
                        bbox_inches='tight', pad_inches=0)
            plt.close()


class DamBreak2D(Problem):
    def get_name(self):
        return 'dam_break_2d'

    def setup(self):
        get_path = self.input_path

        cmd = 'python code/dam_break_2d.py' + backend

        # Base case info
        self.case_info = {
            'etvf_sun2019': (dict(
                integrator="gtvf",
                no_internal_flow=None,
                pst='sun2019',
                edac=None,
                no_summation=None,
                clamp_pressure=None,
                dx=0.008,
                tf=2,
                pfreq=300,
            ), 'ETVF Sun2019'),
        }

        self.cases = [
            Simulation(get_path(name), cmd,
                       job_info=dict(n_core=n_core,
                                     n_thread=n_thread), cache_nnps=None,
                       **scheme_opts(self.case_info[name][0]))
            for name in self.case_info
        ]

    def run(self):
        self.make_output_dir()
        self.move_figures()
        self._plot_particles()

    def _plot_particles(self):
        from pysph.solver.utils import get_files

        times = [0.6, 1.14, 1.995]
        plot_data = {}
        for name in self.case_info:
            all_files = get_files(self.input_path(name), 'dam_break_2d')
            files = get_files_at_given_times(all_files, times)
            tmp = {}
            for i, f in enumerate(files):
                print(i, f)
                data = load(f)
                pa = data['arrays']['fluid']
                bo = data['arrays']['boundary']
                vmag = np.sqrt(pa.u**2 + pa.v**2)
                tmp[times[i]] = dict(
                    x=pa.x.copy(), y=pa.y.copy(), vmag=vmag, p=pa.p.copy(),
                    xb=bo.x.copy(), yb=bo.y.copy(), mb=bo.m.copy()
                )
            plot_data[name] = tmp

        for scalar in ['p', 'vmag']:
            self._plot_util(plot_data, scalar, times)

    def _plot_util(self, plot_data, scalar, times):
        import matplotlib.pyplot as plt
        import string

        ids = cycle(string.ascii_lowercase)

        figlen = len(plot_data)
        fig, ax = plt.subplots(1, 3, figsize=(16, 4), dpi=300,
                               sharex=True, sharey=True)
        for i, t in enumerate(times):
            vmin, vmax = 0, 0
            for name in plot_data:
                vmin = min(vmin, min(plot_data[name][t][scalar]))
                vmax = max(vmax, max(plot_data[name][t][scalar]))

            for j, name in enumerate(plot_data):
                data = plot_data[name][t]
                tmp = ax[i].scatter(
                    data['x'], data['y'], c=data[scalar], marker='.', s=6,
                    edgecolors='none', cmap='jet', linewidth=0,
                    rasterized=True, vmin=vmin, vmax=vmax
                )
                fig.colorbar(tmp, ax=ax[i], shrink=0.95, label=scalar,
                             format='%.0e' if scalar == 'p' else '%.1f')
                ax[i].scatter(
                    data['xb'], data['yb'], c=data['mb'], marker='.', s=6,
                    edgecolors='none', linewidth=0, rasterized=True
                )
                ax[i].annotate(
                    't = %.1f' % t, xy=(0.5, 3.5), fontsize=18
                )
                ax[i].grid()
                ax[i].set_xlabel(r'$x$')
                ax[i].set_ylabel(r'$y$')
                plt.xlim(-0.1, 4.1)
                plt.ylim(-0.1, 4.1)

        fig.savefig(self.output_path(f'db2d_{scalar}.pdf'),
                    bbox_inches='tight', pad_inches=0)
        plt.close()

    def move_figures(self):
        import shutil
        import os

        for name in self.case_info:
            source = self.input_path(name)

            target_dir = "manuscript/figures/" + source[8:] + "/"
            os.makedirs(target_dir)
            # print(target_dir)

            file_names = os.listdir(source)

            for file_name in file_names:
                # print(file_name)
                if file_name.endswith((".jpg", ".pdf", ".png")):
                    # print(target_dir)
                    shutil.copy(os.path.join(source, file_name), target_dir)


class TaylorGreen(Problem):
    def get_name(self):
        return 'taylor_green'

    def setup(self):
        get_path = self.input_path

        cmd = 'python code/taylor_green_etvf.py' + backend

        # length = [1., 2., 3., 4.]
        # height = [0.1]
        # pfreq = 500

        # self.re = [100, 1000]
        # Base case info
        _case_info = {
            'etvf_sun2019_re_100_nx_50': (dict(
                integrator="gtvf",
                internal_flow=None,
                pst='sun2019',
                edac=None,
                summation=None,
                re=100,
                nx=50,
                perturb=0.1,
                tf=2.5,
            ), 'ETVF Sun2019 nx 50'),

            'etvf_sun2019_re_100_nx_100': (dict(
                integrator="gtvf",
                internal_flow=None,
                pst='sun2019',
                edac=None,
                summation=None,
                re=100,
                nx=100,
                perturb=0.1,
                tf=2.5,
            ), 'ETVF Sun2019 nx 100'),

            'etvf_sun2019_re_100_nx_150': (dict(
                integrator="gtvf",
                internal_flow=None,
                pst='sun2019',
                edac=None,
                summation=None,
                re=100,
                nx=150,
                perturb=0.1,
                tf=2.5,
            ), 'ETVF Sun2019 nx 150'),

            'etvf_ipst_re_100_nx_50': (dict(
                integrator="gtvf",
                internal_flow=None,
                pst='ipst',
                edac=None,
                summation=None,
                ipst_max_iterations=10,
                ipst_interval=1,
                re=100,
                nx=50,
                perturb=0.1,
                tf=2.5,
            ), 'ETVF IPST nx 50'),

            'etvf_ipst_re_100_nx_100': (dict(
                integrator="gtvf",
                internal_flow=None,
                pst='ipst',
                edac=None,
                summation=None,
                ipst_max_iterations=10,
                ipst_interval=1,
                re=100,
                nx=100,
                perturb=0.1,
                tf=2.5,
            ), 'ETVF IPST nx 100'),

            'etvf_ipst_re_100_nx_150': (dict(
                integrator="gtvf",
                internal_flow=None,
                pst='ipst',
                edac=None,
                summation=None,
                ipst_max_iterations=10,
                ipst_interval=1,
                re=100,
                nx=150,
                perturb=0.1,
                tf=2.5,
            ), 'ETVF IPST nx 150'),
        }

        # Main case info
        self.case_info = {
            f'{s}': dict(kernel_choice=1, **_case_info[s][0])
            for s in _case_info
        }
        # print(self.case_info)

        self.labels = {f'{s}': _case_info[s][1] for s in _case_info}

        self.cases = [
            Simulation(get_path(name), cmd,
                       job_info=dict(n_core=n_core,
                                     n_thread=n_thread), cache_nnps=None,
                       **scheme_opts(self.case_info[name]))
            for name in self.case_info
        ]

    def run(self):
        self.make_output_dir()
        self._plot_error_plots()
        self._plot_particle_property(dict(nx=150, pst='sun2019'),
                                     fname='nx_150_sun2019', label='p', plot_key='p')

        self._plot_particle_property(dict(nx=150, pst='ipst'),
                                     fname='nx_150_ipst', label='p', plot_key='p')

    def _plot_error_plots(self):
        # Plots due to Sun 2019
        import matplotlib.pyplot as plt

        for name in self.case_info:
            if "sun2019" in name:
                nx = self.case_info[name]['nx']
                data = np.load(self.input_path(name, 'results.npz'))
                plt.semilogy(data['t'], data['decay'], label=f'{nx} x {nx}')

        plt.semilogy(data['t'], data['decay_ex'], 'k', label='exact')

        plt.legend(loc='best')
        plt.xlabel("t in seconds")
        plt.ylabel("Decay in maximum velocity")
        plt.savefig(self.output_path('tg_decay.pdf'),
                    bbox_inches='tight', pad_inches=0)
        plt.close()

        for name in self.case_info:
            if "sun2019" in name:
                nx = self.case_info[name]['nx']
                data = np.load(self.input_path(name, 'results.npz'))
                plt.plot(data['t'], data['l1'], label=f'{nx} x {nx}')

        plt.legend(loc='best')
        plt.xlabel("t in seconds")
        plt.ylabel(r"$L_1$ error in velocity magnitude")
        plt.savefig(self.output_path('tg_l1.pdf'),
                    bbox_inches='tight', pad_inches=0)
        plt.close()

        for name in self.case_info:
            if "sun2019" in name:
                nx = self.case_info[name]['nx']
                data = np.load(self.input_path(name, 'results.npz'))
                plt.plot(data['t'], data['p_l1'], label=f'{nx} x {nx}')

        plt.legend(loc='best')
        plt.xlabel("t in seconds")
        plt.ylabel(r"$L_1$ error of pressure")
        plt.savefig(self.output_path('tg_p_l1.pdf'),
                    bbox_inches='tight', pad_inches=0)
        plt.close()

        # ==================================
        # Plots due to IPST
        # ==================================
        for name in self.case_info:
            if "ipst" in name:
                nx = self.case_info[name]['nx']
                data = np.load(self.input_path(name, 'results.npz'))
                plt.semilogy(data['t'], data['decay'], label=f'{nx} x {nx}')

        plt.semilogy(data['t'], data['decay_ex'], 'k', label='exact')

        plt.legend(loc='best')
        plt.xlabel("t in seconds")
        plt.ylabel("Decay in maximum velocity")
        plt.savefig(self.output_path('tg_decay_ipst.pdf'),
                    bbox_inches='tight', pad_inches=0)
        plt.close()

        for name in self.case_info:
            if "ipst" in name:
                nx = self.case_info[name]['nx']
                data = np.load(self.input_path(name, 'results.npz'))
                plt.plot(data['t'], data['l1'], label=f'{nx} x {nx}')

        plt.legend(loc='best')
        plt.xlabel("t in seconds")
        plt.ylabel(r"$L_1$ error in velocity magnitude")
        plt.savefig(self.output_path('tg_l1_ipst.pdf'),
                    bbox_inches='tight', pad_inches=0)
        plt.close()

        for name in self.case_info:
            if "ipst" in name:
                nx = self.case_info[name]['nx']
                data = np.load(self.input_path(name, 'results.npz'))
                plt.plot(data['t'], data['p_l1'], label=f'{nx} x {nx}')

        plt.legend(loc='best')
        plt.xlabel("t in seconds")
        plt.ylabel(r"$L_1$ error of pressure")
        plt.savefig(self.output_path('tg_p_l1_ipst.pdf'),
                    bbox_inches='tight', pad_inches=0)
        plt.close()

    def _plot_particle_property(self, conditions, fname=None, label=None, plot_key='h'):
        import matplotlib.pyplot as plt

        fig, ax = plt.subplots(1, 1, figsize=(6, 4), sharey=True)
        plt.setp(ax, aspect=1.0, adjustable='box')
        for case in filter_cases(self.cases, **conditions):
            re = case.params['re']
            files = get_files(case.input_path(), 'taylor_green')
            data = load(files[-1])
            f = data['arrays']['fluid']
            tmp = ax.scatter(
                f.x, f.y, c=f.get(plot_key), s=2,
                cmap=plt.cm.get_cmap('jet', 16),
                edgecolors='none', rasterized=True,
                vmin=np.min(f.get(plot_key)),
                vmax=np.max(f.get(plot_key))
            )
            ax.set_xlabel(r'$x$')
            ax.set_ylabel(r'$y$')
            ax.set_xlim(0, 1)
            ax.set_ylim(0, 1)
        ax.grid()
        cbar = fig.colorbar(tmp, ax=ax, shrink=0.9, label=label, pad=0.01)
        plt.savefig(self.output_path(f'tg_pplot_{plot_key}_{fname}'))
        plt.close()


class TaylorGreenRe1000(Problem):
    def get_name(self):
        return 'taylor_green_re_1000'

    def setup(self):
        get_path = self.input_path

        cmd = 'python code/taylor_green_etvf.py' + backend

        # length = [1., 2., 3., 4.]
        # height = [0.1]
        # pfreq = 500

        # self.re = [100, 1000]
        # Base case info
        _case_info = {
            'etvf_sun2019_re_1000_nx_50': (dict(
                integrator="gtvf",
                internal_flow=None,
                pst='sun2019',
                edac=None,
                summation=None,
                re=1000,
                nx=50,
                perturb=0.1,
                tf=2.5,
            ), 'ETVF Sun2019 nx 50'),

            'etvf_sun2019_re_1000_nx_100': (dict(
                integrator="gtvf",
                internal_flow=None,
                pst='sun2019',
                edac=None,
                summation=None,
                re=1000,
                nx=100,
                perturb=0.1,
                tf=2.5,
            ), 'ETVF Sun2019 nx 100'),

            'etvf_sun2019_re_1000_nx_150': (dict(
                integrator="gtvf",
                internal_flow=None,
                pst='sun2019',
                edac=None,
                summation=None,
                re=1000,
                nx=150,
                perturb=0.1,
                tf=2.5,
            ), 'ETVF Sun2019 nx 150'),

            'etvf_ipst_re_1000_nx_50': (dict(
                integrator="gtvf",
                internal_flow=None,
                pst='ipst',
                edac=None,
                summation=None,
                ipst_max_iterations=10,
                ipst_interval=1,
                re=1000,
                nx=50,
                perturb=0.1,
                tf=2.5,
            ), 'ETVF IPST nx 50'),

            'etvf_ipst_re_1000_nx_100': (dict(
                integrator="gtvf",
                internal_flow=None,
                pst='ipst',
                edac=None,
                summation=None,
                ipst_max_iterations=10,
                ipst_interval=1,
                re=1000,
                nx=100,
                perturb=0.1,
                tf=2.5,
            ), 'ETVF IPST nx 100'),

            'etvf_ipst_re_1000_nx_150': (dict(
                integrator="gtvf",
                internal_flow=None,
                pst='ipst',
                edac=None,
                summation=None,
                ipst_max_iterations=10,
                ipst_interval=1,
                re=1000,
                nx=150,
                perturb=0.1,
                tf=2.5,
            ), 'ETVF IPST nx 150'),
        }

        # Main case info
        self.case_info = {
            f'{s}': dict(kernel_choice=1, **_case_info[s][0])
            for s in _case_info
        }
        # print(self.case_info)

        self.labels = {f'{s}': _case_info[s][1] for s in _case_info}

        self.cases = [
            Simulation(get_path(name), cmd,
                       job_info=dict(n_core=n_core,
                                     n_thread=n_thread), cache_nnps=None,
                       **scheme_opts(self.case_info[name]))
            for name in self.case_info
        ]

    def run(self):
        self.make_output_dir()
        self._plot_error_plots()
        self._plot_particle_property(dict(nx=150),
                                     label='nx_150', plot_key='p')

    def _plot_error_plots(self):
        # Plots due to Sun 2019
        import matplotlib.pyplot as plt

        for name in self.case_info:
            if "sun2019" in name:
                nx = self.case_info[name]['nx']
                data = np.load(self.input_path(name, 'results.npz'))
                plt.semilogy(data['t'], data['decay'], label=f'{nx} x {nx}')

        plt.semilogy(data['t'], data['decay_ex'], 'k', label='exact')

        plt.legend(loc='best')
        plt.xlabel("t in seconds")
        plt.ylabel("Decay in maximum velocity")
        plt.savefig(self.output_path('tg_decay.pdf'),
                    bbox_inches='tight', pad_inches=0)
        plt.close()

        for name in self.case_info:
            if "sun2019" in name:
                nx = self.case_info[name]['nx']
                data = np.load(self.input_path(name, 'results.npz'))
                plt.plot(data['t'], data['l1'], label=f'{nx} x {nx}')

        plt.legend(loc='best')
        plt.xlabel("t in seconds")
        plt.ylabel(r"$L_1$ error in velocity magnitude")
        plt.savefig(self.output_path('tg_l1.pdf'),
                    bbox_inches='tight', pad_inches=0)
        plt.close()

        for name in self.case_info:
            if "sun2019" in name:
                nx = self.case_info[name]['nx']
                data = np.load(self.input_path(name, 'results.npz'))
                plt.plot(data['t'], data['p_l1'], label=f'{nx} x {nx}')

        plt.legend(loc='best')
        plt.xlabel("t in seconds")
        plt.ylabel(r"$L_1$ error of pressure")
        plt.savefig(self.output_path('tg_p_l1.pdf'),
                    bbox_inches='tight', pad_inches=0)
        plt.close()

        # ==================================
        # Plots due to IPST
        # ==================================
        for name in self.case_info:
            if "ipst" in name:
                nx = self.case_info[name]['nx']
                data = np.load(self.input_path(name, 'results.npz'))
                plt.semilogy(data['t'], data['decay'], label=f'{nx} x {nx}')

        plt.semilogy(data['t'], data['decay_ex'], 'k', label='exact')

        plt.legend(loc='best')
        plt.xlabel("t in seconds")
        plt.ylabel("Decay in maximum velocity")
        plt.savefig(self.output_path('tg_decay_ipst.pdf'),
                    bbox_inches='tight', pad_inches=0)
        plt.close()

        for name in self.case_info:
            if "ipst" in name:
                nx = self.case_info[name]['nx']
                data = np.load(self.input_path(name, 'results.npz'))
                plt.plot(data['t'], data['l1'], label=f'{nx} x {nx}')

        plt.legend(loc='best')
        plt.xlabel("t in seconds")
        plt.ylabel(r"$L_1$ error in velocity magnitude")
        plt.savefig(self.output_path('tg_l1_ipst.pdf'),
                    bbox_inches='tight', pad_inches=0)
        plt.close()

        for name in self.case_info:
            if "ipst" in name:
                nx = self.case_info[name]['nx']
                data = np.load(self.input_path(name, 'results.npz'))
                plt.plot(data['t'], data['p_l1'], label=f'{nx} x {nx}')

        plt.legend(loc='best')
        plt.xlabel("t in seconds")
        plt.ylabel(r"$L_1$ error of pressure")
        plt.savefig(self.output_path('tg_p_l1_ipst.pdf'),
                    bbox_inches='tight', pad_inches=0)
        plt.close()

    def _plot_particle_property(self, conditions, label=None, plot_key='h'):
        import matplotlib.pyplot as plt

        fig, ax = plt.subplots(1, 1, figsize=(6, 4), sharey=True)
        plt.setp(ax, aspect=1.0, adjustable='box')
        for case in filter_cases(self.cases, **conditions):
            re = case.params['re']
            files = get_files(case.input_path(), 'taylor_green')
            data = load(files[-1])
            f = data['arrays']['fluid']
            tmp = ax.scatter(
                f.x, f.y, c=f.get(plot_key), s=2,
                cmap=plt.cm.get_cmap('jet', 16),
                edgecolors='none', rasterized=True,
                vmin=np.min(f.get(plot_key)),
                vmax=np.max(f.get(plot_key))
            )
            ax.set_xlabel(r'$x$')
            ax.set_ylabel(r'$y$')
            ax.set_xlim(0, 1)
            ax.set_ylim(0, 1)
        ax.grid()
        if plot_key == 'n_nbrs':
            label="Neighbors"
        else:
            label=plot_key
        cbar = fig.colorbar(tmp, ax=ax, shrink=0.9, label=label, pad=0.01)
        plt.savefig(self.output_path(f'tg_pplot_{plot_key}_re_{re}'))
        plt.close()


class FreeSurfaceIdentification(Problem):
    def get_name(self):
        return 'free_surface_identification_demonstration'

    def setup(self):
        get_path = self.input_path

        cmd = 'python code/free_surface_identification_demonstration.py' + backend

        # Base case info
        self.case_info = {
            'free_surface': (dict(
            ), 'ETVF Sun2019'),
        }

        self.cases = [
            Simulation(get_path(name), cmd,
                       job_info=dict(n_core=n_core,
                                     n_thread=n_thread), cache_nnps=None,
                       **scheme_opts(self.case_info[name][0]))
            for name in self.case_info
        ]

    def run(self):
        self.make_output_dir()
        self.move_figures()

    def move_figures(self):
        import shutil
        import os

        source = "code/free_surface_identification_demonstration_output"

        target_dir = "manuscript/figures/free_surface_identification_demonstration/"
        os.makedirs(target_dir, exist_ok=True)
        # print(target_dir)

        file_names = os.listdir(source)

        for file_name in file_names:
            # print(file_name)
            if file_name.endswith((".jpg", ".pdf", ".png")):
                # print(target_dir)
                shutil.copy(os.path.join(source, file_name), target_dir)


# With and with out correction terms results
class UniaxialCompressionCorrectionsTest(Problem):
    def get_name(self):
        return 'uniaxial_compression_corrections_test'

    def setup(self):
        get_path = self.input_path

        cmd = 'python code/uniaxial_compression.py' + backend

        # length = [1., 2., 3., 4.]
        # height = [0.1]
        # pfreq = 500

        # Base case info
        self.case_info = {
            'etvf_sun2019_dx_low': (dict(
                pst='sun2019',
                no_uhat_velgrad=None,
                uhat_cont=None,
                continuity_tvf_correction=None,
                shear_stress_tvf_correction=None,
                edac=None,
                no_surf_p_zero=None,
                dx=2e-3,
                pfreq=30), r'CTVF SUN2019 $\Delta x=2 \times 10^{-3}$ m'),

            'etvf_sun2019_dx_medium': (dict(
                pst='sun2019',
                no_uhat_velgrad=None,
                uhat_cont=None,
                continuity_tvf_correction=None,
                shear_stress_tvf_correction=None,
                edac=None,
                no_surf_p_zero=None,
                dx=1e-3,
                pfreq=30), 'CTVF SPST $\Delta x=1 \times 10^{-3}$ m'),

            'etvf_sun2019_dx_high': (dict(
                pst='sun2019',
                no_uhat_velgrad=None,
                uhat_cont=None,
                continuity_tvf_correction=None,
                shear_stress_tvf_correction=None,
                edac=None,
                no_surf_p_zero=None,
                dx=0.5 * 1e-3,
                pfreq=30), 'CTVF SPST $\Delta x=0.5 \times 10^{-3}$ m'),

            'etvf_sun2019_dx_very_high': (dict(
                pst='sun2019',
                no_uhat_velgrad=None,
                uhat_cont=None,
                continuity_tvf_correction=None,
                shear_stress_tvf_correction=None,
                edac=None,
                no_surf_p_zero=None,
                dx=0.25 * 1e-3,
                pfreq=30), 'CTVF SPST $\Delta x=0.25 \times 10^{-3}$ m'),

            'etvf_sun2019_dx_low_no_correction': (dict(
                pst='sun2019',
                uhat_velgrad=None,
                uhat_cont=None,
                no_continuity_tvf_correction=None,
                no_shear_stress_tvf_correction=None,
                edac=None,
                no_surf_p_zero=None,
                dx=2e-3,
                pfreq=30), 'CTVF SPST Low No Corrections'),

            'etvf_sun2019_dx_medium_no_correction': (dict(
                pst='sun2019',
                uhat_velgrad=None,
                uhat_cont=None,
                no_continuity_tvf_correction=None,
                no_shear_stress_tvf_correction=None,
                edac=None,
                no_surf_p_zero=None,
                dx=1e-3,
                pfreq=30), 'CTVF SPST Medium No Corrections'),

            'etvf_sun2019_dx_high_no_correction': (dict(
                pst='sun2019',
                uhat_velgrad=None,
                uhat_cont=None,
                no_continuity_tvf_correction=None,
                no_shear_stress_tvf_correction=None,
                edac=None,
                no_surf_p_zero=None,
                dx=0.5 * 1e-3,
                pfreq=30), 'CTVF SPST High No Corrections'),

            'etvf_sun2019_dx_very_high_no_correction': (dict(
                pst='sun2019',
                uhat_velgrad=None,
                uhat_cont=None,
                no_continuity_tvf_correction=None,
                no_shear_stress_tvf_correction=None,
                edac=None,
                no_surf_p_zero=None,
                dx=0.25 * 1e-3,
                pfreq=30), 'CTVF SPST Super High No Corrections'),
        }

        self.cases = [
            Simulation(get_path(name), cmd,
                       job_info=dict(n_core=n_core,
                                     n_thread=n_thread), cache_nnps=None,
                       **scheme_opts(self.case_info[name][0]))
            for name in self.case_info
        ]

    def run(self):
        self.make_output_dir()
        self.plot_von_mises()

    def plot_von_mises(self):
        import matplotlib.pyplot as plt

        data = {}
        for name in self.case_info:
            data[name] = np.load(self.input_path(name, 'results.npz'))

        # =======================================
        # Low resolution figure
        # =======================================
        plt.figure(figsize=(6, 5))

        name = 'etvf_sun2019_dx_low'
        t_sph_A = data[name]['t_sph_A']
        t_fem_A = data[name]['t_fem_A']
        von_mises_sph_A = data[name]['von_mises_sph_A']
        von_mises_fem_A = data[name]['von_mises_fem_A']
        plt.plot(t_fem_A, von_mises_fem_A, "--g", label='FEM')
        plt.plot(t_sph_A, von_mises_sph_A, "--b", label='SPH Das')

        # GTVF results
        t_current = data[name]['t_current']
        von_mises_A_current = data[name]['von_mises_A_current']

        plt.plot(t_current, von_mises_A_current, "-sc",
                 label=self.case_info[name][1] + " medium")

        i = 0
        # color = ['r', 'y', 'k']
        for name in self.case_info:
            if self.case_info[name][0]['dx'] == 0.5e-3 and self.case_info[name][0]['pst'] == 'sun2019':
                t_current = data[name]['t_current']
                von_mises_A_current = data[name]['von_mises_A_current']

                plt.plot(t_current, von_mises_A_current, "-s",
                         label=self.case_info[name][1])
                i = i + 1

            if self.case_info[name][0]['dx'] == 1e-3 and self.case_info[name][0]['pst'] == 'sun2019':
                t_current = data[name]['t_current']
                von_mises_A_current = data[name]['von_mises_A_current']

                plt.plot(t_current, von_mises_A_current, "-s",
                         label=self.case_info[name][1])
                i = i + 1

            if self.case_info[name][0]['dx'] == 2e-3 and self.case_info[name][0]['pst'] == 'sun2019':
                t_current = data[name]['t_current']
                von_mises_A_current = data[name]['von_mises_A_current']

                plt.plot(t_current, von_mises_A_current, "-s",
                         label=self.case_info[name][1])

                i = i + 1

        plt.xlabel('time (ms)')
        plt.ylabel('von Mises')
        plt.legend(loc='upper left')
        # plt.tight_layout(pad=0)
        plt.savefig(self.output_path('von_mises_A_correction_terms.pdf'))
        plt.clf()
        plt.close()


# fluids with and without correction cases
class TaylorGreenSun2019CorrectionsTest(Problem):
    def get_name(self):
        return 'taylor_green_sun2019_corrections_test'

    def setup(self):
        get_path = self.input_path

        cmd = 'python code/taylor_green_etvf.py' + backend

        # length = [1., 2., 3., 4.]
        # height = [0.1]
        # pfreq = 500

        # self.re = [100, 1000]
        # Base case info
        _case_info = {
            'etvf_sun2019_re_100_nx_150': (dict(
                integrator="gtvf",
                internal_flow=None,
                pst='sun2019',
                edac=None,
                summation=None,
                re=100,
                nx=150,
                perturb=0.1,
                tf=2.5,
            ), 'ETVF Sun2019 nx 150'),

            'etvf_sun2019_re_100_nx_150_no_corrections': (dict(
                integrator="gtvf",
                internal_flow=None,
                pst='sun2019',
                edac=None,
                summation=None,
                no_corrections=None,
                re=100,
                nx=150,
                perturb=0.1,
                tf=2.5,
            ), 'ETVF Sun2019 nx 150 No Corrections'),

            'etvf_sun2019_re_1000_nx_150': (dict(
                integrator="gtvf",
                internal_flow=None,
                pst='sun2019',
                edac=None,
                summation=None,
                re=1000,
                nx=150,
                perturb=0.1,
                tf=2.5,
            ), 'ETVF Sun2019 nx 150'),

            'etvf_sun2019_re_1000_nx_150_no_corrections': (dict(
                integrator="gtvf",
                internal_flow=None,
                pst='sun2019',
                edac=None,
                summation=None,
                no_corrections=None,
                re=1000,
                nx=150,
                perturb=0.1,
                tf=2.5,
            ), 'ETVF Sun2019 nx 150 No Corrections'),
        }

        # Main case info
        self.case_info = {
            f'{s}': dict(kernel_choice=1, **_case_info[s][0])
            for s in _case_info
        }
        # print(self.case_info)

        self.labels = {f'{s}': _case_info[s][1] for s in _case_info}

        self.cases = [
            Simulation(get_path(name), cmd,
                       job_info=dict(n_core=n_core,
                                     n_thread=n_thread), cache_nnps=None,
                       **scheme_opts(self.case_info[name]))
            for name in self.case_info
        ]

    def run(self):
        self.make_output_dir()
        self._plot_error_plots()

    def _plot_error_plots(self):
        # Plots due to Sun 2019
        import matplotlib.pyplot as plt

        for name in self.case_info:
            nx = self.case_info[name]['nx']
            re = self.case_info[name]['re']
            if re == 100:
                if "corrections" in name:
                    data = np.load(self.input_path(name, 'results.npz'))
                    plt.semilogy(data['t'], data['decay'], label=fr'$re$={re} {nx} x {nx} No Corrections')
                else:
                    data = np.load(self.input_path(name, 'results.npz'))
                    plt.semilogy(data['t'], data['decay'], label=fr'$re$={re} {nx} x {nx}')

        plt.semilogy(data['t'], data['decay_ex'], 'k', label='exact')

        plt.legend(loc='best')
        plt.xlabel("t in seconds")
        plt.ylabel("Decay in maximum velocity")
        plt.savefig(self.output_path('tg_decay_re_100.pdf'),
                    bbox_inches='tight', pad_inches=0)
        plt.close()

        for name in self.case_info:
            nx = self.case_info[name]['nx']
            re = self.case_info[name]['re']
            if re == 100:
                if "corrections" in name:
                    data = np.load(self.input_path(name, 'results.npz'))
                    plt.plot(data['t'], data['l1'], label=fr'$re$={re} {nx} x {nx} No Corrections')
                else:
                    data = np.load(self.input_path(name, 'results.npz'))
                    plt.plot(data['t'], data['l1'], label=fr'$re$={re} {nx} x {nx}')

        plt.legend(loc='best')
        plt.xlabel("t in seconds")
        plt.ylabel(r"$L_1$ error in velocity magnitude")
        plt.savefig(self.output_path('tg_l1_re_100.pdf'),
                    bbox_inches='tight', pad_inches=0)
        plt.close()

        for name in self.case_info:
            nx = self.case_info[name]['nx']
            re = self.case_info[name]['re']
            if re == 100:
                if "corrections" in name:
                    data = np.load(self.input_path(name, 'results.npz'))
                    plt.plot(data['t'], data['p_l1'], label=fr'$re$={re} {nx} x {nx} No Corrections')
                else:
                    data = np.load(self.input_path(name, 'results.npz'))
                    plt.plot(data['t'], data['p_l1'], label=fr'$re$={re} {nx} x {nx}')

        plt.legend(loc='best')
        plt.xlabel("t in seconds")
        plt.ylabel(r"$L_1$ error of pressure")
        plt.savefig(self.output_path('tg_p_l1_re_100.pdf'),
                    bbox_inches='tight', pad_inches=0)
        plt.close()

        # ==========================
        # If reynolds number is 1000
        # ==========================
        for name in self.case_info:
            nx = self.case_info[name]['nx']
            re = self.case_info[name]['re']
            if re == 1000:
                if "corrections" in name:
                    data = np.load(self.input_path(name, 'results.npz'))
                    plt.semilogy(data['t'], data['decay'], label=fr'$re$={re} {nx} x {nx} No Corrections')
                else:
                    data = np.load(self.input_path(name, 'results.npz'))
                    plt.semilogy(data['t'], data['decay'], label=fr'$re$={re} {nx} x {nx}')

        plt.semilogy(data['t'], data['decay_ex'], 'k', label='exact')

        plt.legend(loc='best')
        plt.xlabel("t in seconds")
        plt.ylabel("Decay in maximum velocity")
        plt.savefig(self.output_path('tg_decay_re_1000.pdf'),
                    bbox_inches='tight', pad_inches=0)
        plt.close()

        for name in self.case_info:
            nx = self.case_info[name]['nx']
            re = self.case_info[name]['re']
            if re == 1000:
                if "corrections" in name:
                    data = np.load(self.input_path(name, 'results.npz'))
                    plt.plot(data['t'], data['l1'], label=fr'$re$={re} {nx} x {nx} No Corrections')
                else:
                    data = np.load(self.input_path(name, 'results.npz'))
                    plt.plot(data['t'], data['l1'], label=fr'$re$={re} {nx} x {nx}')

        plt.legend(loc='best')
        plt.xlabel("t in seconds")
        plt.ylabel(r"$L_1$ error in velocity magnitude")
        plt.savefig(self.output_path('tg_l1_re_1000.pdf'),
                    bbox_inches='tight', pad_inches=0)
        plt.close()

        for name in self.case_info:
            nx = self.case_info[name]['nx']
            re = self.case_info[name]['re']
            if re == 1000:
                if "corrections" in name:
                    data = np.load(self.input_path(name, 'results.npz'))
                    plt.plot(data['t'], data['p_l1'], label=fr'$re$={re} {nx} x {nx} No Corrections')
                else:
                    data = np.load(self.input_path(name, 'results.npz'))
                    plt.plot(data['t'], data['p_l1'], label=fr'$re$={re} {nx} x {nx}')

        plt.legend(loc='best')
        plt.xlabel("t in seconds")
        plt.ylabel(r"$L_1$ error of pressure")
        plt.savefig(self.output_path('tg_p_l1_re_1000.pdf'),
                    bbox_inches='tight', pad_inches=0)
        plt.close()


class TaylorGreenIPSTCorrectionsTest(Problem):
    def get_name(self):
        return 'taylor_green_ipst_corrections_test'

    def setup(self):
        get_path = self.input_path

        cmd = 'python code/taylor_green_etvf.py' + backend

        # Base case info
        _case_info = {
            'etvf_ipst_re_100_nx_150': (dict(
                integrator="gtvf",
                internal_flow=None,
                pst='ipst',
                edac=None,
                summation=None,
                ipst_max_iterations=10,
                ipst_interval=1,
                re=100,
                nx=150,
                perturb=0.1,
                tf=2.5,
            ), 'ETVF IPST nx 150'),

            'etvf_ipst_re_100_nx_150_no_corrections': (dict(
                integrator="gtvf",
                internal_flow=None,
                pst='ipst',
                edac=None,
                summation=None,
                no_corrections=None,
                ipst_max_iterations=10,
                ipst_interval=1,
                re=100,
                nx=150,
                perturb=0.1,
                tf=2.5,
            ), 'ETVF IPST Re 100 nx 150 No Corrections'),

            'etvf_ipst_re_1000_nx_150': (dict(
                integrator="gtvf",
                internal_flow=None,
                pst='ipst',
                edac=None,
                summation=None,
                ipst_max_iterations=10,
                ipst_interval=1,
                re=1000,
                nx=150,
                perturb=0.1,
                tf=2.5,
            ), 'ETVF IPST Re 1000 nx 150'),

            'etvf_ipst_re_1000_nx_150_no_corrections': (dict(
                integrator="gtvf",
                internal_flow=None,
                pst='ipst',
                edac=None,
                summation=None,
                no_corrections=None,
                ipst_max_iterations=10,
                ipst_interval=1,
                re=1000,
                nx=150,
                perturb=0.1,
                tf=2.5,
            ), 'ETVF IPST Re=1000 nx 150 No Corrections'),

        }

        # Main case info
        self.case_info = {
            f'{s}': dict(kernel_choice=1, **_case_info[s][0])
            for s in _case_info
        }
        # print(self.case_info)

        self.labels = {f'{s}': _case_info[s][1] for s in _case_info}

        self.cases = [
            Simulation(get_path(name), cmd,
                       job_info=dict(n_core=n_core,
                                     n_thread=n_thread), cache_nnps=None,
                       **scheme_opts(self.case_info[name]))
            for name in self.case_info
        ]

    def run(self):
        self.make_output_dir()
        self._plot_error_plots()

    def _plot_error_plots(self):
        # Plots due to Sun 2019
        import matplotlib.pyplot as plt

        for name in self.case_info:
            nx = self.case_info[name]['nx']
            re = self.case_info[name]['re']
            if re == 100:
                if "corrections" in name:
                    data = np.load(self.input_path(name, 'results.npz'))
                    plt.semilogy(data['t'], data['decay'], label=fr'$re$={re} {nx} x {nx} No Corrections')
                else:
                    data = np.load(self.input_path(name, 'results.npz'))
                    plt.semilogy(data['t'], data['decay'], label=fr'$re$={re} {nx} x {nx}')

        plt.semilogy(data['t'], data['decay_ex'], 'k', label='exact')

        plt.legend(loc='best')
        plt.xlabel("t in seconds")
        plt.ylabel("Decay in maximum velocity")
        plt.savefig(self.output_path('tg_decay_re_100.pdf'),
                    bbox_inches='tight', pad_inches=0)
        plt.close()

        for name in self.case_info:
            nx = self.case_info[name]['nx']
            re = self.case_info[name]['re']
            if re == 100:
                if "corrections" in name:
                    data = np.load(self.input_path(name, 'results.npz'))
                    plt.plot(data['t'], data['l1'], label=fr'$re$={re} {nx} x {nx} No Corrections')
                else:
                    data = np.load(self.input_path(name, 'results.npz'))
                    plt.plot(data['t'], data['l1'], label=fr'$re$={re} {nx} x {nx}')

        plt.legend(loc='best')
        plt.xlabel("t in seconds")
        plt.ylabel(r"$L_1$ error in velocity magnitude")
        plt.savefig(self.output_path('tg_l1_re_100.pdf'),
                    bbox_inches='tight', pad_inches=0)
        plt.close()

        for name in self.case_info:
            nx = self.case_info[name]['nx']
            re = self.case_info[name]['re']
            if re == 100:
                if "corrections" in name:
                    data = np.load(self.input_path(name, 'results.npz'))
                    plt.plot(data['t'], data['p_l1'], label=fr'$re$={re} {nx} x {nx} No Corrections')
                else:
                    data = np.load(self.input_path(name, 'results.npz'))
                    plt.plot(data['t'], data['p_l1'], label=fr'$re$={re} {nx} x {nx}')

        plt.legend(loc='best')
        plt.xlabel("t in seconds")
        plt.ylabel(r"$L_1$ error of pressure")
        plt.savefig(self.output_path('tg_p_l1_re_100.pdf'),
                    bbox_inches='tight', pad_inches=0)
        plt.close()

        # ==========================
        # If reynolds number is 1000
        # ==========================
        for name in self.case_info:
            nx = self.case_info[name]['nx']
            re = self.case_info[name]['re']
            if re == 1000:
                if "corrections" in name:
                    data = np.load(self.input_path(name, 'results.npz'))
                    plt.semilogy(data['t'], data['decay'], label=fr'$re$={re} {nx} x {nx} No Corrections')
                else:
                    data = np.load(self.input_path(name, 'results.npz'))
                    plt.semilogy(data['t'], data['decay'], label=fr'$re$={re} {nx} x {nx}')

        plt.semilogy(data['t'], data['decay_ex'], 'k', label='exact')

        plt.legend(loc='best')
        plt.xlabel("t in seconds")
        plt.ylabel("Decay in maximum velocity")
        plt.savefig(self.output_path('tg_decay_re_1000.pdf'),
                    bbox_inches='tight', pad_inches=0)
        plt.close()

        for name in self.case_info:
            nx = self.case_info[name]['nx']
            re = self.case_info[name]['re']
            if re == 1000:
                if "corrections" in name:
                    data = np.load(self.input_path(name, 'results.npz'))
                    plt.plot(data['t'], data['l1'], label=fr'$re$={re} {nx} x {nx} No Corrections')
                else:
                    data = np.load(self.input_path(name, 'results.npz'))
                    plt.plot(data['t'], data['l1'], label=fr'$re$={re} {nx} x {nx}')

        plt.legend(loc='best')
        plt.xlabel("t in seconds")
        plt.ylabel(r"$L_1$ error in velocity magnitude")
        plt.savefig(self.output_path('tg_l1_re_1000.pdf'),
                    bbox_inches='tight', pad_inches=0)
        plt.close()

        for name in self.case_info:
            nx = self.case_info[name]['nx']
            re = self.case_info[name]['re']
            if re == 1000:
                if "corrections" in name:
                    data = np.load(self.input_path(name, 'results.npz'))
                    plt.plot(data['t'], data['p_l1'], label=fr'$re$={re} {nx} x {nx} No Corrections')
                else:
                    data = np.load(self.input_path(name, 'results.npz'))
                    plt.plot(data['t'], data['p_l1'], label=fr'$re$={re} {nx} x {nx}')

        plt.legend(loc='best')
        plt.xlabel("t in seconds")
        plt.ylabel(r"$L_1$ error of pressure")
        plt.savefig(self.output_path('tg_p_l1_re_1000.pdf'),
                    bbox_inches='tight', pad_inches=0)
        plt.close()


class CavitySun2019CorrectionsTest(Problem):
    def get_name(self):
        return 'cavity_sun2019_corrections_test'

    def setup(self):
        get_path = self.input_path

        cmd = 'python code/lid_driven_cavity.py' + backend

        # length = [1., 2., 3., 4.]
        # height = [0.1]
        # pfreq = 500

        self.re = [100, 1000]
        # Base case info
        _case_info = {
            'etvf_sun2019_re_100_nx_150': (dict(
                integrator="gtvf",
                internal_flow=None,
                pst='sun2019',
                edac=None,
                summation=None,
                re=100,
                nx=150,
                tf=25,
                pfreq=300,
            ), 'Corrections'),

            'etvf_sun2019_re_100_nx_150_no_corrections': (dict(
                integrator="gtvf",
                internal_flow=None,
                pst='sun2019',
                edac=None,
                summation=None,
                no_corrections=None,
                re=100,
                nx=150,
                tf=25,
                pfreq=300,
            ), 'No Corrections'),
        }

        # Main case info
        self.case_info = {
            f'{s}': dict(kernel_choice=1, **_case_info[s][0])
            for s in _case_info
        }
        # print(self.case_info)

        self.labels = {f'{s}': _case_info[s][1] for s in _case_info}

        self.cases = [
            Simulation(get_path(name), cmd,
                       job_info=dict(n_core=n_core,
                                     n_thread=n_thread), cache_nnps=None,
                       **scheme_opts(self.case_info[name]))
            for name in self.case_info
        ]

    def run(self):
        self.make_output_dir()
        self._plot_u_profile()
        self.plot_particles()

    def _plot_u_profile(self):
        import matplotlib.pyplot as plt
        from pysph.examples.ghia_cavity_data import get_u_vs_y, get_v_vs_x

        # Re 100
        # u vs y
        y, exp_u = get_u_vs_y()
        x, exp_v = get_v_vs_x()

        for re in self.re:
            fig, ax = plt.subplots(2, 1, figsize=(7, 10))
            fig.subplots_adjust(hspace=0)
            ax[0].plot(exp_u[re], y, 'ko', fillstyle='none',
                       label=f'Ghia et al. (Re={re})')
            ax[0].set_xlabel('$u$')
            ax[0].set_ylabel('$y$')
            ax[1].plot(x, exp_v[re], 'ko', fillstyle='none',
                       label=f'Ghia et al. (Re={re})')
            ax[1].set_xlabel('$x$')
            ax[1].set_ylabel('$v$')

            for name in self.case_info:
                if self.case_info[name]['re'] == re:
                    data = np.load(self.input_path(name, 'results.npz'))
                    ax[0].plot(data['u_c'], data['x'],
                               label=self.labels[name])
                    ax[1].plot(data['x'], data['v_c'],
                               label=self.labels[name])
            ax[0].legend(loc='best')
            ax[1].legend(loc='best')
            ax[0].set_aspect('equal', 'box')
            ax[1].set_aspect('equal', 'box')
            fig.savefig(self.output_path(f'uv_re{re}.pdf'),
                        bbox_inches='tight', pad_inches=0)
            plt.close()

    def plot_particles(self):
        import matplotlib.pyplot as plt
        from pysph.solver.utils import get_files

        fig, ax = plt.subplots(1, 2, figsize=(11, 4), sharex=True, sharey=True)
        fig.subplots_adjust(wspace=0.2)
        for i, case in enumerate(self.cases):
            files = get_files(case.input_path(), 'lid_driven_cavity')

            data = load(files[-1])
            f = data['arrays']['fluid']
            vmag = (f.u**2. + f.v**2.)**0.5
            tmp = ax[i].scatter(
                f.x, f.y, s=3, c=vmag, rasterized=True
            )
            ax[i].grid()
            ax[i].set_xlabel(self.labels[case.name])
            ax[i].set_xlim(0, 1)
            ax[i].set_ylim(0, 1)
        fig.colorbar(tmp, ax=ax.tolist(), shrink=0.95, label='vmag')
        fig.savefig(self.output_path('good_vs_bad.pdf'),
                    bbox_inches='tight', pad_inches=0)
        plt.close()


class OscillatingPlateGTVF(Problem):
    def get_name(self):
        return 'oscillating_plate_gtvf'

    def setup(self):
        get_path = self.input_path

        cmd = 'python code/oscillating_plate_gtvf.py' + backend

        # Tensile instability cases
        _cases_1 = {
            # this case is to show that the basic SPH equations result in an
            # artificial fracture of the oscillating plate at maximum tensile
            # stress
            'gtvf': (dict(
                pfreq=1000,
                tf=1.,
                N=10,
            ), 'GTVF'),
        }

        # Main case info
        self.case_info_1 = {
            f'{s}': dict(**_cases_1[s][0])
            for s in _cases_1
        }

        self.labels_1 = {f'{s}': _cases_1[s][1] for s in _cases_1}

        self.case_info = {**self.case_info_1}
        self.labels = {**self.labels_1}

        self.cases = [
            Simulation(get_path(name), cmd,
                       job_info=dict(n_core=n_core,
                                     n_thread=n_thread), cache_nnps=None,
                       **scheme_opts(self.case_info[name]))
            for name in self.case_info
        ]

    def run(self):
        self.make_output_dir()
        self.plot_particles()

    def plot_particles(self):

        for name in self.case_info:
            if name == "gtvf":
                source = self.input_path(name)
                files = get_files(source)
                # print(files)
                data = load(files[5])
                arrays = data['arrays']
                plate = arrays['plate']
                wall = arrays['wall']

                plt.figure()
                plt.scatter(wall.x, wall.y, c=wall.m, s=0.1)
                plt.scatter(plate.x, plate.y, c=plate.sigma00, s=0.1)
                plt.grid()
                plt.axes().set_aspect('equal')
                plt.colorbar(format='%.0e')

                plt.savefig(self.output_path('gtvf_original.pdf'))
                plt.clf()
                plt.close()

        # Convergence plots
        plt.figure()

        name = "gtvf"
        data = np.load(self.input_path(name, 'results.npz'))
        # sort the fem data before plotting
        p = data['t'].argsort()
        t = data['t'][p]
        amplitude = data['amplitude'][p]
        plt.plot(t, amplitude, label=f'N=10')

        plt.legend()
        plt.xlabel("Time (seconds)")
        plt.ylabel("Amplitude")

        plt.savefig(self.output_path('ipst_convergence_plot.pdf'))
        plt.clf()
        plt.close()


class OscillatingPlateLargeDeformation(Problem):
    def get_name(self):
        return 'oscillating_plate_large_deformation'

    def setup(self):
        get_path = self.input_path

        cmd = 'python code/oscillating_plate_large_deformation.py' + backend

        # Tensile instability cases
        _cases_1 = {
            'ipst': (dict(
                pst='ipst',
                no_uhat_velgrad=None,
                shear_stress_tvf_correction=None,
                edac=None,
                no_surf_p_zero=None,
                uhat_cont=None,
                continuity_tvf_correction=None,
                ipst_min_iterations=5,
                ipst_max_iterations=10,
                ipst_interval=1,
                length=0.4,
                height=0.02,
                N=10,
                pfreq=500,
                tf=1.,
            ), 'IPST N=10'),
        }

        # Main case info
        self.case_info_1 = {
            f'{s}': dict(kernel_choice=1, **_cases_1[s][0])
            for s in _cases_1
        }

        self.labels_1 = {f'{s}': _cases_1[s][1] for s in _cases_1}

        self.case_info = {**self.case_info_1}
        self.labels = {**self.labels_1}

        self.cases = [
            Simulation(get_path(name), cmd,
                       job_info=dict(n_core=n_core,
                                     n_thread=n_thread), cache_nnps=None,
                       **scheme_opts(self.case_info[name]))
            for name in self.case_info
        ]

    def run(self):
        self.make_output_dir()
        self.plot_particles()

    def plot_particles(self):
        for name in self.case_info:
            source = self.input_path(name)
            files = get_files(source)
            # print(files)
            data = load(files[57])
            arrays = data['arrays']
            plate = arrays['plate']
            wall = arrays['wall']

            plt.figure()
            plt.scatter(wall.x, wall.y, c=wall.m, s=0.1)
            plt.scatter(plate.x, plate.y, c=plate.sigma00, s=0.1)
            plt.grid()
            plt.axes().set_aspect('equal')
            plt.colorbar(format='%.0e')

            plt.savefig(self.output_path('ctvf_ipst.pdf'))
            plt.clf()
            plt.close()


if __name__ == '__main__':
    import matplotlib
    matplotlib.use('pdf')

    PROBLEMS = [
        OscillatingPlate, Rings, CantileverBending, UniaxialCompression,
        HighVelocityImpact, Cavity, DamBreak2D, TaylorGreen, TaylorGreenRe1000,
        FreeSurfaceIdentification,
        # new test cases
        UniaxialCompressionCorrectionsTest,
        TaylorGreenSun2019CorrectionsTest,
        TaylorGreenIPSTCorrectionsTest,
        CavitySun2019CorrectionsTest,

        OscillatingPlateGTVF,
        OscillatingPlateLargeDeformation
    ]

    automator = Automator(simulation_dir='outputs',
                          output_dir=os.path.join('manuscript', 'figures'),
                          all_problems=PROBLEMS)

    # task = FileCommandTask(
    #   'latexmk manuscript/paper.tex -pdf -outdir=manuscript',
    #   ['manuscript/paper.pdf']
    # )
    # automator.add_task(task, name='pdf', post_proc=True)

    automator.run()
