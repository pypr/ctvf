"""
High-Velocity impact (HVI). Section 4.4 of Generalized TVF by Zhang 2017.

TODO:

- Fix the edges of the structure.
- Fix the Young's modulus.
- Compare with different schemes.
"""

import numpy as np
from math import cos, sin, cosh, sinh, sqrt

# SPH equations
from pysph.sph.equation import Group, MultiStageEquations
from pysph.sph.basic_equations import IsothermalEOS

from pysph.sph.equation import Equation
from pysph.base.utils import get_particle_array
from pysph.base.kernels import (CubicSpline, WendlandQuintic, QuinticSpline)
# from pysph.solver.application import Application
from pysph.solver.solver import Solver
from pysph.solver.application import Application
from pysph.sph.integrator import EPECIntegrator
from pysph.sph.integrator_step import SolidMechStep
from pysph.tools.sph_evaluator import SPHEvaluator
from pysph.sph.scheme import SchemeChooser

from pysph.sph.solid_mech.basic import (get_particle_array_elastic_dynamics,
                                        get_speed_of_sound,
                                        get_shear_modulus)

from solid_mech import SolidsScheme

# from solid_mech import (SolidMechanicsScheme, GraySolidsScheme,
#                         ETVFSolidsScheme, ETVFNewSolidsScheme,
#                         GTVFSolidsScheme, EdacSolidsScheme,
#                         ETVFEDACSolidsScheme,
#                         setup_particle_array_for_gray_solid,
#                         setup_particle_array_for_etvf_solid,
#                         setup_particle_array_for_edac_solid,
#                         setup_particle_array_for_gtvf_solid,
#                         setup_particle_array_for_etvf_edac_solid,
#                         get_poisson_ratio_from_E_G,
#                         add_plasticity_properties,
#                         ComputeJ2,
#                         LimitDeviatoricStress)

# for normals
from pysph.sph.isph.wall_normal import ComputeNormals, SmoothNormals


from boundary_particles import (add_boundary_identification_properties,
                                get_boundary_identification_etvf_equations)


class MakeAccelerationZeroOnSelectedIndices(Equation):
    def initialize(self, d_idx, d_au, d_av, d_aw, d_auhat, d_avhat, d_awhat,
                   d_make_acceleration_zero):
        if d_make_acceleration_zero[d_idx] == 1:
            d_au[d_idx] = 0
            d_av[d_idx] = 0
            d_aw[d_idx] = 0

            d_auhat[d_idx] = 0
            d_avhat[d_idx] = 0
            d_awhat[d_idx] = 0


class AdamiBoundaryConditionExtrapolateNoSlipFixedParticles(Equation):
    def initialize(self, d_idx, d_p, d_wij, d_s00, d_s01, d_s02, d_s11, d_s12,
                   d_s22, d_make_acceleration_zero):
        if d_make_acceleration_zero[d_idx] == 1:
            d_s00[d_idx] = 0.0
            d_s01[d_idx] = 0.0
            d_s02[d_idx] = 0.0
            d_s11[d_idx] = 0.0
            d_s12[d_idx] = 0.0
            d_s22[d_idx] = 0.0
            d_p[d_idx] = 0.0
            d_wij[d_idx] = 0.0

    def loop(self, d_idx, d_wij, d_p, d_s00, d_s01, d_s02, d_s11, d_s12, d_s22,
             s_idx, s_s00, s_s01, s_s02, s_s11, s_s12, s_s22, s_p, WIJ,
             d_make_acceleration_zero, s_make_acceleration_zero):
        if d_make_acceleration_zero[d_idx] == 1:
            if s_make_acceleration_zero[s_idx] == 0:
                d_s00[d_idx] += s_s00[s_idx] * WIJ
                d_s01[d_idx] += s_s01[s_idx] * WIJ
                d_s02[d_idx] += s_s02[s_idx] * WIJ
                d_s11[d_idx] += s_s11[s_idx] * WIJ
                d_s12[d_idx] += s_s12[s_idx] * WIJ
                d_s22[d_idx] += s_s22[s_idx] * WIJ

                d_p[d_idx] += s_p[s_idx] * WIJ

                # denominator of Eq. (27)
                d_wij[d_idx] += WIJ

    def post_loop(self, d_wij, d_idx, d_s00, d_s01, d_s02, d_s11, d_s12, d_s22,
                  d_p, d_make_acceleration_zero):
        if d_make_acceleration_zero[d_idx] == 1:
            if d_wij[d_idx] > 1e-14:
                d_s00[d_idx] /= d_wij[d_idx]
                d_s01[d_idx] /= d_wij[d_idx]
                d_s02[d_idx] /= d_wij[d_idx]
                d_s11[d_idx] /= d_wij[d_idx]
                d_s12[d_idx] /= d_wij[d_idx]
                d_s22[d_idx] /= d_wij[d_idx]

                d_p[d_idx] /= d_wij[d_idx]


def create_circle(diameter=1, spacing=0.05, center=None):
    dx = spacing
    x = [0.0]
    y = [0.0]
    r = spacing
    nt = 0
    while r < diameter / 2:
        nnew = int(np.pi * r**2 / dx**2 + 0.5)
        tomake = nnew - nt
        theta = np.linspace(0., 2. * np.pi, tomake + 1)
        for t in theta[:-1]:
            x.append(r * np.cos(t))
            y.append(r * np.sin(t))
        nt = nnew
        r = r + dx
    x = np.array(x)
    y = np.array(y)
    x, y = (t.ravel() for t in (x, y))
    if center is None:
        return x, y
    else:
        return x + center[0], y + center[1]


class LVI(Application):
    def initialize(self):
        rho = 1.2 * 1e3
        e = 0.01 * 1e9
        nu = 0.49
        g = get_shear_modulus(e, nu)
        print("shear modulus is ", g)

        self.dx = 0.25 * 1e-3
        self.cylinder_radius = 5. * 1e-3
        self.cylinder_rho0 = rho
        self.cylinder_G = g
        self.cylinder_E = e
        self.cylinder_nu = nu

        self.structure_length = 2. * 1e-3
        self.structure_height = 50. * 1e-3
        self.structure_rho0 = rho
        self.structure_G = g
        self.structure_E = e
        self.structure_nu = nu

        self.fac = self.dx / 2.
        # self.kernel_fac = 3
        self.hdx = 1.3
        self.h = self.hdx * self.dx

        self.dt = 0.25 * self.h / (
            (self.structure_E / self.structure_rho0)**0.5 + 2.85)

        self.c0 = get_speed_of_sound(self.structure_E, self.structure_nu,
                                     self.structure_rho0)
        self.cylinder_u = 0.15 * self.c0

        print("speed of sound is")
        print(self.c0)
        self.pb = self.structure_rho0 * self.c0**2
        # self.pb = 0.
        print("timestep is,", self.dt)

        self.dim = 2

        self.artificial_vis_alpha = 1.0
        self.artificial_vis_beta = 0.0

        # self.alpha = 0.
        # self.beta = 0.

        self.artificial_stress_eps = 0.3

        self.normal_comupte_count = 1

        # for pre step
        self.seval = None

        # edac constants
        self.edac_alpha = 0.5
        self.edac_nu = self.edac_alpha * self.c0 * self.h / 8

        # attributes for Sun PST technique
        # dummy value, will be updated in consume user options
        self.u_max = self.cylinder_u
        self.mach_no = self.u_max / self.c0

        # boundary equations
        self.boundary_equations_structure = get_boundary_identification_etvf_equations(
            destinations=["structure"],  sources=[])
        self.boundary_equations_cylinder = get_boundary_identification_etvf_equations(
            destinations=["cylinder"],  sources=[])
        self.boundary_equations = self.boundary_equations_structure + self.boundary_equations_cylinder

    def create_particles(self):
        # create a structure
        xs, ys = np.mgrid[0.:self.structure_length +
                          self.dx / 2.:self.dx, 0.:self.structure_height +
                          self.dx / 2.:self.dx]
        xs = xs.ravel()
        ys = ys.ravel()

        m = self.structure_rho0 * self.dx**2.

        kernel = QuinticSpline(dim=2)
        self.wdeltap = kernel.kernel(rij=self.dx, h=self.h)
        # self.wdeltap = 0.0

        structure = get_particle_array(
            x=xs, y=ys, m=m, h=self.h, rho=self.structure_rho0,
            name="structure",
            constants={
                'E': self.structure_E,
                'n': 4,
                'nu': self.structure_nu,
                'spacing0': self.dx,
                'rho_ref': self.structure_rho0
            })

        # ------------------------
        # filter the indices of the structure whose accelerations needed
        # to be zero
        # ------------------------
        make_acceleration_zero = np.zeros(len(structure.x), dtype=int)

        for i in range(len(structure.x)):
            if structure.y[i] < 8. * self.dx or structure.y[i] > self.structure_height - 8. * self.dx:
                make_acceleration_zero[i] = 1

        structure.add_property("make_acceleration_zero", type='int')
        structure.make_acceleration_zero[:] = make_acceleration_zero[:]

        # for adamibc_eq
        structure.add_property("wij")

        # cylinder coordinates
        xc, yc = create_circle(
            2. * self.cylinder_radius, self.dx,
            [-self.dx / 2. - self.cylinder_radius, self.structure_height / 2.])

        cylinder = get_particle_array(
            x=xc, y=yc, m=m, h=self.h, rho=self.structure_rho0,
            name="cylinder",
            constants={
                'E': self.cylinder_E,
                'n': 4,
                'nu': self.cylinder_nu,
                'spacing0': self.dx,
                'rho_ref': self.cylinder_rho0
            })

        self.scheme.setup_properties([structure, cylinder])

        # TODO: FIXME, this function should only be used for etvf
        # but right now don't know how to eliminate the pre_step
        # for different schemes. So need to fix it.
        # add_boundary_identification_properties(cylinder)
        # structure.add_output_arrays(['is_boundary', 'h_b'])

        # cylinder.rho0[:] = self.cylinder_rho0
        cylinder.u[:] = self.cylinder_u

        # cylinder.set_lb_props(list(cylinder.properties.keys()))

        return [structure, cylinder]

    def create_equations(self):
        eqns = self.scheme.get_equations()

        # Adami boundary conditions
        adamibc_eq = []
        adamibc_eq.append(
            AdamiBoundaryConditionExtrapolateNoSlipFixedParticles(
                dest="structure", sources=["structure"]))

        eqns.groups[0].append(Group(adamibc_eq))

        # make accelerations zero on specific indices
        makeaccelzero_eq = []
        makeaccelzero_eq.append(MakeAccelerationZeroOnSelectedIndices(
            dest="structure", sources=None))

        eqns.groups[1].append(Group(makeaccelzero_eq))

        # print(eqns)
        return eqns

    def configure_scheme(self):
        dt = self.dt
        tf = 2 * 1e-3
        self.scheme.configure_solver(dt=dt, tf=tf, pfreq=50)

    def create_scheme(self):
        solid = SolidsScheme(solids=['structure',  'cylinder'],
                             boundaries=None,
                             dim=2,
                             pb=self.pb,
                             edac_nu=self.edac_nu,
                             u_max=self.u_max,
                             mach_no=self.mach_no,
                             h=self.h,
                             hdx=self.hdx)

        s = SchemeChooser(default='solid', solid=solid)
        return s

    def _make_accel_eval(self, equations, pa_arrays):
        if self.seval is None:
            kernel = QuinticSpline(dim=self.dim)
            seval = SPHEvaluator(arrays=pa_arrays, equations=equations,
                                 dim=self.dim, kernel=kernel)
            self.seval = seval
            return self.seval
        else:
            self.seval.update()
            return self.seval
        return seval

    def pre_step(self, solver):
        if self.options.pst in ['sun2019', 'ipst']:
            if solver.count % 1 == 0:
                t = solver.t
                dt = solver.dt

                arrays = self.particles
                a_eval = self._make_accel_eval(self.boundary_equations, arrays)

                # When
                a_eval.evaluate(t, dt)


if __name__ == '__main__':
    app = LVI()
    app.run()
