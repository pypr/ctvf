import numpy as np
from math import cos, sin, sinh, cosh

# SPH equations
from pysph.base.kernels import (QuinticSpline)
from pysph.solver.solver import Solver
from pysph.solver.application import Application
from pysph.tools.sph_evaluator import SPHEvaluator
from pysph.sph.scheme import SchemeChooser

from pysph.base.utils import get_particle_array
from pysph.sph.equation import Group, MultiStageEquations

from etvf import (get_particle_array_etvf_elastic_solid)

from pysph.sph.solid_mech.basic import (get_particle_array_elastic_dynamics,
                                        get_speed_of_sound)

from solid_mech import (SolidMechanicsScheme, GraySolidsScheme,
                        ETVFSolidsScheme, GTVFSolidsScheme,
                        get_poisson_ratio_from_E_G,
                        setup_particle_array_for_gray_solid,
                        setup_particle_array_for_etvf_solid,
                        setup_particle_array_for_gtvf_solid,
                        add_plasticity_properties,
                        ComputeJ2,
                        LimitDeviatoricStress)


from boundary_particles import (add_boundary_identification_properties,
                                get_boundary_identification_etvf_equations)


class TaylorBarImpact(Application):
    def initialize(self):
        self.bar_len = 0.76 * 1e-2
        self.bar_hei = 2.54 * 1e-2

        self.dx_bar = 5.058 * 1e-4
        # self.dx_bar = 1.058 * 1e-3
        self.fac = self.dx_bar / 2.
        self.kernel_fac = 3
        self.h = 1.2 * self.dx_bar
        self.bar_rho0 = 7.85 * 1e3
        self.bar_E = 208. * 1e9
        self.bar_G = 80. * 1e9
        self.bar_nu = get_poisson_ratio_from_E_G(self.bar_E, self.bar_G)
        self.yield_modulus = 600 * 1e6

        self.wall_layers = 3
        self.wall_len = self.bar_len * 5.

        # compute the timestep
        self.tf = 100. * 1e-6
        self.dt = 0.25 * self.h / (
            (self.bar_E / self.bar_rho0)**0.5 + 2.85)
        # self.dt = 0.25 * self.h / (
        #     (self.bar_E / self.bar_rho0)**0.5 + 2.85)

        self.c0 = get_speed_of_sound(self.bar_E, self.bar_nu,
                                     self.bar_rho0)
        self.pb = self.bar_rho0 * self.c0**2
        # self.pb = 0.
        print("timestep is,", self.dt)

        self.dim = 2

        self.artificial_vis_alpha = 1.0
        self.artificial_vis_beta = 0.0

        # self.alpha = 0.
        # self.beta = 0.

        self.artificial_stress_eps = 0.3

        # for pre step
        self.seval = None

        # boundary equations
        self.boundary_equations = get_boundary_identification_etvf_equations(
            destinations=["bar"], sources=["wall"])

    def create_particles(self):
        # bar coordinates
        xp, yp = np.mgrid[-self.bar_len/2.:self.bar_len/2. + self.dx_bar / 2.:self.dx_bar,
                          0.:self.bar_hei + self.dx_bar / 2.:self.dx_bar]
        xp = xp.ravel()
        yp = yp.ravel()

        yp = yp + self.bar_hei / 8.
        m = self.bar_rho0 * self.dx_bar**2.

        kernel = QuinticSpline(dim=2)
        self.wdeltap = kernel.kernel(rij=self.dx_bar, h=self.h)

        bar = get_particle_array(
            x=xp, y=yp, m=m, h=self.h, rho=self.bar_rho0, name="bar",)

        if self.options.scheme == "gray":
            setup_particle_array_for_gray_solid(bar, wdeltap=self.wdeltap, n=4,
                                                rho_ref=self.bar_rho0,
                                                E=self.bar_E,
                                                nu=self.bar_nu)

        if self.options.scheme == "etvf" or self.options.scheme == "etvf_new":
            setup_particle_array_for_etvf_solid(bar,
                                                rho_ref=self.bar_rho0,
                                                E=self.bar_E,
                                                nu=self.bar_nu)

        if self.options.scheme == "gtvf":
            setup_particle_array_for_gtvf_solid(bar,
                                                rho_ref=self.bar_rho0,
                                                E=self.bar_E,
                                                nu=self.bar_nu)

        # TODO: FIXME, this function should only be used for etvf
        # but right now don't know how to eliminate the pre_step
        # for different schemes. So need to fix it.
        add_boundary_identification_properties(bar)

        # This is to simulate plasticity. Applicable for all schemes
        add_plasticity_properties(bar)
        bar.v[:] = - 200.

        # ======================
        # wall particle array
        # ======================
        xw, yw = np.mgrid[-self.wall_len/2.:self.wall_len/2.:self.dx_bar,
                          0.:self.wall_layers * self.dx_bar:self.dx_bar]
        yw = yw - self.wall_layers * self.dx_bar - self.dx_bar / 2.


        # create the particle array
        wall = get_particle_array_etvf_elastic_solid(
            x=xw, y=yw, m=m, h=self.h, rho=self.bar_rho0, name="wall",
            constants=dict(E=self.bar_E, nu=self.bar_nu))
        # wall.rho0[:] = 1000.

        wall.set_lb_props(list(wall.properties.keys()))

        return [bar, wall]

    def configure_scheme(self):
        dt = self.dt
        tf = self.tf
        self.scheme.configure_solver(dt=dt, tf=tf, pfreq=500)

    def create_scheme(self):
        gray = GraySolidsScheme(
            solids=['bar'], boundaries=['wall'], dim=2,
            artificial_stress_eps=0.3,
            artificial_vis_alpha=self.artificial_vis_alpha,
            artificial_vis_beta=self.artificial_vis_beta)

        etvf = ETVFSolidsScheme(
            solids=['bar'], boundaries=['wall'], dim=2,
            use_uhat=True, h=self.h,
            kernel_factor=self.kernel_fac, pb=self.pb,
            artificial_vis_alpha=self.artificial_vis_alpha,
            artificial_vis_beta=self.artificial_vis_beta)

        gtvf = GTVFSolidsScheme(
            solids=['bar'], boundaries=['wall'], dim=2,
            use_uhat=True, pb=self.pb,
            artificial_vis_alpha=self.artificial_vis_alpha,
            artificial_vis_beta=self.artificial_vis_beta)

        s = SchemeChooser(default='etvf', etvf=etvf, etvf_new=etvf_new,
                          gtvf=gtvf, gray=gray)

        return s

    def create_equations(self):
        eqns = self.scheme.get_equations()

        # plasticity equations
        plastic_eq = []
        for solid in ["bar"]:
            plastic_eq.append(ComputeJ2(dest=solid, sources=None))
            plastic_eq.append(
                LimitDeviatoricStress(solid, sources=None,
                                      yield_modulus=self.yield_modulus))

        eqns.groups[1].insert(1, Group(plastic_eq))

        print(eqns)
        return eqns

    def _make_accel_eval(self, equations, pa_arrays):
        if self.seval is None:
            kernel = QuinticSpline(dim=self.dim)
            seval = SPHEvaluator(arrays=pa_arrays, equations=equations,
                                 dim=self.dim, kernel=kernel)
            self.seval = seval
            return self.seval
        else:
            self.seval.update()
            return self.seval
        return seval

    def pre_step(self, solver):
        if solver.count % 1 == 0:
            t = solver.t
            dt = solver.dt

            arrays = self.particles
            a_eval = self._make_accel_eval(self.boundary_equations, arrays)

            # When
            a_eval.evaluate(t, dt)


if __name__ == '__main__':
    app = TaylorBarImpact()
    app.run()
    # app.post_process(app.info_filename)
    # app.create_rings_geometry()
