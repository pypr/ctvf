"""Square fluid

Stability of ETVF scheme
"""
from __future__ import print_function
from numpy import ones_like, mgrid
import numpy as np
import os

# PySPH base and carray imports
# from pysph.base.utils import get_particle_array_wcsph
from pysph.base.kernels import Gaussian, QuinticSpline

from pysph.solver.application import Application
from pysph.solver.solver import Solver
from pysph.sph.integrator import EPECIntegrator
from pysph.sph.integrator_step import WCSPHStep

from pysph.tools.sph_evaluator import SPHEvaluator
from pysph.sph.basic_equations import XSPHCorrection, ContinuityEquation
from pysph.sph.wc.basic import TaitEOS, MomentumEquation
from pysph.sph.equation import Group, MultiStageEquations

from pysph.examples.elliptical_drop import EllipticalDrop as EDScheme

# tvf
from pysph.sph.wc.transport_velocity import (
    StateEquation, SetWallVelocity, SolidWallPressureBC, VolumeSummation,
    SolidWallNoSlipBC, MomentumEquationArtificialViscosity, ContinuitySolid)

# gtvf
from pysph.sph.wc.gtvf import (get_particle_array_gtvf, GTVFIntegrator,
                               GTVFStep, ContinuityEquationGTVF,
                               CorrectDensity,
                               MomentumEquationArtificialStress,
                               MomentumEquationViscosity)

# for normals
from pysph.sph.isph.wall_normal import ComputeNormals, SmoothNormals

# current paper imports for fluid flow
from boundary_particles import (add_boundary_identification_properties, IdentifyBoundaryParticle2,
                                IdentifyBoundaryParticleCosAngle)
from etvf import (MomentumEquationPressureGradientETVF, SetHIJForInsideParticles)


class SquareFluid(Application):
    def initialize(self):
        self.co = 1400.0
        self.rho = 1.0
        self.rho0 = 1.0
        self.p0 = self.co**2. * self.rho
        self.nu = 0.01
        self.hdx = 1.3
        self.dx = 0.025
        self.fac = self.dx / 2.
        self.h = self.hdx * self.dx
        self.kernel_factor = 3
        self.m0 = self.rho * self.dx**2.
        self.alpha = 0.1

        self.pb = self.p0

        self.seval = None
        self.dim = 2

    def create_particles(self):
        """Create the circular patch of fluid."""
        dx = self.dx
        hdx = self.hdx
        rho = self.rho
        name = 'fluid'
        x, y = mgrid[-1.05:1.05 + 1e-4:dx, -1.05:1.05 + 1e-4:dx]
        # Get the particles inside the circle.
        # condition = ~((x * x + y * y - 1.0) > 1e-10)
        # x = x[condition].ravel()
        # y = y[condition].ravel()

        m = ones_like(x) * dx * dx * rho
        h = ones_like(x) * hdx * dx
        rho = ones_like(x) * rho
        u = 0.
        v = 0.

        pa = get_particle_array_gtvf(x=x, y=y, m=m, rho=rho, h=h, u=u, v=v,
                                     name=name)
        pa.add_property('h_b')
        add_boundary_identification_properties(pa)

        print("Stationary Square fluid :: %d particles" %
              (pa.get_number_of_particles()))

        # add requisite variables needed for this formulation
        for name in ('arho', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'rho0', 'u0',
                     'v0', 'w0', 'x0', 'y0', 'z0'):
            pa.add_property(name)

        # set the output property arrays
        pa.set_output_arrays(
            ['x', 'y', 'u', 'v', 'rho', 'm', 'h', 'p', 'pid', 'tag', 'gid'])

        return [pa]

    def create_solver(self):
        kernel = QuinticSpline(dim=2)

        integrator = GTVFIntegrator(fluid=GTVFStep())

        dt = 5e-6
        tf = 0.0076
        solver = Solver(kernel=kernel, dim=2, integrator=integrator, dt=dt,
                        tf=tf)

        return solver

    def create_equations(self):

        stage1 = [
            Group(
                equations=[
                    ContinuityEquationGTVF(dest='fluid', sources=['fluid']),

                    # For ETVF
                    # ComputeNormals(dest='fluid', sources=['fluid'])
                ], ),
            # Group(
            #     equations=[
            #         # For ETVF
            #         SmoothNormals(dest='fluid', sources=['fluid'])
            #     ], ),

            # Group(equations=[
            #     # For ETVF
            #     IdentifyBoundaryParticleCosAngle(dest='fluid',
            #                                      sources=['fluid'])
            # ]),

            # don't use this.
            # Group(equations=[IdentifyBoundaryParticle2(dest='fluid', sources=['fluid'], fac=self.fac)]),

            Group(equations=[
                SetHIJForInsideParticles(dest='fluid', sources=[
                    'fluid'
                ], h=self.h, kernel_factor=self.kernel_factor)
            ]),
        ]

        stage2 = [
            Group(
                equations=[
                    CorrectDensity(dest='fluid', sources=['fluid']),

                ], ),
            Group(
                equations=[
                    StateEquation(dest='fluid', sources=None, p0=self.p0,
                                  rho0=self.rho0, b=1.0)
                ], ),
            Group(
                equations=[
                    MomentumEquationPressureGradientETVF(
                        dest='fluid', sources=['fluid'], pb=self.pb,
                        rho=self.rho),
                    MomentumEquationViscosity(dest='fluid', sources=['fluid'],
                                              nu=self.nu),
                    MomentumEquationArtificialStress(dest='fluid',
                                                     sources=['fluid'], dim=2)
                ], ),

            # for ETVF
            # Group(
            #     equations=[
            #         PsuedoForceOnFreeSurface(dest='fluid', sources=['fluid'],
            #                                  dx=self.dx, m0=self.m0,
            #                                  pb=self.pb, rho=self.rho0)
            #     ], ),
        ]
        return MultiStageEquations([stage1, stage2])

    def _make_accel_eval(self, equations, pa_arrays):
        if self.seval is None:
            kernel = QuinticSpline(dim=self.dim)
            seval = SPHEvaluator(arrays=pa_arrays, equations=equations,
                                 dim=self.dim, kernel=kernel)
            self.seval = seval
            return self.seval
        else:
            return self.seval
        return seval

    def pre_step(self, solver):
        if solver.count % 10 == 0:
            t = solver.t
            dt = solver.dt

            eqs = [
                # ------------------------
                # compute the normals
                Group(
                    equations=[
                        # For ETVF
                        ComputeNormals(dest='fluid', sources=['fluid'])
                    ], ),
                Group(
                    equations=[
                        # For ETVF
                        SmoothNormals(dest='fluid', sources=['fluid'])
                    ], ),
                Group(equations=[
                    IdentifyBoundaryParticleCosAngle(dest='fluid',
                                                     sources=['fluid'])
                ]),
                # Group(equations=[
                #     IdentifyBoundaryParticle2(dest='plate', sources=['plate'],
                #                               fac=self.fac)
                # ]),
                # compute the normals ends
                # ------------------------
            ]
            arrays = self.particles
            a_eval = self._make_accel_eval(eqs, arrays)

            # When
            a_eval.evaluate(t, dt)
        # pass

if __name__ == '__main__':
    app = SquareFluid()
    app.run()


# Semi-major axis length (exact, computed) = 1.9445172415576217, 1.9567269643753342
