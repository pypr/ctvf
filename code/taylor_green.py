"""WARNING: All the code below is meant for experimentation, use elsewhere with
caution.

Experimenting with three cases:

1. Normal TVF implementation
2. Static particles
3. Damped u' velocity.

The following options are checked for each:

- Correction to ME
- Use of continuity equation with and w/o correction.
- Using the new equations.

"""

from compyle.api import declare
from pysph.sph.scheme import TVFScheme, add_bool_argument
from pysph.sph.equation import Equation, Group, MultiStageEquations
from pysph.sph.integrator_step import IntegratorStep
from pysph.examples import taylor_green as TG
from pysph.sph.wc.gtvf import GTVFIntegrator


class ContinuityEquationGTVF1(Equation):
    def initialize(self, d_arho, d_idx):
        d_arho[d_idx] = 0.0

    def loop(self, d_idx, s_idx, s_m, d_rho, s_rho, d_uhat, d_vhat, d_what,
             s_uhat, s_vhat, s_what, d_arho, d_up, d_vp, d_wp,
             s_up, s_vp, s_wp, DWIJ):
        uhatij = d_uhat[d_idx] - s_uhat[s_idx]
        vhatij = d_vhat[d_idx] - s_vhat[s_idx]
        whatij = d_what[d_idx] - s_what[s_idx]
        upij = d_rho[d_idx]*d_up[d_idx] - s_rho[s_idx]*s_up[s_idx]
        vpij = d_rho[d_idx]*d_vp[d_idx] - s_rho[s_idx]*s_vp[s_idx]
        wpij = d_rho[d_idx]*d_wp[d_idx] - s_rho[s_idx]*s_wp[s_idx]

        udotdij = DWIJ[0]*uhatij + DWIJ[1]*vhatij + DWIJ[2]*whatij
        updotdwij = DWIJ[0]*upij + DWIJ[1]*vpij + DWIJ[2]*wpij
        fac = s_m[s_idx] / s_rho[s_idx]
        d_arho[d_idx] += fac * d_rho[d_idx] * udotdij - fac * updotdwij


class MEPGGTVF(Equation):
    def __init__(self, dest, sources, pref, gx=0.0, gy=0.0, gz=0.0):
        self.pref = pref
        self.gx = gx
        self.gy = gy
        self.gz = gz
        super().__init__(dest, sources)

    def initialize(self, d_idx, d_au, d_av, d_aw, d_auhat, d_avhat, d_awhat,
                   d_p):
        d_au[d_idx] = self.gx
        d_av[d_idx] = self.gy
        d_aw[d_idx] = self.gz

        d_auhat[d_idx] = 0.0
        d_avhat[d_idx] = 0.0
        d_awhat[d_idx] = 0.0

    def loop(self, d_rho, s_rho, d_idx, s_idx, d_p, s_p, s_m, d_au, d_av,
             d_aw, DWIJ, d_auhat, d_avhat, d_awhat):
        rhoi2 = d_rho[d_idx] * d_rho[d_idx]
        rhoj2 = s_rho[s_idx] * s_rho[s_idx]

        pij = d_p[d_idx]/rhoi2 + s_p[s_idx]/rhoj2

        tmp = -s_m[s_idx] * pij

        d_au[d_idx] += tmp * DWIJ[0]
        d_av[d_idx] += tmp * DWIJ[1]
        d_aw[d_idx] += tmp * DWIJ[2]

        tmp = -self.pref * s_m[s_idx]/rhoi2

        d_auhat[d_idx] += tmp * DWIJ[0]
        d_avhat[d_idx] += tmp * DWIJ[1]
        d_awhat[d_idx] += tmp * DWIJ[2]


class DampUPrime(Equation):
    def __init__(self, dest, sources, factor):
        self.factor = factor
        super().__init__(dest, sources)

    def post_loop(self, d_idx, d_auhat, d_avhat, d_awhat, d_up, d_vp, d_wp,
                  dt):
        factor = self.factor/dt
        d_auhat[d_idx] -= factor*d_up[d_idx]
        d_avhat[d_idx] -= factor*d_vp[d_idx]
        d_awhat[d_idx] -= factor*d_wp[d_idx]


class SetUpZero(Equation):
    def post_loop(self, d_idx, d_up, d_vp, d_wp):
        d_up[d_idx] = 0.0
        d_vp[d_idx] = 0.0
        d_wp[d_idx] = 0.0


class StationaryParticles(Equation):
    def initialize(self, d_idx, d_up, d_vp, d_wp, d_u, d_v, d_w,
                   d_uhat, d_vhat, d_what):
        d_up[d_idx] = -d_u[d_idx]
        d_vp[d_idx] = -d_v[d_idx]
        d_wp[d_idx] = -d_w[d_idx]
        d_uhat[d_idx] = 0.0
        d_vhat[d_idx] = 0.0
        d_what[d_idx] = 0.0


class CorrectionUp(Equation):
    def loop(self, d_idx, d_up, d_vp, d_wp, s_u, s_v, s_w, d_au, d_av, d_aw,
             s_idx, s_m, s_rho, s_up, s_vp, s_wp, d_rho, DWIJ):
        rj = s_rho[s_idx]
        vijdotdwij = ((d_up[d_idx] - s_up[s_idx])*DWIJ[0] +
                      (d_vp[d_idx] - s_vp[s_idx])*DWIJ[1] +
                      (d_wp[d_idx] - s_wp[s_idx])*DWIJ[2])
        Vj = s_m[s_idx]/rj

        # ri = d_rho[d_idx]
        # vijdotdwij = ((d_up[d_idx]*ri - s_up[s_idx]*rj)*DWIJ[0] +
        #               (d_vp[d_idx]*ri - s_vp[s_idx]*rj)*DWIJ[1] +
        #               (d_wp[d_idx]*ri - s_wp[s_idx]*rj)*DWIJ[2])
        # Vj = s_m[s_idx]/(rj*ri)
        d_au[d_idx] += s_u[s_idx]*Vj*vijdotdwij
        d_av[d_idx] += s_v[s_idx]*Vj*vijdotdwij
        d_aw[d_idx] += s_w[s_idx]*Vj*vijdotdwij


class CorrectionUhat(Equation):
    def loop(self, d_idx, s_u, s_v, s_w, d_au, d_av, d_aw,
             d_uhat, d_vhat, d_what, s_uhat, s_vhat, s_what,
             s_idx, s_m, s_rho, s_up, s_vp, s_wp, DWIJ):
        vijdotdwij = ((d_uhat[d_idx] - s_uhat[s_idx])*DWIJ[0] +
                      (d_vhat[d_idx] - s_vhat[s_idx])*DWIJ[1] +
                      (d_what[d_idx] - s_what[s_idx])*DWIJ[2])
        Vj = s_m[s_idx]/s_rho[s_idx]
        d_au[d_idx] += s_u[s_idx]*Vj*vijdotdwij
        d_av[d_idx] += s_v[s_idx]*Vj*vijdotdwij
        d_aw[d_idx] += s_w[s_idx]*Vj*vijdotdwij


class TVFStep(IntegratorStep):
    def initialize(self):
        pass

    def stage1(self, d_idx, d_u, d_v, d_w, d_au, d_av, d_aw, d_uhat, d_auhat,
               d_vhat, d_avhat, d_what, d_awhat, d_up, d_vp, d_wp, d_rho,
               d_arho, d_x, d_y, d_z, dt):
        dtb2 = 0.5*dt

        d_u[d_idx] += dtb2*d_au[d_idx]
        d_v[d_idx] += dtb2*d_av[d_idx]
        d_w[d_idx] += dtb2*d_aw[d_idx]

        d_up[d_idx] += dtb2*d_auhat[d_idx]
        d_vp[d_idx] += dtb2*d_avhat[d_idx]
        d_wp[d_idx] += dtb2*d_awhat[d_idx]

        d_uhat[d_idx] = d_u[d_idx] + d_up[d_idx]
        d_vhat[d_idx] = d_v[d_idx] + d_vp[d_idx]
        d_what[d_idx] = d_w[d_idx] + d_wp[d_idx]

        d_x[d_idx] += dt*d_uhat[d_idx]
        d_y[d_idx] += dt*d_vhat[d_idx]
        d_z[d_idx] += dt*d_what[d_idx]

    def stage2(self, d_idx, d_u, d_v, d_w, d_au, d_av, d_aw, d_vmag2,
               d_up, d_vp, d_wp, d_auhat, d_avhat, d_awhat,
               d_rho, d_arho, dt):
        dtb2 = 0.5*dt
        d_rho[d_idx] += dt*d_arho[d_idx]
        d_u[d_idx] += dtb2*d_au[d_idx]
        d_v[d_idx] += dtb2*d_av[d_idx]
        d_w[d_idx] += dtb2*d_aw[d_idx]

        d_vmag2[d_idx] = (d_u[d_idx]*d_u[d_idx] + d_v[d_idx]*d_v[d_idx] +
                          d_w[d_idx]*d_w[d_idx])


class TVFStepStationary(IntegratorStep):
    def initialize(self):
        pass

    def stage1(self, d_idx, d_u, d_v, d_w, d_au, d_av, d_aw, d_uhat, d_auhat,
               d_vhat, d_avhat, d_what, d_awhat, d_up, d_vp, d_wp, d_rho,
               d_arho, d_x, d_y, d_z, dt):
        dtb2 = 0.5*dt

        d_u[d_idx] += dtb2*d_au[d_idx]
        d_v[d_idx] += dtb2*d_av[d_idx]
        d_w[d_idx] += dtb2*d_aw[d_idx]

        d_up[d_idx] = -d_u[d_idx]
        d_vp[d_idx] = -d_v[d_idx]
        d_wp[d_idx] = -d_w[d_idx]

        d_uhat[d_idx] = 0.0
        d_vhat[d_idx] = 0.0
        d_what[d_idx] = 0.0

        d_x[d_idx] += dt*d_uhat[d_idx]
        d_y[d_idx] += dt*d_vhat[d_idx]
        d_z[d_idx] += dt*d_what[d_idx]

    def stage2(self, d_idx, d_u, d_v, d_w, d_au, d_av, d_aw, d_vmag2,
               d_up, d_vp, d_wp, d_auhat, d_avhat, d_awhat,
               d_rho, d_arho, dt):
        dtb2 = 0.5*dt
        d_rho[d_idx] += dt*d_arho[d_idx]
        d_u[d_idx] += dtb2*d_au[d_idx]
        d_v[d_idx] += dtb2*d_av[d_idx]
        d_w[d_idx] += dtb2*d_aw[d_idx]

        d_vmag2[d_idx] = (d_u[d_idx]*d_u[d_idx] + d_v[d_idx]*d_v[d_idx] +
                          d_w[d_idx]*d_w[d_idx])


class GTVFStep(IntegratorStep):
    def stage1(self, d_idx, d_u, d_v, d_w, d_au, d_av, d_aw, d_uhat, d_vhat,
               d_what, d_auhat, d_avhat, d_awhat, d_up, d_vp, d_wp, dt):
        dtb2 = 0.5*dt
        d_u[d_idx] += dtb2*d_au[d_idx]
        d_v[d_idx] += dtb2*d_av[d_idx]
        d_w[d_idx] += dtb2*d_aw[d_idx]

        d_up[d_idx] += dtb2*d_auhat[d_idx]
        d_vp[d_idx] += dtb2*d_avhat[d_idx]
        d_wp[d_idx] += dtb2*d_awhat[d_idx]

        d_uhat[d_idx] = d_u[d_idx] + d_up[d_idx]
        d_vhat[d_idx] = d_v[d_idx] + d_vp[d_idx]
        d_what[d_idx] = d_w[d_idx] + d_wp[d_idx]

    def stage2(self, d_idx, d_uhat, d_vhat, d_what, d_x, d_y, d_z, d_rho,
               d_arho, dt):
        d_rho[d_idx] += dt*d_arho[d_idx]

        d_x[d_idx] += dt*d_uhat[d_idx]
        d_y[d_idx] += dt*d_vhat[d_idx]
        d_z[d_idx] += dt*d_what[d_idx]

    def stage3(self, d_idx, d_u, d_v, d_w, d_au, d_av, d_aw, d_vmag2, dt):
        dtb2 = 0.5*dt
        d_u[d_idx] += dtb2*d_au[d_idx]
        d_v[d_idx] += dtb2*d_av[d_idx]
        d_w[d_idx] += dtb2*d_aw[d_idx]
        d_vmag2[d_idx] = (d_u[d_idx]*d_u[d_idx] + d_v[d_idx]*d_v[d_idx] +
                          d_w[d_idx]*d_w[d_idx])


class GTVFStepStationary(IntegratorStep):
    def stage1(self, d_idx, d_u, d_v, d_w, d_au, d_av, d_aw, d_uhat, d_vhat,
               d_what, d_auhat, d_avhat, d_awhat, d_up, d_vp, d_wp, dt):
        dtb2 = 0.5*dt
        d_u[d_idx] += dtb2*d_au[d_idx]
        d_v[d_idx] += dtb2*d_av[d_idx]
        d_w[d_idx] += dtb2*d_aw[d_idx]

        d_up[d_idx] = -d_u[d_idx]
        d_vp[d_idx] = -d_v[d_idx]
        d_wp[d_idx] = -d_w[d_idx]

        d_uhat[d_idx] = 0.0
        d_vhat[d_idx] = 0.0
        d_what[d_idx] = 0.0

    def stage2(self, d_idx, d_uhat, d_vhat, d_what, d_x, d_y, d_z, d_rho,
               d_arho, dt):
        d_rho[d_idx] += dt*d_arho[d_idx]

        d_x[d_idx] += dt*d_uhat[d_idx]
        d_y[d_idx] += dt*d_vhat[d_idx]
        d_z[d_idx] += dt*d_what[d_idx]

    def stage3(self, d_idx, d_u, d_v, d_w, d_au, d_av, d_aw, d_vmag2, dt):
        dtb2 = 0.5*dt
        d_u[d_idx] += dtb2*d_au[d_idx]
        d_v[d_idx] += dtb2*d_av[d_idx]
        d_w[d_idx] += dtb2*d_aw[d_idx]
        d_vmag2[d_idx] = (d_u[d_idx]*d_u[d_idx] + d_v[d_idx]*d_v[d_idx] +
                          d_w[d_idx]*d_w[d_idx])


class ETVFScheme(TVFScheme):
    """WARNING:

    This scheme is not designed to be very generic and is merely a convenience
    for the Taylor Green problem that is explored below.

    """
    def __init__(self, fluids, solids, dim, rho0, c0, nu, p0, pb, h0,
                 gx=0.0, gy=0.0, gz=0.0, alpha=0.0, tdamp=0.0,
                 damp_factor=1.0, correction='up', up='damp',
                 continuity_eq=False):
        super().__init__(
            fluids, solids, dim, rho0, c0, nu, p0, pb, h0, gx, gy, gz,
            alpha, tdamp
        )
        self.damp_factor = damp_factor
        self.correction = correction
        self.up = up
        self.continuity_eq = continuity_eq

    def add_user_options(self, group):
        super().add_user_options(group)
        group.add_argument(
            "--damp-factor", action="store", type=float, dest="damp_factor",
            default=None,
            help="Factor to use when damping the homogenization velocity."
        )
        choices = ['none', 'up', 'uhat']
        group.add_argument(
            "--correction", action="store", dest="correction",
            default=self.correction, choices=choices,
            help="Specify correction to use (one of %s)." % choices
        )
        choices = ['damp', 'zero', 'static']
        group.add_argument(
            "--up", action="store", dest="up",
            default=self.up, choices=choices,
            help="Specify setup of up (one of %s)." % choices
        )
        add_bool_argument(
            group, "continuity-eq", dest="continuity_eq",
            help="Use continuity instead of summation density.",
            default=None
        )

    def consume_user_options(self, options):
        vars = ['alpha', 'tdamp', 'damp_factor', 'correction', 'up',
                'continuity_eq']
        data = dict((var, self._smart_getattr(options, var))
                    for var in vars)
        self.configure(**data)

    def get_equations(self):
        if self.continuity_eq:
            return self._get_gtvf_equations()
        else:
            return self._get_tvf_equations()

    def _get_gtvf_equations(self):
        from pysph.sph.wc.transport_velocity import StateEquation
        from pysph.sph.wc.gtvf import (
            ContinuityEquationGTVF,
            MomentumEquationArtificialStress,
            MomentumEquationViscosity
        )
        all = self.fluids + self.solids
        stage1 = []
        if self.correction == 'none':
            CE = ContinuityEquationGTVF
        elif self.correction == 'up':
            CE = ContinuityEquationGTVF1
        else:
            raise RuntimeError("Unsupported correction, %s" % self.correction)
        eqs = [CE(dest=fluid, sources=all) for fluid in self.fluids]
        stage1.append(Group(equations=eqs, real=False))

        stage2 = []
        eqs = [StateEquation(dest=fluid, sources=None, p0=self.p0,
                             rho0=self.rho0, b=1.0) for fluid in self.fluids]
        stage2.append(Group(equations=eqs, real=False))

        eqs = []
        for fluid in self.fluids:
            eqs.extend([
                MEPGGTVF(
                    dest=fluid, sources=all, pref=self.p0,
                    gx=self.gx, gy=self.gy, gz=self.gz
                ),
                MomentumEquationViscosity(
                    dest=fluid, sources=all, nu=self.nu
                ),
                MomentumEquationArtificialStress(
                    dest=fluid, sources=self.fluids, dim=self.dim
                )
            ])
            if self.correction == 'up':
                eqs.append(CorrectionUp(dest=fluid, sources=self.fluids))
            if self.up == 'damp':
                eqs.append(DampUPrime(dest=fluid, sources=None,
                                      factor=self.damp_factor))
            elif self.up == 'zero':
                eqs.append(SetUpZero(dest=fluid, sources=None))
            elif self.up == 'static':
                eqs.append(StationaryParticles(dest=fluid, sources=None))

        stage2.append(Group(equations=eqs, real=True))
        return MultiStageEquations([stage1, stage2])

    def _get_tvf_equations(self):
        g = super().get_equations()
        g0 = g[0]
        g0_eqs = []
        eqs = []
        for fluid in self.fluids:
            if self.up == 'damp':
                eqs.append(DampUPrime(dest=fluid, sources=None,
                                      factor=self.damp_factor))
            elif self.up == 'zero':
                eqs.append(SetUpZero(dest=fluid, sources=None))
            elif self.up == 'static':
                eqs.append(StationaryParticles(dest=fluid, sources=None))

            if self.correction == 'up':
                eqs.append(CorrectionUp(dest=fluid, sources=self.fluids))
            elif self.correction == 'uhat':
                eqs.append(CorrectionUhat(dest=fluid, sources=self.fluids))
        g0.equations.extend(g0_eqs)
        g[-1].equations.extend(eqs)
        return g

    def setup_properties(self, particles, clean=True):
        super().setup_properties(particles, clean)
        pas = dict([(p.name, p) for p in particles])
        for fluid in self.fluids:
            pa = pas[fluid]
            props = ['up', 'vp', 'wp', 'arho']
            for prop in props:
                pa.add_property(prop)
            pa.add_output_arrays(['up', 'vp', 'wp'])

    def configure_solver(self, kernel=None, integrator_cls=None,
                         extra_steppers=None, **kw):
        from pysph.base.kernels import QuinticSpline
        from pysph.sph.integrator import PECIntegrator
        if kernel is None:
            kernel = QuinticSpline(dim=self.dim)
        steppers = {}
        if extra_steppers is not None:
            steppers.update(extra_steppers)

        if self.continuity_eq:
            if self.up == 'static':
                step_cls = GTVFStepStationary
            else:
                step_cls = GTVFStep
        elif self.up in ['zero', 'damp']:
            step_cls = TVFStep
        elif self.up in ['static']:
            step_cls = TVFStepStationary
        else:
            raise ValueError('Invalid option for "up": %s' % self.up)

        for fluid in self.fluids:
            if fluid not in steppers:
                steppers[fluid] = step_cls()

        Integrator = GTVFIntegrator if self.continuity_eq else PECIntegrator
        cls = integrator_cls if integrator_cls is not None else Integrator
        integrator = cls(**steppers)

        from pysph.solver.solver import Solver
        self.solver = Solver(
            dim=self.dim, integrator=integrator, kernel=kernel, **kw
        )


class MyTaylorGreen(TG.TaylorGreen):
    def create_scheme(self):
        h0 = None
        etvf = ETVFScheme(
            ['fluid'], [], dim=2, rho0=TG.rho0, c0=TG.c0, nu=None,
            p0=TG.p0, pb=None, h0=h0
        )
        schemes = super().create_scheme()
        schemes.schemes['etvf'] = etvf
        schemes.default = 'etvf'
        return schemes

    def configure_scheme(self):
        h0 = self.hdx * self.dx
        scheme = self.scheme
        if self.options.scheme == 'etvf':
            scheme.configure(pb=self.options.pb_factor * TG.p0, nu=self.nu,
                             h0=h0)
        super().configure_scheme()


if __name__ == '__main__':
    app = MyTaylorGreen()
    app.run()
    app.post_process(app.info_filename)
