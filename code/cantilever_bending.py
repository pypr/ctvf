"""Elastic Vibration of a Cantilever Beam

# Taken from


1. Nonlinear transient analysis of isotropic and composite shell structures
under dynamic loading by SPH method. Jun Lin thesis

1. Updated Smoothed Particle Hydrodynamics forSimulating Bending and Compression FailureProgress of Ice
doi:10.3390/w9110882


2. A numerical study on ice failure process and ice-ship interactions by Smoothed Particle Hydrodynamics
https://doi.org/10.1016/j.ijnaoe.2019.02.008

"""
import numpy as np
from math import cos, sin, sinh, cosh
import os

# SPH equations
from pysph.base.kernels import (QuinticSpline)
from pysph.solver.solver import Solver
from pysph.solver.application import Application
from pysph.tools.sph_evaluator import SPHEvaluator
from pysph.sph.equation import Group, MultiStageEquations, BasicCodeBlock
from pysph.sph.scheme import SchemeChooser
from pysph.sph.equation import Equation

from pysph.base.utils import get_particle_array

from solid_mech import SetHIJForInsideParticles, SolidsScheme

from boundary_particles import (add_boundary_identification_properties,
                                get_boundary_identification_etvf_equations)

from pysph.sph.solid_mech.basic import (get_speed_of_sound, get_bulk_mod,
                                        get_shear_modulus)
from pysph.tools.geometry import get_2d_tank, get_2d_block
from pysph.sph.scheme import add_bool_argument

from compyle.api import declare
from pysph.sph.equation import Equation, Group, BasicCodeBlock
from pysph.sph.wc.linalg import gj_solve, augmented_matrix, identity
from mako.template import Template


def get_fixed_beam(beam_length, beam_height, beam_inside_length,
                   boundary_layers, spacing):
    """
 |||=============
 |||=============
 |||===================================================================|
 |||===================================================================|Beam height
 |||===================================================================|
 |||=============
 |||=============
   <------------><---------------------------------------------------->
      Beam inside                   Beam length
      length
    """
    import matplotlib.pyplot as plt
    # create a block first
    xb, yb = get_2d_block(dx=spacing, length=beam_length + beam_inside_length,
                          height=beam_height)

    # create a (support) block with required number of layers
    xs1, ys1 = get_2d_block(dx=spacing, length=beam_inside_length,
                            height=boundary_layers * spacing)
    xs1 += np.min(xb) - np.min(xs1)
    ys1 += np.min(yb) - np.max(ys1) - spacing

    # create a (support) block with required number of layers
    xs2, ys2 = get_2d_block(dx=spacing, length=beam_inside_length,
                            height=boundary_layers * spacing)
    xs2 += np.min(xb) - np.min(xs2)
    ys2 += np.max(ys2) - np.min(yb) + spacing

    xs = np.concatenate([xs1, xs2])
    ys = np.concatenate([ys1, ys2])

    xs3, ys3 = get_2d_block(dx=spacing, length=boundary_layers * spacing,
                            height=np.max(ys) - np.min(ys))
    xs3 += np.min(xb) - np.max(xs3) - 1. * spacing
    # ys3 += np.max(ys2) - np.min(yb) + spacing

    xs = np.concatenate([xs, xs3])
    ys = np.concatenate([ys, ys3])
    # plt.scatter(xs, ys, s=1)
    # plt.scatter(xb, yb, s=1)
    # plt.axes().set_aspect('equal', 'datalim')
    # plt.savefig("geometry", dpi=300)
    # plt.show()

    return xb, yb, xs, ys


class ApplyForceGradual(Equation):
    def __init__(self, dest, sources, delta_fx=0, delta_fy=0, delta_fz=0):
        self.force_increment_x = delta_fx
        self.force_increment_y = delta_fy
        self.force_increment_z = delta_fz
        super(ApplyForceGradual, self).__init__(dest, sources)

    def initialize(self, d_idx, d_au, d_av, d_aw, d_final_force_time,
                   d_total_force_applied_x, d_total_force_applied_y,
                   d_total_force_applied_z, d_force_idx, d_m, t):
        if t < d_final_force_time[0]:
            if d_idx == 0:
                d_total_force_applied_x[0] += self.force_increment_x
                d_total_force_applied_y[0] += self.force_increment_y
                d_total_force_applied_z[0] += self.force_increment_z

        if d_force_idx[d_idx] == 1:
            d_au[d_idx] += d_total_force_applied_x[0] / d_m[d_idx]
            d_av[d_idx] += d_total_force_applied_y[0] / d_m[d_idx]
            d_aw[d_idx] += d_total_force_applied_z[0] / d_m[d_idx]


class ApplyDampingForce(Equation):
    def __init__(self, dest, sources, c=0.1):
        self.c = c
        super(ApplyDampingForce, self).__init__(dest, sources)

    def initialize(self, d_idx, d_u, d_v, d_w, d_au, d_av, d_aw, d_m, t):
        d_au[d_idx] -= self.c * d_u[d_idx] / d_m[d_idx]
        d_av[d_idx] -= self.c * d_v[d_idx] / d_m[d_idx]
        d_aw[d_idx] -= self.c * d_w[d_idx] / d_m[d_idx]


def plot_vertical_displacement_of_midway_of_beam_y_0(folder, length, height,
                                                     deflection, file_end=-5):
    # -----------------------------
    # Now get the theoretical value
    from pysph.tools.geometry import get_2d_block
    import matplotlib.tri as tri
    import matplotlib.pyplot as plt
    from pysph.sph.solid_mech.basic import (get_speed_of_sound, get_bulk_mod,
                                            get_shear_modulus)
    from pysph.solver.utils import load, get_files

    E = 210 * 1e9
    nu = 0.3
    G = get_shear_modulus(E, nu)

    deflection = deflection

    H = height
    L = length
    I = H**3. / 12.
    F = deflection * 3. * E * I / L**3.
    print("force applied is")
    print(F)

    # plot the vert displacemet (v) at midline
    x = np.linspace(0., L, 1000)

    tmp1 = (F * x**3) / (6 * E * I)
    tmp2 = (F * L**2 * x) / (2 * E * I)
    tmp3 = (F * L**3) / (3 * E * I)
    tmp4 = (F * H**2) / (8 * G * I) * (L - x)

    vert_disp = tmp1 - tmp2 + tmp3 + tmp4

    plt.clf()
    plt.plot(-x - (min(-x)), vert_disp, label="Theoretical vertical displacement")
    # fig.suptitle('Variation of sigma_x at x=L/2 with variation in y', fontsize=20)
    plt.xlabel('x', fontsize=18)
    plt.ylabel('vertical displacement', fontsize=16)
    plt.legend()

    files = get_files(folder)
    data = load(files[0])
    # solver_data = data['solver_data']
    arrays = data['arrays']
    pa = arrays['plate']
    indices_mid_way = np.where(pa.indices_mid_way == 1)

    last_file = files[file_end]
    data = load(last_file)
    # solver_data = data['solver_data']
    arrays = data['arrays']
    pa = arrays['plate']
    x = pa.x[indices_mid_way[0]]
    sph_y = pa.y[indices_mid_way[0]]
    # print("maximum of sigma_00 is ", max(sigma_00)*1e-6, ' mpa')
    label = 'x_vs_y with length ' + str(length) + ' and a deflection of ' + str(deflection)

    plt.plot(x, sph_y, label="SPH vertical displacement")
    # plt.savefig("fuck")
    plt.legend()
    plt.show()


class CantileverBeamDeflectionWithTipload(Application):
    def add_user_options(self, group):
        group.add_argument("--rho", action="store", type=float, dest="rho",
                           default=7800.,
                           help="Density of the particle (Defaults to 7800.)")

        group.add_argument(
            "--Vf", action="store", type=float, dest="Vf", default=0.05,
            help="Velocity of the plate (Vf) (Defaults to 0.05)")

        group.add_argument("--length", action="store", type=float,
                           dest="length", default=0.1,
                           help="Length of the plate")

        group.add_argument("--height", action="store", type=float,
                           dest="height", default=0.01,
                           help="height of the plate")

        group.add_argument("--deflection", action="store", type=float,
                           dest="deflection", default=1e-4,
                           help="Deflection of the plate")

        group.add_argument("--N", action="store", type=int, dest="N",
                           default=10,
                           help="No of particles in the height direction")

        group.add_argument("--final-force-time", action="store", type=float,
                           dest="final_force_time", default=1e-3,
                           help="Total time taken to apply the external load")

        group.add_argument("--damping-c", action="store", type=float,
                           dest="damping_c", default=0.1,
                           help="Damping constant in damping force")

        group.add_argument("--material", action="store", type=str,
                           dest="material", default="steel",
                           help="Material of the plate")

    def consume_user_options(self):
        self.L = self.options.length
        self.H = self.options.height
        self.N = self.options.N
        self.damping_c = self.options.damping_c

        if self.options.material == "steel":
            self.plate_rho0 = 7800
            self.plate_E = 210 * 1e9
            self.plate_nu = 0.3
        elif self.options.material == "rubber":
            self.plate_rho0 = 1000
            self.plate_E = 2 * 1e6
            self.plate_nu = 0.3975

        elif self.options.material == "aluminium":
            self.plate_rho0 = 2800
            self.plate_G = 26.5 * 1e6
            self.plate_K = 69 * 1e6
            self.plate_nu = (3. * self.plate_K - 2. * self.plate_G) / (
                6. * self.plate_K + 2. * self.plate_G)
            self.plate_E = 2. * self.plate_G * (1 + self.plate_nu)

        self.c0 = get_speed_of_sound(self.plate_E, self.plate_nu,
                                     self.plate_rho0)
        # self.c0 = 5960
        # print("speed of sound is")
        # print(self.c0)
        self.pb = self.plate_rho0 * self.c0**2

        self.edac_alpha = 0.5
        self.hdx = 1.2

        self.dx_plate = self.H / self.N
        self.h = self.hdx * self.dx_plate
        self.edac_nu = self.edac_alpha * self.c0 * self.h / 8

        # attributes for Sun PST technique
        # dummy value, will be updated in consume user options
        self.u_max = 13
        self.mach_no = self.u_max / self.c0

        # force to be applied
        # self.fx = 0
        # self.fy = 262.5 * 1e3
        # self.fz = 0

        self.delta_fx = 0.
        self.delta_fy = 0.
        self.delta_fz = 0.

        # for pre step
        self.seval = None

        # boundary equations
        # self.boundary_equations = get_boundary_identification_etvf_equations(
        # destinations=["plate"], sources=["plate"])
        self.boundary_equations = get_boundary_identification_etvf_equations(
            destinations=["plate"], sources=["plate", "wall"],
            boundaries=["wall"])

        self.wall_layers = 2

        # set the total time of the simulation
        if self.options.material == "steel":
            self.tf = 2e-3
            self.final_force_time = 1e-3

        elif self.options.material == "rubber":
            self.tf = 10e-01
            self.final_force_time = 2e-02

        elif self.options.material == "aluminium":
            self.tf = 10e-01
            self.final_force_time = 2e-02

        self.dt = 0.25 * self.h / (
            (self.plate_E / self.plate_rho0)**0.5 + self.u_max)

        print("timestep is")
        print(self.dt)
        print("total time to run is")
        print(self.tf)

        self.c0 = get_speed_of_sound(self.plate_E, self.plate_nu,
                                     self.plate_rho0)
        self.pb = self.plate_rho0 * self.c0**2
        # self.pb = 0.
        print("timestep is,", self.dt)

        self.dim = 2

        # self.alpha = 0.
        # self.beta = 0.

        self.artificial_stress_eps = 0.3

        # edac constants
        self.edac_alpha = 0.5
        self.edac_nu = self.edac_alpha * self.c0 * self.h / 8

        # variables regarding the plate which are used to simulate the
        # deflection
        self.deflection = self.options.deflection
        I = self.H**3. / 12.
        L = self.L

        self.fx = 0.
        self.fy = self.deflection * 3. * self.plate_E * I / L**3.
        self.fz = 0.
        print("force to be applied is ")
        print(self.fy)

        # the incremental force to be applied
        self.final_force_time = self.options.final_force_time
        timesteps = self.final_force_time / self.dt
        step_force_x = self.fx / timesteps
        step_force_y = self.fy / timesteps
        step_force_z = self.fz / timesteps

        # now distribute such force over a group of particles
        self.delta_fx = step_force_x / (self.N + 1)
        self.delta_fy = step_force_y / (self.N + 1)
        self.delta_fz = step_force_z / (self.N + 1)

        # for post process
        # get the indices which are half way of the beam
        xp, yp, xw, yw = get_fixed_beam(self.L, self.H, self.L / 2.5,
                                        self.wall_layers, self.dx_plate)
        xp_unique = np.unique(xp)
        size = len(xp_unique)
        half_xp = xp_unique[int(size / 2 + size / 8)]
        self.indices_half_way = np.where(xp == half_xp)[0]
        # print(self.indices_half_way)

        # get the indices in the mid way (y=0)
        # y_zero_indices = np.where(yp == 0)
        # self.indices_mid_way = np.where(xp[tmp_indices] >= 0.)
        # x_greater_than_zero_indices = np.where(xp >= 0.)

        # self.indices_mid_way = np.intersect1d(y_zero_indices,
        #                                       x_greater_than_zero_indices)
        self.indices_mid_way = np.where(yp == 0)
        # print(self.indices_mid_way)

    def create_particles(self):
        xp, yp, xw, yw = get_fixed_beam(self.L, self.H, self.L / 2.5,
                                        self.wall_layers, self.dx_plate)
        # make sure that the beam intersection with wall starts at the 0.
        min_xp = np.min(xp)

        # add this to the beam and wall
        xp += abs(min_xp)
        xw += abs(min_xp)

        max_xw = np.max(xw)
        xp -= abs(max_xw)
        xw -= abs(max_xw)

        m = self.plate_rho0 * self.dx_plate**2.

        plate = get_particle_array(
            x=xp, y=yp, m=m, h=self.h, rho=self.plate_rho0, name="plate",
            constants={
                'E': self.plate_E,
                'n': 4,
                'nu': self.plate_nu,
                'spacing0': self.dx_plate,
                'rho_ref': self.plate_rho0
            })
        # ============================================ #
        # find all the indices which are at the right most
        # ============================================ #
        max_x = max(xp)
        indices = np.where(xp > max_x - self.dx_plate / 2.)
        # print(indices[0])
        force_idx = np.zeros_like(plate.x)
        plate.add_property('force_idx', type='int', data=force_idx)
        # print(beam.zero_force_idx)
        plate.force_idx[indices] = 1

        plate.add_constant('final_force_time',
                           np.array([self.final_force_time]))
        plate.add_constant('total_force_applied_x', np.array([0.]))
        plate.add_constant('total_force_applied_y', np.array([0.]))
        plate.add_constant('total_force_applied_z', np.array([0.]))

        # save the indices of which we compute the bending moment
        plate.add_property('indices_half_way')
        plate.indices_half_way[:] = 0.
        plate.indices_half_way[self.indices_half_way] = 1.

        # indices at y=0
        plate.add_property('indices_mid_way')
        plate.indices_mid_way[:] = 0.
        plate.indices_mid_way[self.indices_mid_way] = 1.

        # create the particle array
        wall = get_particle_array(
            x=xw, y=yw, m=m, h=self.h, rho=self.plate_rho0, name="wall",
            constants={
                'E': self.plate_E,
                'n': 4,
                'nu': self.plate_nu,
                'spacing0': self.dx_plate,
                'rho_ref': self.plate_rho0
            })

        self.scheme.setup_properties([wall, plate])

        return [plate, wall]

    def configure_scheme(self):
        self.scheme.configure(h=self.h)
        dt = self.dt
        tf = self.tf
        # c0 = self.c0
        self.ma = self.u_max / self.c0
        self.scheme.configure(h=self.h, pb=self.pb, edac_nu=self.edac_nu,
                              u_max=self.u_max, mach_no=self.mach_no,
                              hdx=self.hdx)

        self.scheme.configure_solver(tf=tf, dt=dt, pfreq=500)

    def create_scheme(self):
        solid = SolidsScheme(solids=['plate'], boundaries=['wall'], dim=2,
                             artificial_vis_alpha=1., h=0., pb=0., edac_nu=0.,
                             u_max=0., mach_no=0., hdx=0.)

        s = SchemeChooser(default='solid', solid=solid)
        return s

    def create_equations(self):
        eqns = self.scheme.get_equations()

        # Apply external force
        force_eqs = []
        force_eqs.append(
            ApplyForceGradual("plate", sources=None, delta_fx=self.delta_fx,
                              delta_fy=self.delta_fy, delta_fz=self.delta_fz))

        eqns.groups[-1].append(Group(force_eqs))
        # print(eqns.groups[-1])

        # Apply damping force
        force_eqs = []
        force_eqs.append(
            ApplyDampingForce("plate", sources=None, c=self.damping_c))

        eqns.groups[-1].append(Group(force_eqs))
        # print(eqns)

        return eqns

    def _make_accel_eval(self, equations, pa_arrays):
        if self.seval is None:
            kernel = self.scheme.scheme.kernel(dim=self.dim)
            seval = SPHEvaluator(arrays=pa_arrays, equations=equations,
                                 dim=self.dim, kernel=kernel)
            self.seval = seval
            return self.seval
        else:
            self.seval.update()
            return self.seval
        return seval

    def pre_step(self, solver):
        if self.options.pst in ['sun2019', 'ipst']:
            if solver.count % 1 == 0:
                t = solver.t
                dt = solver.dt

                arrays = self.particles
                a_eval = self._make_accel_eval(self.boundary_equations, arrays)

                # When
                a_eval.evaluate(t, dt)

    def post_process(self, fname):
        if len(self.output_files) == 0:
            return

        from pysph.solver.utils import iter_output, load

        files = self.output_files
        t, max_y = [], []
        for sd, array in iter_output(files, 'plate'):
            _t = sd['t']
            t.append(_t)
            max_y.append(max(array.y))

        import matplotlib
        import os
        # matplotlib.use('Agg')

        from matplotlib import pyplot as plt

        # res = os.path.join(self.output_dir, "results.npz")
        # np.savez(res, t=t, amplitude=amplitude)

        # gtvf data
        # data = np.loadtxt('./oscillating_plate.csv', delimiter=',')
        # t_gtvf, amplitude_gtvf = data[:, 0], data[:, 1]

        # plt.clf()

        # plt.plot(t_gtvf, amplitude_gtvf, "s-", label='GTVF Paper')
        label = 'max y position of plate with length ' + str(
            self.L) + ' and a deflection of ' + str(self.deflection)
        plt.plot(t, max_y, label=label)

        plt.xlabel('t')
        plt.ylabel('Max y')
        plt.legend()
        fig = os.path.join(self.output_dir, "max_y.png")
        plt.savefig(fig, dpi=300)
        plt.clf()

        files = self.output_files
        last_file = files[-1]
        data = load(last_file)
        # solver_data = data['solver_data']
        arrays = data['arrays']
        pa = arrays['plate']
        y = pa.y[self.indices_half_way]
        sigma_00 = pa.sigma00[self.indices_half_way]
        label = 'sigma00_vs_y with length ' + str(
            self.L) + ' and a deflection of ' + str(self.deflection)
        plt.plot(sigma_00, y, label=label)
        plt.xlabel('sigma xx')
        plt.ylabel('y')
        plt.legend()
        fig = os.path.join(self.output_dir, "sigma00_vs_y.png")
        plt.savefig(fig, dpi=300)
        # plt.show()


if __name__ == '__main__':
    app = CantileverBeamDeflectionWithTipload()
    app.run()
    # app.post_process(app.info_filename)
    print(app.info_filename)
    plot_vertical_displacement_of_midway_of_beam_y_0(
        os.path.dirname(app.info_filename),
        app.L,
        app.H,
        app.deflection)

    # app.create_rings_geometry()
