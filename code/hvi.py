"""
High-Velocity impact (HVI). Section 4.4 of Generalized TVF by Zhang 2017.


1. GRAY

python hvi.py --openmp --pst gray --no-volume-evolution --no-shear-stress-tvf-correction --no-edac --no-surf-p-zero -d hvi_gray_output


2. GTVF

python hvi.py --openmp --pst gtvf --no-volume-evolution --uhat --no-shear-stress-tvf-correction --no-edac --no-surf-p-zero --kernel-choice 4 -d hvi_gtvf_kernel_4_output


3. SUN2019

python hvi.py --openmp --pst sun2019 --ipst-max-iterations 10 --volume-evolution --no-uhat --shear-stress-tvf-correction --edac --no-surf-p-zero --kernel-choice 4 -d hvi_sun2019_kernel_4_output


4. IPST

python hvi.py --openmp --pst ipst --ipst-max-iterations 10 --volume-evolution --no-uhat --shear-stress-tvf-correction --edac --no-surf-p-zero --kernel-choice 4 -d hvi_ipst_kernel_4_output
"""

import numpy as np
from math import cos, sin, cosh, sinh, sqrt

# SPH equations
from pysph.sph.equation import Group, MultiStageEquations
from pysph.sph.basic_equations import IsothermalEOS

from pysph.sph.equation import Equation
from pysph.base.utils import get_particle_array
from pysph.base.kernels import (CubicSpline, WendlandQuintic, QuinticSpline)
# from pysph.solver.application import Application
from pysph.solver.solver import Solver
from pysph.solver.application import Application
from pysph.sph.integrator import EPECIntegrator
from pysph.sph.integrator_step import SolidMechStep
from pysph.tools.sph_evaluator import SPHEvaluator
from pysph.sph.scheme import SchemeChooser

from pysph.sph.solid_mech.basic import (get_particle_array_elastic_dynamics,
                                        get_speed_of_sound)

from solid_mech import (SetHIJForInsideParticles, add_plasticity_properties,
                        ComputeJ2, LimitDeviatoricStress,
                        SolidsScheme)

# for normals
from pysph.sph.isph.wall_normal import ComputeNormals, SmoothNormals

from boundary_particles import (add_boundary_identification_properties,
                                get_boundary_identification_etvf_equations)


class MakeAccelerationZeroOnSelectedIndices(Equation):
    def initialize(self, d_idx, d_au, d_av, d_aw, d_auhat, d_avhat, d_awhat,
                   d_make_acceleration_zero):
        if d_make_acceleration_zero[d_idx] == 1:
            d_au[d_idx] = 0
            d_av[d_idx] = 0
            d_aw[d_idx] = 0

            d_auhat[d_idx] = 0
            d_avhat[d_idx] = 0
            d_awhat[d_idx] = 0


def create_circle(diameter=1, spacing=0.05, center=None):
    dx = spacing
    x = [0.0]
    y = [0.0]
    r = spacing
    nt = 0
    while r < diameter / 2:
        nnew = int(np.pi * r**2 / dx**2 + 0.5)
        tomake = nnew - nt
        theta = np.linspace(0., 2. * np.pi, tomake + 1)
        for t in theta[:-1]:
            x.append(r * np.cos(t))
            y.append(r * np.sin(t))
        nt = nnew
        r = r + dx

    # add these three points manually
    # 1
    # x.append(-0.000179317)
    # y.append(0.0)
    # # 2
    # x.append(7.96537 * 1e-5)
    # y.append(-0.000137973)
    # # 3
    # x.append(7.96537 * 1e-5)
    # y.append(0.000140617)

    x = np.array(x)
    y = np.array(y)
    x, y = (t.ravel() for t in (x, y))

    # import matplotlib.pyplot as plt
    # plt.scatter(x, y)
    # plt.axes().set_aspect('equal', 'datalim')
    # plt.show()

    if center is None:
        return x, y
    else:
        return x + center[0], y + center[1]


class HVI(Application):
    def initialize(self):
        rho = 2785.
        # e = 69. * 1e9
        e = 70. * 1e9
        g = 2.76 * 1e10
        # nu = get_poisson_ratio_from_E_G(e, g)
        nu = 0.35
        print("poisson ration is ", nu)

        self.yield_modulus = 3. * 1e8

        self.dx = 0.25 * 1e-3 / 1.5
        self.cylinder_radius = 5. * 1e-3
        self.cylinder_rho0 = rho
        self.cylinder_G = g
        self.cylinder_E = e
        self.cylinder_nu = nu
        self.cylinder_u = 3100.

        self.structure_length = 2. * 1e-3
        self.structure_height = 50. * 1e-3
        self.structure_rho0 = rho
        self.structure_G = g
        self.structure_E = e
        self.structure_nu = nu

        self.fac = self.dx / 2.
        self.kernel_fac = 3
        self.hdx = 1.2
        self.h = self.hdx * self.dx

        self.dt = 0.25 * self.h / (
            (self.structure_E / self.structure_rho0)**0.5 + 2.85)

        self.c0 = get_speed_of_sound(self.structure_E, self.structure_nu,
                                     self.structure_rho0)
        print("speed of sound is")
        print(self.c0)
        self.pb = self.structure_rho0 * self.c0**2
        # self.pb = 0.
        print("timestep is,", self.dt)

        self.dim = 2

        self.artificial_vis_alpha = 1.0
        self.artificial_vis_beta = 0.0

        # self.alpha = 0.
        # self.beta = 0.

        self.artificial_stress_eps = 0.3

        self.normal_comupte_count = 1

        # for pre step
        self.seval = None

        # edac constants
        self.edac_alpha = 0.5
        self.edac_nu = self.edac_alpha * self.c0 * self.h / 8

        # attributes for Sun PST technique
        self.u_max = 4500.  # this is hard coded from previous simulation runs
        self.mach_no = self.u_max / self.c0

        # attributes for IPST technique
        self.ipst_max_iterations = 10

        # # boundary equations
        # self.boundary_equations_structure = get_boundary_identification_etvf_equations(
        #     destinations=["structure"], sources=[])
        # self.boundary_equations_cylinder = get_boundary_identification_etvf_equations(
        #     destinations=["cylinder"], sources=[])

        self.boundary_equations_structure = get_boundary_identification_etvf_equations(
            destinations=["structure"], sources=["cylinder"])
        self.boundary_equations_cylinder = get_boundary_identification_etvf_equations(
            destinations=["cylinder"], sources=["structure"])

        self.boundary_equations = self.boundary_equations_structure + self.boundary_equations_cylinder

    def create_particles(self):
        # self.dx = self.dx / 2.
        # create a structure
        xs, ys = np.mgrid[0.:self.structure_length + self.dx / 2.:self.dx,
                          0.:self.structure_height + self.dx / 2.:self.dx]
        xs = xs.ravel()
        ys = ys.ravel()

        m = self.structure_rho0 * self.dx**2.

        kernel = QuinticSpline(dim=2)
        self.wdeltap = kernel.kernel(rij=self.dx, h=self.h)
        # self.wdeltap = 0.0

        structure = get_particle_array(x=xs,
                                       y=ys,
                                       m=m,
                                       h=self.h,
                                       rho=self.structure_rho0,
                                       name="structure",
                                       constants={
                                           'E': self.structure_E,
                                           'n': 4,
                                           'nu': self.structure_nu,
                                           'spacing0': self.dx,
                                           'rho_ref': self.structure_rho0
                                       })

        # ------------------------
        # filter the indices of the structure whose accelerations needed
        # to be zero
        # ------------------------
        make_acceleration_zero = np.zeros(len(structure.x), dtype=int)

        for i in range(len(structure.x)):
            if (structure.y[i] < 8. * self.dx
                    or structure.y[i] > self.structure_height - 8. * self.dx):
                make_acceleration_zero[i] = 1

        structure.add_property("make_acceleration_zero", type='int')
        structure.make_acceleration_zero[:] = make_acceleration_zero[:]

        # This is to simulate plasticity. Applicable for all schemes
        add_plasticity_properties(structure)

        # data = np.load('hvi_cylinder_pack.npz')
        # xc = data['xs']
        # yc = data['ys']

        # cylinder coordinates
        xc, yc = create_circle(
            2. * self.cylinder_radius, self.dx,
            [-self.dx / 2. - self.cylinder_radius, self.structure_height / 2.])

        # xc[:] -= +self.dx / 2. + self.cylinder_radius
        # yc[:] += self.structure_height / 2.

        cylinder = get_particle_array(x=xc,
                                      y=yc,
                                      m=m,
                                      h=self.h,
                                      rho=self.structure_rho0,
                                      name="cylinder",
                                      constants={
                                          'E': self.cylinder_E,
                                          'n': 4,
                                          'nu': self.cylinder_nu,
                                          'spacing0': self.dx,
                                          'rho_ref': self.cylinder_rho0
                                      })

        # This is to simulate plasticity. Applicable for all schemes
        add_plasticity_properties(cylinder)

        cylinder.u[:] = self.cylinder_u

        self.scheme.setup_properties([cylinder, structure])

        return [structure, cylinder]

    def create_equations(self):
        eqns = self.scheme.get_equations()

        # plasticity equations
        plastic_eq = []
        for solid in ["structure", "cylinder"]:
            plastic_eq.append(ComputeJ2(dest=solid, sources=None))
            plastic_eq.append(
                LimitDeviatoricStress(solid,
                                      sources=None,
                                      yield_modulus=self.yield_modulus))

        eqns.groups[1].insert(1, Group(plastic_eq))

        # make accelerations zero on specific indices
        makeaccelzero_eq = []
        makeaccelzero_eq.append(
            MakeAccelerationZeroOnSelectedIndices(dest="structure",
                                                  sources=None))

        eqns.groups[1].append(Group(makeaccelzero_eq))

        # change the sources
        eqns.groups[1][-2].equations[2].sources = ["cylinder", "structure"]
        eqns.groups[1][-2].equations[-1].sources = ["cylinder", "structure"]

        return eqns

    def configure_scheme(self):
        dt = self.dt
        tf = 8. * 1e-6

        output = np.linspace(0., 8, 9) * 1e-6

        pfreq = 2**32

        self.scheme.configure_solver(dt=dt, tf=tf, pfreq=pfreq,
                                     output_at_times=output)

    def create_scheme(self):
        solid = SolidsScheme(solids=['cylinder', 'structure'],
                             boundaries=None,
                             dim=2,
                             pb=self.pb,
                             edac_nu=self.edac_nu,
                             u_max=self.u_max,
                             mach_no=self.mach_no,
                             hdx=self.hdx,
                             h=self.h)

        s = SchemeChooser(default='solid', solid=solid)
        return s

    def _make_accel_eval(self, equations, pa_arrays):
        if self.seval is None:
            kernel = QuinticSpline(dim=self.dim)
            seval = SPHEvaluator(arrays=pa_arrays,
                                 equations=equations,
                                 dim=self.dim,
                                 kernel=kernel)
            self.seval = seval
            return self.seval
        else:
            self.seval.update()
            return self.seval
        return seval

    def pre_step(self, solver):
        if solver.count % 1 == 0:
            t = solver.t
            dt = solver.dt

            arrays = self.particles
            a_eval = self._make_accel_eval(self.boundary_equations, arrays)

            # When
            a_eval.evaluate(t, dt)

    def post_process(self, fname):
        import matplotlib.pyplot as plt
        import os
        from pysph.solver.utils import iter_output
        from pysph.solver.utils import get_files

        files = get_files(os.path.dirname(fname))

        i = 0
        for sd, structure, cylinder in iter_output(files, 'structure', 'cylinder'):
            _t = sd['t']
            plt.clf()
            c_min = min(min(structure.p), min(cylinder.p))
            c_max = max(max(structure.p), max(cylinder.p))
            plt.scatter(structure.x, structure.y, s=2, c=structure.p, vmin=c_min, vmax=c_max)
            tmp = plt.scatter(cylinder.x, cylinder.y, s=2, c=cylinder.p, vmin=c_min, vmax=c_max)
            plt.colorbar(tmp)

            plt.axis('equal')
            plt.axis('off')

            figname = os.path.join(os.path.dirname(fname), "time" + str(i) + ".png")
            plt.savefig(figname, dpi=300)
            i = i + 1


if __name__ == '__main__':
    app = HVI()
    app.run()
    app.post_process(app.info_filename)
