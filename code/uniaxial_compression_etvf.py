"""SPH Stress and Deformation Solutions for a Uniaxial Test

Paper: Evaluation of Accuracy and Stability of the Classical SPH Method Under
Uniaxial Compression

By: R. Das . P. W. Cleary

"""
import numpy as np
from math import cos, sin, cosh, sinh

# SPH equations
from pysph.sph.equation import Group, MultiStageEquations
from pysph.sph.basic_equations import IsothermalEOS

from pysph.sph.equation import Equation
from pysph.base.utils import get_particle_array
from pysph.base.kernels import (CubicSpline, WendlandQuintic, QuinticSpline)
# from pysph.solver.application import Application
from pysph.solver.solver import Solver
from pysph.solver.application import Application
from pysph.sph.integrator import EPECIntegrator
from pysph.sph.integrator_step import SolidMechStep
from pysph.tools.sph_evaluator import SPHEvaluator

from pysph.sph.basic_equations import (ContinuityEquation,
                                       MonaghanArtificialViscosity,
                                       XSPHCorrection, VelocityGradient2D)

from pysph.sph.wc.gtvf import (GTVFIntegrator, CorrectDensity,
                               MomentumEquationArtificialStress as Astress)

# for normals
from pysph.sph.isph.wall_normal import ComputeNormals, SmoothNormals

from gtvf import (ContinuityEquationUhat, VelocityGradient2DUhat)

from pysph.sph.solid_mech.basic import (HookesDeviatoricStressRate,
                                        IsothermalEOS, get_speed_of_sound,
                                        MomentumEquationWithStress,
                                        MonaghanArtificialStress)

from etvf import (get_particle_array_etvf_elastic_solid, ETVFStep,
                  SetHIJForInsideParticles, ComputeAuHatETVF)

# for Von Mises computation
from etvf import (ComputePrincipalStress2D)

from boundary_particles import (add_boundary_identification_properties,
                                IdentifyBoundaryParticle2,
                                IdentifyBoundaryParticleCosAngle)


def add_properties(pa, *props):
    for prop in props:
        pa.add_property(name=prop)


class UniaxialCompression(Application):
    def initialize(self):
        # given
        # bulk modulus
        self.bar_K = 12.2 * 1e9
        # shear modulus
        self.bar_G = 2.67 * 1e9
        # find nu (poisson's ratio)
        self.bar_nu = (3. * self.bar_K - 2. * self.bar_G) / (6. * self.bar_K +
                                                             2. * self.bar_G)

        # then find the Youngs modulus
        self.bar_E = 2. * self.bar_G * (1 + self.bar_nu)

        self.dx_bar = 1 * 1e-3
        self.fac = self.dx_bar / 2.
        self.kernel_factor = 3
        self.h = 1.2 * self.dx_bar
        self.bar_length = 82. * 1e-3
        self.bar_height = 140. * 1e-3
        # changing this for checking (RKC SPH)
        self.bar_rho0 = 2300.

        # moving piston
        self.piston_length = self.bar_length + self.bar_length / 4.
        self.piston_height = self.dx_bar * 4.
        self.piston_velocity = 1.5 * 1e-3

        # wall
        self.wall_length = self.bar_length + self.bar_length / 4.
        self.wall_heigth = self.dx_bar * 4.
        # compute the timestep
        self.dt = 0.25 * self.h / ((self.bar_E / self.bar_rho0)**0.5 + 2.85)

        self.c0 = get_speed_of_sound(self.bar_E, self.bar_nu, self.bar_rho0)
        print(self.c0)

        self.pb = self.bar_rho0 * self.c0**2
        # self.pb = 0.

        self.dim = 2

        self.alpha = 1.0
        self.beta = 2.0

        # self.alpha = 0.
        # self.beta = 0.

        # for pre step
        self.seval = None

        # post process coordinates
        # bar coordinates
        xb, yb = np.mgrid[0.:self.bar_length +
                          self.dx_bar / 2.:self.dx_bar, 0.:self.bar_height +
                          self.dx_bar / 2.:self.dx_bar]
        xb = xb.ravel()
        yb = yb.ravel()

        m = self.bar_rho0 * self.dx_bar**2.

        bar = get_particle_array_etvf_elastic_solid(
            x=xb, y=yb, m=m, h=self.h, rho=self.bar_rho0, name="bar",
            constants=dict(E=self.bar_E, nu=self.bar_nu,
                           rho_ref=self.bar_rho0))
        bar.add_property("rhodiv")
        bar.add_property("sigma_1")
        bar.add_property("sigma_2")
        add_boundary_identification_properties(bar)

        # idx of von mises stress
        # get all the indices with its y is at 70mm
        y_70_idxs = []
        y_103_idxs = []
        x_41_idxs = []
        x_21_idxs = []
        for i in range(len(bar.x)):
            if bar.y[i] == 70. * 1e-3:
                y_70_idxs.append(i)

            if bar.y[i] == 103. * 1e-3:
                y_103_idxs.append(i)

            if bar.x[i] == 41. * 1e-3:
                x_41_idxs.append(i)

            if bar.x[i] == 21. * 1e-3:
                x_21_idxs.append(i)

        # print(x_21_idxs)

        A_idx = None
        for idx in x_41_idxs:
            if idx in y_70_idxs:
                A_idx = idx

        B_idx = None
        for idx in x_21_idxs:
            if idx in y_103_idxs:
                B_idx = idx

        C_idx = None
        for idx in x_21_idxs:
            if idx in y_70_idxs:
                C_idx = idx

        print("A idx is", A_idx)
        print("B idx is", B_idx)
        print("C idx is", C_idx)
        self.A_idx = A_idx
        self.B_idx = B_idx
        self.C_idx = C_idx
        # ends

    def create_particles(self):
        # bar coordinates
        xb, yb = np.mgrid[0.:self.bar_length +
                          self.dx_bar / 2.:self.dx_bar, 0.:self.bar_height +
                          self.dx_bar / 2.:self.dx_bar]
        xb = xb.ravel()
        yb = yb.ravel()

        m = self.bar_rho0 * self.dx_bar**2.

        bar = get_particle_array_etvf_elastic_solid(
            x=xb, y=yb, m=m, h=self.h, rho=self.bar_rho0, name="bar",
            constants=dict(E=self.bar_E, nu=self.bar_nu,
                           rho_ref=self.bar_rho0))
        bar.add_property("rhodiv")
        bar.add_property("sigma_1")
        bar.add_property("sigma_2")
        add_boundary_identification_properties(bar)

        # create a moving piston
        xp, yp = np.mgrid[0.:self.piston_length +
                          self.dx_bar / 2.:self.dx_bar, 0.:self.piston_height +
                          self.dx_bar / 2.:self.dx_bar]
        xp = xp.ravel()
        yp = yp.ravel()

        m = self.bar_rho0 * self.dx_bar**2.

        piston = get_particle_array_etvf_elastic_solid(
            x=xp, y=yp, m=m, h=self.h, rho=self.bar_rho0, name="piston",
            constants=dict(E=self.bar_E, nu=self.bar_nu,
                           rho_ref=self.bar_rho0))
        piston.y += max(bar.y) + self.dx_bar
        piston.v[:] = -1.5 * 1e-3
        piston.x -= (self.piston_length - self.bar_length) / 2.
        # add_boundary_identification_properties(piston)

        # create a bottom wall
        xw, yw = np.mgrid[0.:self.piston_length +
                          self.dx_bar / 2.:self.dx_bar, 0.:self.piston_height +
                          self.dx_bar / 2.:self.dx_bar]
        xw = xw.ravel()
        yw = yw.ravel()

        m = self.bar_rho0 * self.dx_bar**2.

        wall = get_particle_array_etvf_elastic_solid(
            x=xw, y=yw, m=m, h=self.h, rho=self.bar_rho0, name="wall",
            constants=dict(E=self.bar_E, nu=self.bar_nu,
                           rho_ref=self.bar_rho0))
        wall.y -= max(wall.y) + 1.0 * self.dx_bar
        wall.x -= (self.wall_length - self.bar_length) / 2.

        return [bar, piston, wall]

    def create_solver(self):
        # kernel = CubicSpline(dim=2)
        # kernel = WendlandQuintic(dim=2)
        kernel = QuinticSpline(dim=2)

        integrator = GTVFIntegrator(bar=ETVFStep(), wall=ETVFStep(),
                                    piston=ETVFStep())

        dt = self.dt
        print("timestep is,", dt)
        tf = 1e-4

        # tf = 0.1

        output = np.array(
            [40., 50, 60, 80, 90, 110, 120, 160, 190, 240, 280, 380]) * 1e-6
        solver = Solver(kernel=kernel, dim=2, integrator=integrator, dt=dt,
                        tf=tf, adaptive_timestep=False, pfreq=1500,
                        output_at_times=output)
        return solver

    def create_equations(self):

        stage1 = [
            Group(
                equations=[
                    ContinuityEquation(dest="bar",
                                       sources=['bar', 'wall', 'piston']),
                    # ContinuityEquation(dest="piston",
                    #                    sources=['piston', 'bar']),
                    ContinuityEquation(dest="wall", sources=['bar', 'wall']),

                    # vi,j : requires properties v00, v01, v10, v11
                    VelocityGradient2D(dest="bar",
                                       sources=['bar', 'wall', 'piston']),
                    # VelocityGradient2D(dest="piston",
                    #                    sources=['bar', 'piston']),
                    VelocityGradient2D(dest="wall", sources=['bar']),

                    # # with uhat
                    # ContinuityEquationUhat(dest="bar",
                    #                        sources=['bar', 'wall', 'piston']),
                    # ContinuityEquationUhat(dest="piston",
                    #                        sources=['piston', 'bar']),
                    # ContinuityEquationUhat(dest="wall",
                    #                        sources=['bar', 'wall']),

                    # # vi,j : requires properties v00, v01, v10, v11
                    # VelocityGradient2DUhat(dest="bar",
                    #                        sources=['bar', 'wall', 'piston']),
                    # VelocityGradient2DUhat(dest="piston",
                    #                        sources=['bar', 'piston']),
                    # VelocityGradient2DUhat(dest="wall", sources=['bar']),
                ], ),
            Group(
                equations=[
                    HookesDeviatoricStressRate(dest="bar", sources=None),
                    HookesDeviatoricStressRate(dest="piston", sources=None),
                    HookesDeviatoricStressRate(dest="wall", sources=None)
                ], ),
        ]

        stage2 = [
            Group(equations=[
                SetHIJForInsideParticles(dest='bar', sources=['bar'], h=self.h,
                                         kernel_factor=self.kernel_factor)
            ]),
            Group(
                equations=[
                    IsothermalEOS("bar", sources=None),
                    IsothermalEOS("piston", sources=None),
                    IsothermalEOS("wall", sources=None),
                ], ),
            Group(
                equations=[
                    MomentumEquationWithStress(
                        dest="bar", sources=['bar', 'wall', 'piston']),
                    ComputeAuHatETVF(dest="bar",
                                     sources=['bar', 'wall',
                                              'piston'], pb=self.pb),
                    MonaghanArtificialViscosity(
                        dest="bar", sources=['bar', 'wall', 'piston'],
                        alpha=self.alpha, beta=self.beta),
                    # Astress(dest='bar', sources=['bar', 'wall'], dim=self.dim),
                ], ),
            Group(equations=[
                ComputePrincipalStress2D("bar", sources=None),
            ], ),
        ]

        return MultiStageEquations([stage1, stage2])

    def _make_accel_eval(self, equations, pa_arrays):
        if self.seval is None:
            kernel = QuinticSpline(dim=self.dim)
            seval = SPHEvaluator(arrays=pa_arrays, equations=equations,
                                 dim=self.dim, kernel=kernel)
            self.seval = seval
            return self.seval
        else:
            return self.seval
        return seval

    def pre_step(self, solver):
        if solver.count % 10 == 0:
            t = solver.t
            dt = solver.dt

            eqs = [
                # ------------------------
                # compute the normals
                Group(
                    equations=[
                        # For ETVF
                        ComputeNormals(dest='bar',
                                       sources=['wall', 'bar', 'piston'])
                        # ComputeNormals(dest='bar', sources=['bar'])
                    ], ),
                Group(
                    equations=[
                        # For ETVF
                        SmoothNormals(dest='bar', sources=['bar'])
                    ], ),
                Group(equations=[
                    IdentifyBoundaryParticleCosAngle(dest='bar',
                                                     sources=['bar'])
                ]),
                # Group(equations=[
                #     IdentifyBoundaryParticle2(dest='bar', sources=['bar'],
                #                               fac=self.fac)
                # ]),
                # compute the normals ends
                # ------------------------
            ]
            arrays = self.particles
            a_eval = self._make_accel_eval(eqs, arrays)

            # When
            a_eval.evaluate(t, dt)

    # def post_step(self, solver):
    #     dt = solver.dt
    #     for pa in self.particles:
    #         if pa.name == 'piston':
    #             pa.y -= self.piston_velocity * dt

    def post_process(self):
        if len(self.output_files) == 0:
            return

        from pysph.solver.utils import iter_output

        files = self.output_files
        t, von_mises_A = [], []
        von_mises_B, von_mises_C = [], []

        for sd, array in iter_output(files, "bar"):
            _t = sd['t']
            t.append(_t * 1e6)
            von_mises_A.append(
                np.sqrt(array.sigma_1[self.A_idx]**2. +
                        array.sigma_2[self.A_idx]**2. -
                        array.sigma_1[self.A_idx] * array.sigma_2[self.A_idx]))

            von_mises_B.append(
                np.sqrt(array.sigma_1[self.B_idx]**2. +
                        array.sigma_2[self.B_idx]**2. -
                        array.sigma_1[self.B_idx] * array.sigma_2[self.B_idx]))

            von_mises_C.append(
                np.sqrt(array.sigma_1[self.C_idx]**2. +
                        array.sigma_2[self.C_idx]**2. -
                        array.sigma_1[self.C_idx] * array.sigma_2[self.C_idx]))

        # import matplotlib
        import os
        # matplotlib.use('Agg')

        from matplotlib import pyplot as plt

        res = os.path.join(self.output_dir, "results.npz")
        np.savez(res, t=t, von_mises_A=von_mises_A)
        np.savez(res, t=t, von_mises_B=von_mises_B)
        np.savez(res, t=t, von_mises_C=von_mises_C)

        # gtvf data
        # data = np.loadtxt('./oscillating_plate.csv', delimiter=',')
        # t_gtvf, amplitude_gtvf = data[:, 0], data[:, 1]

        plt.clf()

        data_fem_A = np.loadtxt(
            'uniaxial_compression_etvf_data/point_a_100_mu_sec_fem_das_data.csv',
            delimiter=',')
        t_fem_A, von_mises_fem_A = data_fem_A[:, 0], data_fem_A[:, 1]

        data_sph_A = np.loadtxt(
            'uniaxial_compression_etvf_data/point_a_100_mu_sec_sph_das_data.csv',
            delimiter=',')
        t_sph_A, von_mises_sph_A = data_sph_A[:, 0], data_sph_A[:, 1]

        plt.plot(t, von_mises_A, "-sr", label='Point A ETVF')
        plt.plot(t_fem_A, von_mises_fem_A, "--g", label='Point A FEM')
        plt.plot(t_sph_A, von_mises_sph_A, "--b", label='Point A SPH Das')

        # plt.plot(t, von_mises_B, "-g", label='Point B')
        # plt.plot(t, von_mises_C, "--b", label='Point C')

        plt.xlabel('time (ms)')
        plt.ylabel('von Mises')
        plt.legend()
        fig = os.path.join(self.output_dir, "von_mises.png")
        plt.savefig(fig, dpi=300)
        plt.show()


if __name__ == '__main__':
    app = UniaxialCompression()
    app.run()
    app.post_process()
# np.sqrt(sigma_1**2. + sigma_2**2. - sigma_1 * sigma_2)
