"""Oscillating plate with initial velocity profile. Benchmark in Solid mechanics.


1. GRAY

python oscillating_plate.py --openmp --pst gray --no-uhat --no-continuity-tvf-correction --no-shear-stress-tvf-correction --no-edac --no-surf-p-zero --kernel-choice 1 -d oscillating_plate_gray_kernel_1_output --pfreq 10 --detailed

2. GTVF

python rings.py --openmp --pst gtvf --no-volume-evolution --uhat --no-shear-stress-tvf-correction --no-edac --no-surf-p-zero --kernel-choice 4 -d rings_gtvf_kernel_4_output

3. SUN2019

python rings.py --openmp --pst sun2019 --ipst-max-iterations 10 --volume-evolution --no-uhat --shear-stress-tvf-correction --edac --no-surf-p-zero --kernel-choice 4 -d rings_sun2019_kernel_4_output


Cases to run for convergence test

python oscillating_plate.py --openmp --pst sun2019 --no-uhat --no-shear-stress-tvf-correction --no-edac --no-surf-p-zero --kernel-choice 4 -d oscillating_plate_sun2019_kernel_4_output


"""
import numpy as np
from math import cos, sin, sinh, cosh

# SPH equations
from pysph.base.kernels import (QuinticSpline)
from pysph.solver.solver import Solver
from pysph.solver.application import Application
from pysph.tools.sph_evaluator import SPHEvaluator
from pysph.sph.equation import Group, MultiStageEquations
from pysph.sph.scheme import SchemeChooser

from pysph.base.utils import get_particle_array

from solid_mech import SetHIJForInsideParticles, SolidsScheme

from boundary_particles import (add_boundary_identification_properties,
                                get_boundary_identification_etvf_equations)

from pysph.sph.solid_mech.basic import (get_speed_of_sound, get_bulk_mod,
                                        get_shear_modulus)
from pysph.tools.geometry import get_2d_tank, get_2d_block


def get_fixed_beam(beam_length, beam_height, beam_inside_length,
                   boundary_layers, spacing):
    """
 |||=============
 |||=============
 |||===================================================================|
 |||===================================================================|Beam height
 |||===================================================================|
 |||=============
 |||=============
   <------------><---------------------------------------------------->
      Beam inside                   Beam length
      length
    """
    import matplotlib.pyplot as plt
    # create a block first
    xb, yb = get_2d_block(dx=spacing,
                          length=beam_length + beam_inside_length,
                          height=beam_height)

    # create a (support) block with required number of layers
    xs1, ys1 = get_2d_block(dx=spacing,
                            length=beam_inside_length,
                            height=boundary_layers * spacing)
    xs1 += np.min(xb) - np.min(xs1)
    ys1 += np.min(yb) - np.max(ys1) - spacing

    # create a (support) block with required number of layers
    xs2, ys2 = get_2d_block(dx=spacing,
                            length=beam_inside_length,
                            height=boundary_layers * spacing)
    xs2 += np.min(xb) - np.min(xs2)
    ys2 += np.max(ys2) - np.min(yb) + spacing

    xs = np.concatenate([xs1, xs2])
    ys = np.concatenate([ys1, ys2])

    xs3, ys3 = get_2d_block(dx=spacing,
                            length=boundary_layers * spacing,
                            height=np.max(ys) - np.min(ys))
    xs3 += np.min(xb) - np.max(xs3) - 1. * spacing
    # ys3 += np.max(ys2) - np.min(yb) + spacing

    xs = np.concatenate([xs, xs3])
    ys = np.concatenate([ys, ys3])
    # plt.scatter(xs, ys)
    # plt.scatter(xb, yb)
    # plt.axes().set_aspect('equal', 'datalim')
    # plt.show()

    return xb, yb, xs, ys


class OscillatingPlate(Application):
    def initialize(self):
        # dummy value to make the scheme work
        self.plate_rho0 = 1000.
        self.plate_E = 2. * 1e6
        self.plate_nu = 0.3975
        self.c0 = get_speed_of_sound(self.plate_E, self.plate_nu,
                                     self.plate_rho0)
        self.pb = self.plate_rho0 * self.c0**2

        self.edac_alpha = 0.5
        self.hdx = 1.2

        # this is dummpy value
        self.h = 0.001
        self.edac_nu = self.edac_alpha * self.c0 * self.h / 8

        # attributes for Sun PST technique
        # dummy value, will be updated in consume user options
        self.u_max = 2.8513
        self.mach_no = self.u_max / self.c0

        # for pre step
        self.seval = None

        # boundary equations
        # self.boundary_equations = get_boundary_identification_etvf_equations(
            # destinations=["plate"], sources=["plate"])
        self.boundary_equations = get_boundary_identification_etvf_equations(
            destinations=["plate"],
            sources=["plate", "wall"],
            boundaries=["wall"])

    def add_user_options(self, group):
        group.add_argument("--rho",
                           action="store",
                           type=float,
                           dest="rho",
                           default=1000.,
                           help="Density of the particle (Defaults to 1000.)")

        group.add_argument("--Vf",
                           action="store",
                           type=float,
                           dest="Vf",
                           default=0.05,
                           help="Velocity of the plate (Vf) (Defaults to 0.05)")

        group.add_argument("--length",
                           action="store",
                           type=float,
                           dest="length",
                           default=0.2,
                           help="Length of the plate")

        group.add_argument("--height",
                           action="store",
                           type=float,
                           dest="height",
                           default=0.02,
                           help="height of the plate")

        group.add_argument("--N",
                           action="store",
                           type=int,
                           dest="N",
                           default=9,
                           help="No of particles in the height direction")

    def consume_user_options(self):
        self.rho = self.options.rho
        self.L = self.options.length
        self.H = self.options.height
        self.N = self.options.N

        # wave number K
        self.KL = 1.875
        self.K = 1.875 / self.L

        # edge velocity of the plate (m/s)
        self.Vf = self.options.Vf
        # print(self.Vf)
        self.dx_plate = self.H / self.N
        # print("dx_plate[ is ]")
        # print(self.dx_plate)
        # self.fac = self.dx_plate / 2.
        self.h = self.hdx * self.dx_plate
        # print(self.h)
        self.plate_rho0 = self.rho

        self.wall_layers = 2

        # compute the timestep
        self.tf = 1.0
        self.dt = 0.25 * self.h / (
            (self.plate_E / self.plate_rho0)**0.5 + 2.85)
        # self.dt = 0.5 * self.h / (
        #     (self.plate_E / self.plate_rho0)**0.5 + 2.85)
        # self.dt = 0.25 * self.h / (
        #     (self.plate_E / self.plate_rho0)**0.5 + 2.85)

        self.c0 = get_speed_of_sound(self.plate_E, self.plate_nu,
                                     self.plate_rho0)
        self.pb = self.plate_rho0 * self.c0**2
        # self.pb = 0.
        print("timestep is,", self.dt)

        self.dim = 2

        # self.alpha = 0.
        # self.beta = 0.

        self.artificial_stress_eps = 0.3

        # edac constants
        self.edac_alpha = 0.5
        self.edac_nu = self.edac_alpha * self.c0 * self.h / 8

    def create_particles(self):
        xp, yp, xw, yw = get_fixed_beam(self.L, self.H, self.L/2.5,
                                        self.wall_layers, self.dx_plate)
        # make sure that the beam intersection with wall starts at the 0.
        min_xp = np.min(xp)

        # add this to the beam and wall
        xp += abs(min_xp)
        xw += abs(min_xp)

        max_xw = np.max(xw)
        xp -= abs(max_xw)
        xw -= abs(max_xw)

        m = self.plate_rho0 * self.dx_plate**2.

        plate = get_particle_array(x=xp,
                                   y=yp,
                                   m=m,
                                   h=self.h,
                                   rho=self.plate_rho0,
                                   name="plate",
                                   constants={
                                       'E': self.plate_E,
                                       'n': 4,
                                       'nu': self.plate_nu,
                                       'spacing0': self.dx_plate,
                                       'rho_ref': self.plate_rho0
                                   })

        # create the particle array
        wall = get_particle_array(x=xw,
                                  y=yw,
                                  m=m,
                                  h=self.h,
                                  rho=self.plate_rho0,
                                  name="wall",
                                  constants={
                                      'E': self.plate_E,
                                      'n': 4,
                                      'nu': self.plate_nu,
                                      'spacing0': self.dx_plate,
                                      'rho_ref': self.plate_rho0
                                  })

        self.scheme.setup_properties([wall, plate])

        ##################################
        # vertical velocity of the plate #
        ##################################
        # initialize with zero at the beginning
        v = np.zeros_like(xp)
        v = v.ravel()

        # set the vertical velocity for particles which are only
        # out of the wall
        K = self.K
        # L = self.L
        KL = self.KL
        M = sin(KL) + sinh(KL)
        N = cos(KL) + cosh(KL)
        Q = 2 * (cos(KL) * sinh(KL) - sin(KL) * cosh(KL))
        for i in range(len(v)):
            if xp[i] > 0.:
                # set the velocity
                tmp1 = (cos(K * xp[i]) - cosh(K * xp[i]))
                tmp2 = (sin(K * xp[i]) - sinh(K * xp[i]))
                v[i] = self.Vf * plate.cs[0] * (M * tmp1 - N * tmp2) / Q
        plate.v = v
        # print("vmax is ", np.max(v))
        # get the index of the particle which will be used to compute the
        # amplitude
        xp_max = max(xp)
        fltr = np.argwhere(xp == xp_max)
        fltr_idx = int(len(fltr) / 2.)
        amplitude_idx = fltr[fltr_idx][0]

        plate.add_constant("amplitude_idx", amplitude_idx)

        ##################################
        # Add output arrays
        ##################################
        plate.add_output_arrays(['sigma00'])

        return [plate, wall]

    def configure_scheme(self):
        self.scheme.configure(h=self.h)
        dt = self.dt
        tf = self.tf
        self.scheme.configure_solver(dt=dt, tf=tf, pfreq=500)

        scheme = self.scheme
        scheme.configure(edac_nu=self.edac_nu)

    def create_scheme(self):
        solid = SolidsScheme(solids=['plate'],
                             boundaries=['wall'],
                             dim=2,
                             pb=self.pb,
                             edac_nu=self.edac_nu,
                             u_max=self.u_max,
                             mach_no=self.mach_no,
                             h=self.h,
                             hdx=self.hdx)

        s = SchemeChooser(default='solid', solid=solid)
        return s

    def _make_accel_eval(self, equations, pa_arrays):
        if self.seval is None:
            kernel = self.scheme.scheme.kernel(dim=self.dim)
            seval = SPHEvaluator(arrays=pa_arrays,
                                 equations=equations,
                                 dim=self.dim,
                                 kernel=kernel)
            self.seval = seval
            return self.seval
        else:
            self.seval.update()
            return self.seval
        return seval

    def pre_step(self, solver):
        if self.options.pst in ['sun2019', 'ipst']:
            if solver.count % 1 == 0:
                t = solver.t
                dt = solver.dt

                arrays = self.particles
                a_eval = self._make_accel_eval(self.boundary_equations, arrays)

                # When
                a_eval.evaluate(t, dt)

    def post_process(self, fname):
        from pysph.solver.utils import iter_output
        from pysph.solver.utils import get_files

        files = get_files(fname)
        t, amplitude = [], []
        for sd, array in iter_output(files, 'plate'):
            _t = sd['t']
            t.append(_t)
            amplitude.append(array.y[array.amplitude_idx[0]])

        import matplotlib
        import os
        matplotlib.use('Agg')

        from matplotlib import pyplot as plt

        if "info" in fname:
            res = os.path.join(os.path.dirname(fname), "results.npz")
        else:
            res = os.path.join(fname, "results.npz")

        np.savez(res, t=t, amplitude=amplitude)

        # gtvf data
        # path = os.path.abspath(__file__)
        # directory = os.path.dirname(path)

        # data = np.loadtxt(os.path.join(directory, 'oscillating_plate.csv'), delimiter=',')
        # t_gtvf, amplitude_gtvf = data[:, 0], data[:, 1]

        # plt.clf()

        # plt.plot(t_gtvf, amplitude_gtvf, "s-", label='GTVF Paper')
        # print(t)
        # print(amplitude)
        plt.plot(t, amplitude, "-", label='Simulated')

        plt.xlabel('t')
        plt.ylabel('Amplitude')
        plt.legend()
        fig = os.path.join(os.path.dirname(fname), "amplitude.png")
        plt.savefig(fig, dpi=300)


if __name__ == '__main__':
    app = OscillatingPlate()
    app.run()
    app.post_process(app.info_filename)
    # app.create_rings_geometry()
