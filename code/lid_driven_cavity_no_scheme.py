"""
Lid driven cavity




python lid_driven_cavity.py --openmp --integrator gtvf --internal-flow --pst ipst --ipst-interval 1 --re 100 --set-solid-vel-project-uhat --set-uhat-solid-vel-to-u --no-vol-evol-solid --no-edac-solid -d lid_driven_cavity_etvf_integrator_gtvf_pst_ipst_interval_1_re_100_output --detailed-output



1.sun2019
2.ipst
3.tvf


"""
import numpy
import numpy as np

from pysph.examples import cavity as LDC
from pysph.sph.equation import Equation, Group
from pysph.sph.basic_equations import SummationDensity
from pysph.tools.sph_evaluator import SPHEvaluator
from pysph.examples.solid_mech.impact import add_properties

from fluids import ETVFScheme
from solid_mech import ComputeAuHatETVF
from boundary_particles import (ComputeNormals, SmoothNormals,
                                IdentifyBoundaryParticleCosAngleEDAC)
from boundary_particles import (add_boundary_identification_properties)


class MoveParticles(Equation):
    def __init__(self, dest, sources):
        self.conv = -1
        super().__init__(dest, sources)

    def post_loop(self, d_idx, d_x, d_y, d_auhat, d_avhat, dt):
        fac = dt * dt
        d_x[d_idx] += fac * d_auhat[d_idx]
        d_y[d_idx] += fac * d_avhat[d_idx]

    def reduce(self, dst, t, dt):
        rho_max = numpy.max(dst.rho)
        rho_min = numpy.min(dst.rho)
        if (rho_max - rho_min) / rho_max < 1e-2:
            self.conv = 1
        else:
            self.conv = -1

    def converged(self):
        return self.conv


def initial_packing(pa, dx, domain):
    np.random.seed(3)
    x = pa.x
    dt = 0.25 * dx / (LDC.c0 + 1.0)
    pa.x += np.random.random(len(x)) * dx * 0.3
    pa.y += np.random.random(len(x)) * dx * 0.3
    name = pa.name

    eqs = [
        Group(equations=[SummationDensity(dest=name, sources=[name])]),
        Group(
            equations=[
                ComputeAuHatETVF(dest=name, sources=[name], pb=LDC.p0),
                MoveParticles(dest=name, sources=[name])
            ], iterate=True, min_iterations=1, max_iterations=500,
            update_nnps=True)
    ]
    seval = SPHEvaluator(arrays=[pa], equations=eqs, dim=2,
                         domain_manager=domain)
    seval.evaluate(0.0, dt)
    print("Finished packing.")


class LidDrivenCavity(LDC.LidDrivenCavity):
    def initialize(self):
        self.dim = 2

        self.u_max = LDC.Umax
        self.mach_no = LDC.Umax / LDC.c0
        super().initialize()

    def add_user_options(self, group):
        super().add_user_options(group)
        group.add_argument('--packing', action="store_true", default=False,
                           help="Use packing to initialize particles.")
        group.add_argument(
            "--pb-factor", action="store", type=float, dest="pb_factor",
            default=1.0,
            help="Use fraction of the background pressure (default: 1.0).")

    def create_particles(self):
        [fluid, solid] = super().create_particles()

        # add the requisite properties
        add_properties(fluid, 'arho', 'p0', 'rho0', 'u0', 'v0', 'w0', 'x0',
                       'y0', 'z0', 'ap', 'h_b')
        add_boundary_identification_properties(fluid)

        fluid.h_b[:] = fluid.h[:]

        if 'wdeltap' not in fluid.constants:
            fluid.add_constant('wdeltap', -1.)

        if 'n' not in fluid.constants:
            fluid.add_constant('n', 0.)

        if 'c0_ref' not in fluid.constants:
            fluid.add_constant('c0_ref', LDC.c0)

        # setup the boundary
        add_properties(solid, 'uhat', 'vhat', 'what', 'ughat', 'vghat',
                       'wghat', 'ugns', 'vgns', 'wgns', 'ug', 'vg', 'wg')

        ###############
        # for normals #
        ###############
        solid.add_property('normal', stride=3)
        solid.add_output_arrays(['normal'])
        solid.add_property('normal_tmp', stride=3)

        props = ['m', 'rho', 'h']
        for p in props:
            x = solid.get(p)
            if numpy.all(x < 1e-12):
                msg = f'WARNING: cannot compute normals "{p}" is zero'
                print(msg)

        seval = SPHEvaluator(
            arrays=[solid], equations=[
                Group(equations=[
                    ComputeNormals(dest="solid", sources=["solid"])
                ]),
                Group(
                    equations=[SmoothNormals(dest="solid", sources=["solid"])
                               ]),
            ], dim=self.dim)
        seval.evaluate()
        ####################
        # for normals ends #
        ####################

        if self.options.packing:
            initial_packing(fluid, self.dx, self.create_domain())
            pi = np.pi
            b = -8.0 * pi * pi / self.options.re
            u0, v0, p0 = LDC.exact_solution(U=LDC.U, b=b, t=0.0, x=fluid.x,
                                            y=fluid.y)
            fluid.u[:] = u0
            fluid.v[:] = v0
            fluid.p[:] = p0

        solid.u[:] = 0.
        for i in range(solid.get_number_of_particles()):
            if solid.y[i] > LDC.L:
                if solid.x[i] > 0. and solid.x[i] < LDC.L:
                    solid.u[i] = LDC.Umax

        return [fluid, solid]

    def create_equations(self):
        from pysph.sph.wc.transport_velocity import (SetWallVelocity)

        from pysph.sph.wc.gtvf import (ContinuityEquationGTVF,
                                       MomentumEquationArtificialStress,
                                       MomentumEquationViscosity)

        from fluids import (
            SolidWallPressureBC, ContinuitySolidEquationGTVF,
            ContinuitySolidEquation, ContinuityEquationETVFCorrection,
            ContinuitySolidEquationETVFCorrection,
            MomentumEquationPressureGradient, MomentumEquationTVFDivergence,
            StateEquation, SetWallVelocityUhatNoSlip,
            SetWallVelocityUhatFreeSlip, SetWallVelocityNoSlip,
            SetWallVelocityFreeSlip, MomentumEquationViscosityNoSlip,
            SolidWallNoSlipBCDensityBased, MomentumEquationViscosityETVF,
            MakeAuhatZero, SavePositionsIPSTBeforeMoving, AdjustPositionIPST,
            CheckUniformityIPSTFluidInternalFlow, ComputeAuhatETVFIPST,
            ResetParticlePositionsIPST)
        from solid_mech import (ComputeAuHatETVFSun2019)

        equations = [
            Group(
                equations=[
                    StateEquation(dest='fluid', sources=None, p0=LDC.p0,
                                  rho0=LDC.rho0),
                ], ),
            Group(
                equations=[
                    SetWallVelocityUhatFreeSlip(dest='solid',
                                                sources=['fluid']),
                    SolidWallPressureBC(dest='solid', sources=['fluid']),
                ], ),
            Group(
                equations=[
                    # SetWallVelocityNoSlip(dest='solid', sources=['fluid']),
                    SetWallVelocityFreeSlip(dest='solid', sources=['fluid']),
                ], ),
            Group(
                equations=[
                    SetWallVelocityNoSlip(dest='solid', sources=['fluid']),
                    # SetWallVelocity(dest='solid', sources=['fluid']),
                ], ),
            Group(equations=[
                ContinuityEquationGTVF(dest='fluid', sources=['fluid']),
                ContinuitySolidEquationGTVF(dest='fluid', sources=['solid']),
                ContinuityEquationETVFCorrection(dest='fluid',
                                                 sources=['fluid']),
                ContinuitySolidEquationETVFCorrection(dest='fluid',
                                                      sources=['solid']),
                MomentumEquationPressureGradient(dest='fluid',
                                                 sources=['fluid', 'solid']),
                MomentumEquationArtificialStress(
                    dest='fluid', sources=['fluid'], dim=self.dim),
                MomentumEquationTVFDivergence(dest='fluid', sources=['fluid']),
                # ComputeAuHatETVF(dest='fluid', sources=[
                #     'fluid', 'solid'], pb=LDC.p0),
                # ComputeAuHatETVFSun2019(dest='fluid',
                #                         sources=['fluid', 'solid'],
                #                         mach_no=self.mach_no,
                #                         u_max=self.u_max),
                MomentumEquationViscosity(dest='fluid', sources=['fluid'],
                                          nu=self.nu),
                # MomentumEquationViscosityETVF(dest='fluid',
                #                               sources=['fluid'],
                #                               nu=self.nu),
                SolidWallNoSlipBCDensityBased(dest='fluid', sources=['solid'],
                                              nu=self.nu),
                # MomentumEquationViscosityNoSlip(dest='fluid',
                #                                 sources=['solid'],
                #                                 nu=self.nu),
            ]),
        ]

        g5 = []
        g6 = []
        g7 = []
        g8 = []

        # make auhat zero before computation of ipst force
        eqns = []
        for fluid in self.fluids:
            eqns.append(MakeAuhatZero(dest=fluid, sources=None))

        stage2.append(Group(eqns))

        for fluid in self.fluids:
            g5.append(SavePositionsIPSTBeforeMoving(dest=fluid, sources=None))

            # these two has to be in the iterative group and the nnps has to
            # be updated
            # ---------------------------------------
            g6.append(
                AdjustPositionIPST(dest=fluid, sources=all, u_max=self.u_max))

            g7.append(
                CheckUniformityIPSTFluidInternalFlow(
                    dest=fluid, sources=all, debug=self.debug,
                    tolerance=self.ipst_tolerance))
            # ---------------------------------------

            g8.append(ComputeAuhatETVFIPST(dest=fluid, sources=None))
            g8.append(ResetParticlePositionsIPST(dest=fluid, sources=None))

        stage2.append(Group(g5, condition=self.check_ipst_time))

        # this is the iterative group
        stage2.append(
            Group(equations=[Group(equations=g6),
                             Group(equations=g7)], iterate=True,
                  max_iterations=self.ipst_max_iterations,
                  condition=self.check_ipst_time))

        stage2.append(Group(g8, condition=self.check_ipst_time))

        return equations

    def create_solver(self):
        from pysph.sph.integrator import PECIntegrator
        from pysph.solver.solver import Solver
        from pysph.base.kernels import (CubicSpline, WendlandQuintic,
                                        QuinticSpline)
        from fluids import PECStep

        kernel = QuinticSpline(dim=self.dim)

        integrator = PECIntegrator(fluid=PECStep())

        solver = Solver(kernel=kernel, dim=self.dim, integrator=integrator,
                        dt=self.dt, tf=self.tf)

        return solver


if __name__ == '__main__':
    app = LidDrivenCavity()
    app.run()
    app.post_process(app.info_filename)
