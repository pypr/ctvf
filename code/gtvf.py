"""
GTVF solid equations
###################################

References
----------
"""

from pysph.sph.equation import Equation
from pysph.sph.scheme import Scheme
from textwrap import dedent
from compyle.api import declare
from pysph.sph.scheme import add_bool_argument

from pysph.base.utils import get_particle_array
from pysph.sph.integrator_step import IntegratorStep
import numpy as np
from pysph.base.kernels import QuinticSpline

from pysph.sph.solid_mech.basic import (get_speed_of_sound,
                                        get_bulk_mod, get_shear_modulus)


def get_particle_array_gtvf_elastic_solid(constants=None, **props):
    """Return a particle array for the Standard SPH formulation of
    solids.

    Parameters
    ----------
    constants : dict
        Dictionary of constants

    Other Parameters
    ----------------
    props : dict
        Additional keywords passed are set as the property arrays.

    See Also
    --------
    get_particle_array

    """

    # solids_props = [
    #     'cs', 'e', 'v00', 'v01', 'v02', 'v10', 'v11', 'v12', 'v20', 'v21',
    #     'v22', 'r00', 'r01', 'r02', 'r11', 'r12', 'r22', 's00', 's01', 's02',
    #     's11', 's12', 's22', 'as00', 'as01', 'as02', 'as11', 'as12', 'as22',
    #     's000', 's010', 's020', 's110', 's120', 's220', 'arho', 'au', 'av',
    #     'aw', 'ax', 'ay', 'az', 'ae', 'rho0', 'u0', 'v0', 'w0', 'x0', 'y0',
    #     'z0', 'e0', 'auhat', 'avhat', 'awhat', 'uhat', 'vhat', 'what', 'p0'
    # ]

    solids_props = [
        'r00', 'r01', 'r02', 'r11', 'r12', 'r22',
        'v00', 'v01', 'v02', 'v10', 'v11', 'v12', 'v20', 'v21', 'v22', 's00',
        's01', 's02', 's11', 's12', 's22', 'as00', 'as01', 'as02', 'as11',
        'as12', 'as22', 's000', 's010', 's020', 's110', 's120', 's220', 'arho',
        'au', 'av', 'aw', 'ax', 'ay', 'az', 'ae', 'rho0', 'u0', 'v0', 'w0',
        'x0', 'y0', 'z0', 'e0', 'auhat', 'avhat', 'awhat', 'uhat', 'vhat',
        'what', 'p0'
    ]

    # set wdeltap to -1. Which defaults to no self correction
    consts = {
        'wdeltap': -1.,
        'n': 4,
        'G': 0.0,
        'E': 0.0,
        'nu': 0.0,
        'rho_ref': 1000.0,
        'c0_ref': 0.0,
        'b_mod': 0.,  # bulk modulus
        'p_ref': 0.,  # reference pressure for background force (GTVF)
    }
    if constants:
        consts.update(constants)

    pa = get_particle_array(constants=consts, additional_props=solids_props,
                            **props)

    # set the shear modulus G
    pa.G[0] = get_shear_modulus(pa.E[0], pa.nu[0])

    # set the speed of sound
    # pa.cs = np.ones_like(pa.x) * get_speed_of_sound(pa.E[0], pa.nu[0],
    # pa.rho_ref[0])
    pa.c0_ref[0] = get_speed_of_sound(pa.E[0], pa.nu[0], pa.rho_ref[0])

    # to be used by background pressure
    pa.b_mod[0] = get_bulk_mod(pa.G[0], pa.nu[0])
    pa.p_ref[0] = pa.b_mod[0]

    # default property arrays to save out.
    pa.set_output_arrays([
        'x', 'y', 'z', 'u', 'v', 'w', 'rho', 'm', 'h', 'pid', 'gid', 'tag', 'p'
    ])

    return pa


class ContinuityEquationUhat(Equation):
    def initialize(self, d_idx, d_arho):
        d_arho[d_idx] = 0.0

    def loop(self, d_idx, d_rho, d_arho, d_uhat, d_vhat, d_what,
             s_idx, s_m, s_rho, s_uhat, s_vhat, s_what, DWIJ, VIJ):
        vij = declare('matrix(3)')
        vij[0] = d_uhat[d_idx] - s_uhat[s_idx]
        vij[1] = d_vhat[d_idx] - s_vhat[s_idx]
        vij[2] = d_what[d_idx] - s_what[s_idx]

        vijdotdwij = DWIJ[0] * vij[0] + DWIJ[1] * vij[1] + DWIJ[2] * vij[2]
        # d_arho[d_idx] += s_m[s_idx] * vijdotdwij
        d_arho[d_idx] += d_rho[d_idx] * s_m[s_idx] / s_rho[s_idx] * vijdotdwij


class VelocityGradient2DUhat(Equation):
    def initialize(self, d_idx, d_v00, d_v01, d_v10, d_v11):
        d_v00[d_idx] = 0.0
        d_v01[d_idx] = 0.0
        d_v10[d_idx] = 0.0
        d_v11[d_idx] = 0.0

    def loop(self, d_uhat, d_vhat, d_what, d_idx, s_idx, s_m, s_rho, d_v00,
             d_v01, d_v10, d_v11, s_uhat, s_vhat, s_what, DWIJ):
        vij = declare('matrix(3)')
        vij[0] = d_uhat[d_idx] - s_uhat[s_idx]
        vij[1] = d_vhat[d_idx] - s_vhat[s_idx]
        vij[2] = d_what[d_idx] - s_what[s_idx]

        tmp = s_m[s_idx] / s_rho[s_idx]

        d_v00[d_idx] += tmp * -vij[0] * DWIJ[0]
        d_v01[d_idx] += tmp * -vij[0] * DWIJ[1]

        d_v10[d_idx] += tmp * -vij[1] * DWIJ[0]
        d_v11[d_idx] += tmp * -vij[1] * DWIJ[1]


class VelocityGradient3DUhat(Equation):
    def initialize(self, d_idx, d_v00, d_v01, d_v02, d_v10, d_v11, d_v12,
                   d_v20, d_v21, d_v22):
        d_v00[d_idx] = 0.0
        d_v01[d_idx] = 0.0
        d_v02[d_idx] = 0.0

        d_v10[d_idx] = 0.0
        d_v11[d_idx] = 0.0
        d_v12[d_idx] = 0.0

        d_v20[d_idx] = 0.0
        d_v21[d_idx] = 0.0
        d_v22[d_idx] = 0.0

    def loop(self, d_idx, s_idx, s_m, s_rho, d_v00, d_v01, d_v02, d_v10, d_v11,
             d_v12, d_v20, d_v21, d_v22, d_uhat, d_vhat, d_what, s_uhat,
             s_vhat, s_what, DWIJ, VIJ):
        vij = declare('matrix(3)')
        vij[0] = d_uhat[d_idx] - s_uhat[s_idx]
        vij[1] = d_vhat[d_idx] - s_vhat[s_idx]
        vij[2] = d_what[d_idx] - s_what[s_idx]

        tmp = s_m[s_idx] / s_rho[s_idx]

        d_v00[d_idx] += tmp * -vij[0] * DWIJ[0]
        d_v01[d_idx] += tmp * -vij[0] * DWIJ[1]
        d_v02[d_idx] += tmp * -vij[0] * DWIJ[2]

        d_v10[d_idx] += tmp * -vij[1] * DWIJ[0]
        d_v11[d_idx] += tmp * -vij[1] * DWIJ[1]
        d_v12[d_idx] += tmp * -vij[1] * DWIJ[2]

        d_v20[d_idx] += tmp * -vij[2] * DWIJ[0]
        d_v21[d_idx] += tmp * -vij[2] * DWIJ[1]
        d_v22[d_idx] += tmp * -vij[2] * DWIJ[2]


class GTVFEOS(Equation):
    r""" Compute the pressure using the Isothermal equation of state:

    :math:`p = p_0 + c_0^2(\rho_0 - \rho)`

    """
    def initialize(self, d_idx, d_rho, d_p, d_c0_ref, d_rho_ref):
        d_p[d_idx] = d_c0_ref[0] * d_c0_ref[0] * (d_rho[d_idx] - d_rho_ref[0])

    def post_loop(self, d_idx, d_rho, d_p0, d_p, d_p_ref):
        d_p0[d_idx] = min(10. * abs(d_p[d_idx]), d_p_ref[0])


class ComputeAuHatGTVF(Equation):
    def __init__(self, dest, sources):
        super(ComputeAuHatGTVF, self).__init__(dest, sources)

    def initialize(self, d_idx, d_auhat, d_avhat, d_awhat):
        d_auhat[d_idx] = 0.0
        d_avhat[d_idx] = 0.0
        d_awhat[d_idx] = 0.0

    def loop(self, d_idx, s_idx, d_rho, d_p0, s_rho, s_m, d_p, s_p, d_s00,
             d_s01, d_s02, d_s11, d_s12, d_s22, s_s00, s_s01, s_s02, s_s11,
             s_s12, s_s22, d_au, d_av, d_aw, d_auhat, d_avhat, d_awhat, WIJ,
             SPH_KERNEL, DWIJ, XIJ, RIJ, HIJ):
        dwijhat = declare('matrix(3)')

        rhoa = d_rho[d_idx]

        rhoa21 = 1. / (rhoa * rhoa)

        # add the background pressure acceleration
        tmp = -d_p0[d_idx] * s_m[s_idx] * rhoa21

        SPH_KERNEL.gradient(XIJ, RIJ, 0.5 * HIJ, dwijhat)

        d_auhat[d_idx] += tmp * dwijhat[0]
        d_avhat[d_idx] += tmp * dwijhat[1]
        d_awhat[d_idx] += tmp * dwijhat[2]


class GTVFStep(IntegratorStep):
    def stage1(self, d_idx, d_u, d_v, d_w, d_au, d_av, d_aw, d_uhat, d_vhat,
               d_what, d_auhat, d_avhat, d_awhat, dt):
        dtb2 = 0.5 * dt
        d_u[d_idx] += dtb2 * d_au[d_idx]
        d_v[d_idx] += dtb2 * d_av[d_idx]
        d_w[d_idx] += dtb2 * d_aw[d_idx]

        d_uhat[d_idx] = d_u[d_idx] + dtb2 * d_auhat[d_idx]
        d_vhat[d_idx] = d_v[d_idx] + dtb2 * d_avhat[d_idx]
        d_what[d_idx] = d_w[d_idx] + dtb2 * d_awhat[d_idx]

    def stage2(self, d_idx, d_uhat, d_vhat, d_what, d_x, d_y, d_z, d_rho,
               d_arho, d_s00, d_s01, d_s02, d_s11, d_s12, d_s22, d_as00,
               d_as01, d_as02, d_as11, d_as12, d_as22, dt):
        d_rho[d_idx] += dt * d_arho[d_idx]

        d_x[d_idx] += dt * d_uhat[d_idx]
        d_y[d_idx] += dt * d_vhat[d_idx]
        d_z[d_idx] += dt * d_what[d_idx]

        # update deviatoric stress components
        d_s00[d_idx] = d_s00[d_idx] + dt * d_as00[d_idx]
        d_s01[d_idx] = d_s01[d_idx] + dt * d_as01[d_idx]
        d_s02[d_idx] = d_s02[d_idx] + dt * d_as02[d_idx]
        d_s11[d_idx] = d_s11[d_idx] + dt * d_as11[d_idx]
        d_s12[d_idx] = d_s12[d_idx] + dt * d_as12[d_idx]
        d_s22[d_idx] = d_s22[d_idx] + dt * d_as22[d_idx]

    def stage3(self, d_idx, d_u, d_v, d_w, d_au, d_av, d_aw, dt):
        dtb2 = 0.5 * dt
        d_u[d_idx] += dtb2 * d_au[d_idx]
        d_v[d_idx] += dtb2 * d_av[d_idx]
        d_w[d_idx] += dtb2 * d_aw[d_idx]


class GTVFSolidsScheme(Scheme):
    def __init__(self, solids, boundaries, dim, pb,
                 artificial_vis_alpha=1.0, artificial_vis_beta=0.0,
                 gamma=7.):
        self.solids = solids
        if boundaries is None:
            self.boundaries = []
        else:
            self.boundaries = boundaries

        self.dim = dim

        self.kernel = QuinticSpline
        self.kernel_factor = 2

        self.pb = pb

        self.no_boundaries = len(self.boundaries)

        self.artificial_vis_alpha = artificial_vis_alpha
        self.artificial_vis_beta = artificial_vis_beta

        # attributes for P Sun 2019 PST technique
        self.gamma = gamma

        # boundary conditions
        self.adami_velocity_extrapolate = False
        self.no_slip = False
        self.free_slip = False

        self.solver = None

        self.attributes_changed()

    def add_user_options(self, group):
        group.add_argument("--artificial-vis-alpha", action="store",
                           dest="artificial_vis_alpha", default=1.0,
                           type=float,
                           help="Artificial viscosity coefficients")

        group.add_argument("--artificial-vis-beta", action="store",
                           dest="artificial_vis_beta", default=1.0, type=float,
                           help="Artificial viscosity coefficients, beta")

        add_bool_argument(group, 'adami-velocity-extrapolate',
                          dest='adami_velocity_extrapolate', default=False,
                          help='Use adami velocity extrapolation')

        add_bool_argument(group, 'no-slip', dest='no_slip', default=False,
                          help='No slip bc')

        add_bool_argument(group, 'free-slip', dest='free_slip', default=False,
                          help='Free slip bc')

    def consume_user_options(self, options):
        _vars = [
            'artificial_vis_alpha', 'adami_velocity_extrapolate', 'no_slip',
            'free_slip'
        ]
        data = dict((var, self._smart_getattr(options, var)) for var in _vars)
        self.configure(**data)

    def attributes_changed(self):
        self.kernel = QuinticSpline
        self.kernel_factor = 3

    def get_equations(self):
        # from fluids import (SetWallVelocityFreeSlip, ContinuitySolidEquation,
        #                     ContinuitySolidEquationGTVF,
        #                     ContinuitySolidEquationETVFCorrection)

        from pysph.sph.equation import Group, MultiStageEquations
        from pysph.sph.basic_equations import (ContinuityEquation,
                                               MonaghanArtificialViscosity,
                                               VelocityGradient3D,
                                               VelocityGradient2D)
        from pysph.sph.solid_mech.basic import (IsothermalEOS,
                                                HookesDeviatoricStressRate,
                                                MonaghanArtificialStress)

        from solid_mech import (GTVFSetP0,
                                AdamiBoundaryConditionExtrapolateNoSlip,
                                MomentumEquationSolids)

        stage1 = []
        g1 = []
        all = list(set(self.solids + self.boundaries))

        # ------------------------
        # stage 1 equations starts
        # =================================== #
        # solid velocity extrapolation ends
        # =================================== #
        # tmp = []
        # if self.adami_velocity_extrapolate is True:
        #     if self.no_slip is True:
        #         if len(self.boundaries) > 0:
        #             for boundary in self.boundaries:
        #                 tmp.append(
        #                     SetWallVelocity(dest=boundary,
        #                                     sources=self.solids))

        #     if self.free_slip is True:
        #         if len(self.boundary) > 0:
        #             for boundary in self.boundaries:
        #                 tmp.append(
        #                     SetWallVelocityFreeSlip(dest=boundary,
        #                                             sources=self.solids))
        #     stage1.append(Group(equations=tmp))
        # =================================== #
        # solid velocity extrapolation ends
        # =================================== #

        for solid in self.solids:
            g1.append(ContinuityEquationUhat(dest=solid, sources=all))

            if self.dim == 2:
                g1.append(VelocityGradient2DUhat(dest=solid, sources=all))
            elif self.dim == 3:
                g1.append(VelocityGradient3DUhat(dest=solid, sources=all))

        stage1.append(Group(equations=g1))

        # --------------------
        # solid equations
        # --------------------
        g2 = []
        for solid in self.solids:
            g2.append(HookesDeviatoricStressRate(dest=solid, sources=None))

        stage1.append(Group(equations=g2))

        # ------------------------
        # stage 2 equations starts
        # ------------------------

        stage2 = []
        g1 = []
        g2 = []
        g3 = []
        g4 = []

        for solid in self.solids:
            g2.append(GTVFEOS(solid, sources=None))

        for solid in self.solids:
            g2.append(GTVFSetP0(solid, sources=None))

            stage2.append(Group(g2))

        # -------------------
        # boundary conditions
        # -------------------
        for boundary in self.boundaries:
            g3.append(
                AdamiBoundaryConditionExtrapolateNoSlip(
                    dest=boundary, sources=self.solids))

        if len(g3) > 0:
            stage2.append(Group(g3))

        # -------------------
        # solve momentum equation for solid
        # -------------------
        g4 = []
        for solid in self.solids:
            if self.artificial_vis_alpha > 0. or self.artificial_vis_beta > 0.:
                g4.append(
                    MonaghanArtificialViscosity(
                        dest=solid, sources=all,
                        alpha=self.artificial_vis_alpha,
                        beta=self.artificial_vis_beta))

            g4.append(MomentumEquationSolids(dest=solid, sources=all))

            g4.append(
                ComputeAuHatGTVF(dest=solid,
                                 sources=[solid] + self.boundaries))

        stage2.append(Group(g4))

        return MultiStageEquations([stage1, stage2])

    def configure_solver(self, kernel=None, integrator_cls=None,
                         extra_steppers=None, **kw):
        """TODO: Fix the integrator of the boundary. If it is solve_tau then solve for
        deviatoric stress or else no integrator has to be used
        """
        from solid_mech import (SolidMechStep)
        kernel = self.kernel(dim=self.dim)

        steppers = {}
        if extra_steppers is not None:
            steppers.update(extra_steppers)

        from pysph.sph.wc.gtvf import GTVFIntegrator

        cls = integrator_cls if integrator_cls is not None else GTVFIntegrator

        step_cls = SolidMechStep

        for name in self.solids:
            if name not in steppers:
                steppers[name] = step_cls()

        integrator = cls(**steppers)

        from pysph.solver.solver import Solver
        self.solver = Solver(dim=self.dim, integrator=integrator,
                             kernel=kernel, **kw)

    def setup_properties(self, particles, clean=True):
        from pysph.examples.solid_mech.impact import add_properties

        pas = dict([(p.name, p) for p in particles])

        for solid in self.solids:
            # we expect the solid to have Young's modulus, Poisson ration as
            # given
            pa = pas[solid]

            # add the properties that are used by all the schemes
            add_properties(pa, 'cs', 'v00', 'v01', 'v02', 'v10', 'v11', 'v12',
                           'v20', 'v21', 'v22', 's00', 's01', 's02', 's11',
                           's12', 's22', 'as00', 'as01', 'as02', 'as11',
                           'as12', 'as22', 'arho', 'au', 'av', 'aw')

            # this will change
            kernel = self.kernel(dim=2)
            wdeltap = kernel.kernel(rij=pa.spacing0[0], h=pa.h[0])
            pa.add_constant('wdeltap', wdeltap)

            # set the shear modulus G
            G = get_shear_modulus(pa.E[0], pa.nu[0])
            pa.add_constant('G', G)

            # set the speed of sound
            cs = np.ones_like(pa.x) * get_speed_of_sound(
                pa.E[0], pa.nu[0], pa.rho_ref[0])
            pa.cs[:] = cs[:]

            c0_ref = get_speed_of_sound(pa.E[0], pa.nu[0], pa.rho_ref[0])
            pa.add_constant('c0_ref', c0_ref)

            # auhat properties are needed for gtvf, etvf but not for gray. But
            # for the compatability with the integrator we will add
            add_properties(pa, 'auhat', 'avhat', 'awhat', 'uhat', 'vhat',
                           'what', 'div_r')

            add_properties(pa, 'sigma00', 'sigma01', 'sigma02', 'sigma11',
                           'sigma12', 'sigma22')

            # output arrays
            pa.add_output_arrays(['sigma00', 'sigma01', 'sigma11'])

            # now add properties specific to the scheme and PST
            add_properties(pa, 'p0')

            if 'p_ref' not in pa.constants:
                pa.add_constant('p_ref', 0.)

            if 'b_mod' not in pa.constants:
                pa.add_constant('b_mod', 0.)

            pa.b_mod[0] = get_bulk_mod(pa.G[0], pa.nu[0])
            pa.p_ref[0] = pa.b_mod[0]

            pa.add_output_arrays(['p'])

            # for splash particles
            add_properties(pa, 'rho_splash')
            pa.add_output_arrays(['rho_splash'])

        for boundary in self.boundaries:
            pa = pas[boundary]
            pa.add_property('wij')

            # for adami boundary condition

            add_properties(pa, 'ug', 'vg', 'wg', 'uf', 'vf', 'wf', 's00',
                           's01', 's02', 's11', 's12', 's22', 'cs', 'uhat',
                           'vhat', 'what')

            cs = np.ones_like(pa.x) * get_speed_of_sound(
                pa.E[0], pa.nu[0], pa.rho_ref[0])
            pa.cs[:] = cs[:]

            add_properties(pa, 'uhat', 'vhat', 'what')

    def get_solver(self):
        return self.solver
