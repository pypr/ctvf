import os
import numpy as np
import matplotlib.pyplot as plt
# PySPH imports
from pysph.base.utils import get_particle_array
from pysph.solver.application import Application

from pysph.sph.scheme import TVFScheme, SchemeChooser
from pysph.sph.wc.edac import EDACScheme

from pysph.base.kernels import QuinticSpline

# PySPH imports
from pysph.solver.application import Application
from pysph.solver.solver import Solver

from pysph.sph.equation import Group, MultiStageEquations
from pysph.tools.sph_evaluator import SPHEvaluator
from pysph.base.utils import (get_particle_array_tvf_fluid,
                              get_particle_array_tvf_solid)
from pysph.solver.utils import (load)

from pysph.base.kernels import QuinticSpline
from pysph.solver.output import dump

# for normals
from pysph.sph.isph.wall_normal import ComputeNormals, SmoothNormals
from boundary_particles import (get_boundary_identification_etvf_equations,
                                add_boundary_identification_properties)
from pysph.tools.geometry import get_2d_tank, get_2d_block
from cycler import cycler
from matplotlib import rc, patches, colors
from matplotlib.collections import PatchCollection

rc('font', **{'family': 'Helvetica', 'size': 12})
rc('legend', fontsize='medium')
rc('axes', grid=False, linewidth=1.2)
rc('axes.grid', which='both', axis='both')
# rc('axes.formatter', limits=(1, 2), use_mathtext=True, min_exponent=1)
rc('grid', linewidth=0.5, linestyle='--')
rc('xtick', direction='in', top=True)
rc('ytick', direction='in', right=True)
rc('savefig', format='pdf', bbox='tight', pad_inches=0.05,
   transparent=False, dpi=300)
rc('lines', linewidth=1.5)
rc('axes', prop_cycle=(
    cycler('color', ['tab:blue', 'tab:green', 'tab:red',
                     'tab:orange', 'm', 'tab:purple',
                     'tab:pink', 'tab:gray']) +
    cycler('linestyle', ['-.', '--', '-', ':',
                         (0, (3, 1, 1, 1)), (0, (3, 1, 1, 1, 1, 1)),
                         (0, (3, 2, 1, 1)), (0, (3, 2, 2, 1, 1, 1)),
                         ])
))


def get_sin_geometry(dx, L):
    dx = dx
    x, y = get_2d_block(dx, L, L, center=[-L/2., L/2.])

    sin_indices = []
    for i in range(len(x)):
        if y[i] < L / 2. + np.sin(np.pi * x[i]):
            sin_indices.append(i)

    return x[sin_indices], y[sin_indices]


def get_circle_geometry(diameter=1, spacing=0.05, center=None):
    dx = spacing
    x = [0.0]
    y = [0.0]
    r = spacing
    nt = 0
    while r < diameter / 2:
        nnew = int(np.pi * r**2 / dx**2 + 0.5)
        tomake = nnew - nt
        theta = np.linspace(0., 2. * np.pi, tomake + 1)
        for t in theta[:-1]:
            x.append(r * np.cos(t))
            y.append(r * np.sin(t))
        nt = nnew
        r = r + dx
    x = np.array(x)
    y = np.array(y)
    x, y = (t.ravel() for t in (x, y))
    if center is None:
        return x, y
    else:
        return x + center[0], y + center[1]


def remove_particles_inside(xf, yf):
    indices = []

    for i in range(len(xf)):
        r = np.sqrt(xf[i]**2. + yf[i]**2.)
        if r < 1.:
            indices.append(i)

    return np.delete(xf, indices), np.delete(yf, indices)


def remove_particles_inside_2(xf, yf):
    indices = []
    for i in range(len(xf)):
        if yf[i] < 0.:
            indices.append(i)

    xf, yf = np.delete(xf, indices), np.delete(yf, indices)
    return xf, yf


class TestBoundaryParticlesWithCosAngle():
    def _make_accel_eval(self, equations):
        kernel = QuinticSpline(dim=self.dim)
        seval = SPHEvaluator(arrays=self.pa_arrays, equations=equations,
                             dim=self.dim, kernel=kernel)
        return seval

    def dump(self, filename):
        os.makedirs(self.folder_name, exist_ok=True)
        dump(os.path.join(self.folder_name, filename), self.pa_arrays,
             dict(t=0, dt=0.1, count=0), detailed_output=True, only_real=False)

    def create_equations_single_fluid(self, fluid_name):
        # Given
        # pa = self.pa
        eqs = get_boundary_identification_etvf_equations(
            destinations=["fluid"], sources=["fluid"],
            boundaries=None)
        return eqs

    def create_equations_single_fluid_with_boundary(self, fluid_name,
                                                    boundary_name):

        eqs = get_boundary_identification_etvf_equations(
            destinations=["fluid"], sources=["fluid", "boundary"],
            boundaries=["boundary"])

        return eqs

    def test_dam_break_geometry(self):
        self.folder_name = "free_surface_identification_demonstration_output"
        self.dim = 2
        dx = 0.03
        self.fac = dx / 2
        self.kernel_factor = 3
        rho = 1000.
        m = rho * dx**2.
        h = 1.0 * dx
        self.h = h
        # load x, y from file
        path = os.path.abspath(__file__)
        directory = os.path.dirname(path)

        # data = np.loadtxt(os.path.join(directory, 'oscillating_plate.csv'), delimiter=',')
        filepath_tmp = os.path.join(directory, 'free_surface_data')
        filepath = os.path.join(filepath_tmp, 'dam_break_2d_00000.hdf5')

        data = load(filepath)
        arrays = data['arrays']
        fluid = arrays['fluid']
        boundary = arrays['boundary']
        add_boundary_identification_properties(boundary)

        self.pa_arrays = [fluid, boundary]

        # dump the particle array
        os.makedirs(directory + "/" + self.folder_name, exist_ok=True)
        self.dump("test_dam_break_geometry_initial_0")

        a_eval = self._make_accel_eval(
            self.create_equations_single_fluid_with_boundary(
                fluid.name, boundary.name))

        # When
        a_eval.evaluate(0.0, 0.1)

        x = []
        y = []
        normal_x = []
        normal_y = []
        indices = []
        normal_norm = fluid.normal_norm
        for i in range(len(fluid.x)):
            if normal_norm[i] > 1e-12:
                indices.append(i)
                x.append(fluid.x[i])
                y.append(fluid.y[i])

        for i in indices:
            normal_x.append(fluid.normal[3*i])
            normal_y.append(fluid.normal[3*i+1])

        fig, ax = plt.subplots()
        plt.scatter(boundary.x, boundary.y, s=1. * np.ones(len(boundary.x)), c=10 * np.ones(len(boundary.x)))
        plt.scatter(fluid.x, fluid.y, s=1. * np.ones(len(fluid.x)))
        plt.quiver(x, y, normal_x, normal_y, 250. * np.ones(len(x)), scale=40)
        ax.set_aspect('equal', 'box')

        path = os.path.join(directory, self.folder_name)
        path = path + '/normals_dam_break_initial.png'
        plt.savefig(path, dpi=300)
        # plt.show()

        # When
        x = []
        y = []
        indices = []
        is_boundary = fluid.is_boundary
        for i in range(len(fluid.x)):
            if is_boundary[i] == 1:
                indices.append(i)
                x.append(fluid.x[i])
                y.append(fluid.y[i])

        fig, ax = plt.subplots()
        plt.scatter(boundary.x, boundary.y, s=1. * np.ones(len(boundary.x)), c=10 * np.ones(len(boundary.x)))
        plt.scatter(fluid.x, fluid.y, s=1. * np.ones(len(fluid.x)))
        plt.scatter(x, y, s=1. * np.ones(len(x)))
        ax.set_aspect('equal', 'box')

        path = os.path.join(directory, self.folder_name)
        path = path + '/boundary_particles_initial.png'
        plt.savefig(path, dpi=300)
        # plt.show()

        # dump the particle array after evaluation
        self.dump("test_dam_break_geometry_initial_1")

    def test_dam_break_geometry_later(self):
        self.folder_name = "free_surface_identification_demonstration_output"
        self.dim = 2
        dx = 0.03
        self.fac = dx / 2
        self.kernel_factor = 3
        rho = 1000.
        m = rho * dx**2.
        h = 1.0 * dx
        self.h = h
        # load x, y from file
        path = os.path.abspath(__file__)
        directory = os.path.dirname(path)

        # data = np.loadtxt(os.path.join(directory, 'oscillating_plate.csv'), delimiter=',')
        filepath_tmp = os.path.join(directory, 'free_surface_data')
        filepath = os.path.join(filepath_tmp, 'dam_break_2d_16200.hdf5')

        data = load(filepath)
        arrays = data['arrays']
        fluid = arrays['fluid']
        boundary = arrays['boundary']
        add_boundary_identification_properties(boundary)

        self.pa_arrays = [fluid, boundary]

        # dump the particle array
        os.makedirs(directory + "/" + self.folder_name, exist_ok=True)
        self.dump("test_dam_break_geometry_final_0")

        a_eval = self._make_accel_eval(
            self.create_equations_single_fluid_with_boundary(
                fluid.name, boundary.name))

        # When
        a_eval.evaluate(0.0, 0.1)

        x = []
        y = []
        normal_x = []
        normal_y = []
        indices = []
        normal_norm = fluid.normal_norm
        for i in range(len(fluid.x)):
            if normal_norm[i] > 1e-12:
                indices.append(i)
                x.append(fluid.x[i])
                y.append(fluid.y[i])

        for i in indices:
            normal_x.append(fluid.normal[3*i])
            normal_y.append(fluid.normal[3*i+1])

        # ================
        # normals figure
        # ================
        fig, ax = plt.subplots()
        plt.scatter(boundary.x, boundary.y, s=1. * np.ones(len(boundary.x)), c=10 * np.ones(len(boundary.x)))
        plt.scatter(fluid.x, fluid.y, s=1. * np.ones(len(fluid.x)))
        plt.quiver(x, y, normal_x, normal_y, 250. * np.ones(len(x)), scale=40)
        ax.set_aspect('equal', 'box')

        path = os.path.join(directory, self.folder_name)
        path = path + '/normals_dam_break_final.png'
        plt.savefig(path, dpi=300)
        # plt.show()
        plt.clf()
        plt.close()
        # ================
        # normals figure
        # ================

        # ================
        # Zoomed in figure
        # ================
        x = []
        y = []
        x_b = []
        y_b = []
        left_x = 2.17
        right_x = 3.5
        for i in range(len(fluid.x)):
            if fluid.x[i] > left_x and fluid.x[i] < right_x:
                if fluid.y[i] < 2.:
                    indices.append(i)
                    x.append(fluid.x[i])
                    y.append(fluid.y[i])
        for i in range(len(boundary.x)):
            if boundary.x[i] > left_x and boundary.x[i] < right_x:
                x_b.append(boundary.x[i])
                y_b.append(boundary.y[i])

        fig, ax = plt.subplots()
        # plt.scatter(boundary.x, boundary.y, s=1. * np.ones(len(boundary.x)), c=10 * np.ones(len(boundary.x)))
        plt.scatter(x, y, s=1. * np.ones(len(x)))
        plt.scatter(x_b, y_b, s=1. * np.ones(len(x_b)))

        x = []
        y = []
        normal_x = []
        normal_y = []
        indices = []
        normal_norm = fluid.normal_norm
        for i in range(len(fluid.x)):
            if fluid.x[i] > left_x and fluid.x[i] < right_x:
                if fluid.y[i] < 2.:
                    if normal_norm[i] > 1e-12:
                        indices.append(i)
                        x.append(fluid.x[i])
                        y.append(fluid.y[i])

        for i in indices:
            normal_x.append(fluid.normal[3*i])
            normal_y.append(fluid.normal[3*i+1])

        plt.quiver(x, y, normal_x, normal_y, 250. * np.ones(len(x)), scale=40)
        ax.set_aspect('equal', 'box')

        path = os.path.join(directory, self.folder_name)
        path = path + '/normals_dam_break_final_zoomed.png'
        plt.savefig(path, dpi=300)
        # plt.show()
        plt.clf()
        plt.close()
        # ================
        # Zoomed in figure
        # ================

        # When
        x = []
        y = []
        indices = []
        is_boundary = fluid.is_boundary
        for i in range(len(fluid.x)):
            if is_boundary[i] == 1:
                indices.append(i)
                x.append(fluid.x[i])
                y.append(fluid.y[i])

        fig, ax = plt.subplots()
        plt.scatter(boundary.x, boundary.y, s=1. * np.ones(len(boundary.x)),
                    c="orange")
        plt.scatter(fluid.x, fluid.y, s=1. * np.ones(len(fluid.x)),
                    c="blue")
        plt.scatter(x, y, s=1. * np.ones(len(x)), c="red")
        ax.set_aspect('equal', 'box')

        path = os.path.join(directory, self.folder_name)
        path = path + '/boundary_particles_final.png'
        plt.savefig(path, dpi=300)
        # plt.show()
        plt.clf()
        plt.close()

        # ================
        # Zoomed in figure
        # ================
        x = []
        y = []
        x_b = []
        y_b = []
        for i in range(len(fluid.x)):
            if fluid.x[i] > left_x and fluid.x[i] < right_x:
                if fluid.y[i] < 2.:
                    indices.append(i)
                    x.append(fluid.x[i])
                    y.append(fluid.y[i])
        for i in range(len(boundary.x)):
            if boundary.x[i] > left_x and boundary.x[i] < right_x:
                x_b.append(boundary.x[i])
                y_b.append(boundary.y[i])

        fig, ax = plt.subplots()
        # plt.scatter(boundary.x, boundary.y, s=1. * np.ones(len(boundary.x)), c=10 * np.ones(len(boundary.x)))
        plt.scatter(x, y, s=1. * np.ones(len(x)), c="blue")
        plt.scatter(x_b, y_b, s=1. * np.ones(len(x_b)),
                    c="orange")

        # When
        x = []
        y = []
        indices = []
        is_boundary = fluid.is_boundary
        for i in range(len(fluid.x)):
            if fluid.x[i] > left_x and fluid.x[i] < right_x:
                if fluid.y[i] < 2.:
                    if is_boundary[i] == 1:
                        indices.append(i)
                        x.append(fluid.x[i])
                        y.append(fluid.y[i])

        plt.scatter(x, y, s=1. * np.ones(len(x)), c="red")

        ax.set_aspect('equal', 'box')

        path = os.path.join(directory, self.folder_name)
        path = path + '/boundary_particles_final_zoomed.png'
        plt.savefig(path, dpi=300)
        # plt.show()
        plt.clf()
        plt.close()
        # ================
        # Zoomed in figure
        # ================

        # dump the particle array after evaluation
        self.dump("test_dam_break_geometry_final_1")

    def test_circle_geometry(self):
        self.folder_name = "free_surface_identification_demonstration_output"
        self.dim = 2
        dx = 0.05
        self.fac = dx / 2
        self.kernel_factor = 3
        rho = 1000.
        m = rho * dx**2.
        h = 1.0 * dx
        self.h = h
        # load x, y from file
        path = os.path.abspath(__file__)
        directory = os.path.dirname(path)

        # data = np.loadtxt(os.path.join(directory, 'oscillating_plate.csv'), delimiter=',')
        filepath_tmp = os.path.join(directory, 'free_surface_data')
        filepath = os.path.join(filepath_tmp, 'dam_break_2d_16200.hdf5')

        data = load(filepath)
        # create circle geometry
        xf, yf = get_circle_geometry(diameter=3, spacing=dx)
        xf, yf = remove_particles_inside(xf, yf)
        # xb, yb = get_2d_block(dx, np.pi, 4. * dx)
        # yb -= max(yb) - min(yf)
        fluid = get_particle_array_tvf_fluid(x=xf, y=yf, m=m, rho=rho,
                                             h=h,
                                             name="fluid")
        add_boundary_identification_properties(fluid)
        # boundary = get_particle_array_tvf_fluid(x=xb, y=yb, m=m, rho=rho,
        #                                         h=h,
        #                                         name="boundary")
        # add_boundary_identification_properties(boundary)

        self.pa_arrays = [fluid]

        # dump the particle array
        os.makedirs(directory + "/" + self.folder_name, exist_ok=True)
        self.dump("test_circle_geometry_final_0")

        a_eval = self._make_accel_eval(
            self.create_equations_single_fluid(fluid.name))

        # When
        a_eval.evaluate(0.0, 0.1)

        x = []
        y = []
        normal_x = []
        normal_y = []
        indices = []
        normal_norm = fluid.normal_norm
        for i in range(len(fluid.x)):
            if normal_norm[i] > 1e-12:
                indices.append(i)
                x.append(fluid.x[i])
                y.append(fluid.y[i])

        for i in indices:
            normal_x.append(fluid.normal[3*i])
            normal_y.append(fluid.normal[3*i+1])

        # ================
        # normals figure
        # ================
        fig, ax = plt.subplots()
        plt.scatter(fluid.x, fluid.y, s=1. * np.ones(len(fluid.x)))
        plt.quiver(x, y, normal_x, normal_y, 250. * np.ones(len(x)), scale=40)
        ax.set_aspect('equal', 'box')

        path = os.path.join(directory, self.folder_name)
        path = path + '/circle_geometry_normals.png'
        plt.savefig(path, dpi=300)
        # plt.show()
        plt.clf()
        plt.close()
        # ================
        # normals figure
        # ================

        # ================
        # Zoomed in figure
        # ================
        x = []
        y = []
        x_b = []
        y_b = []
        left_x = 2.17
        right_x = 3.5
        for i in range(len(fluid.x)):
            if fluid.x[i] > left_x and fluid.x[i] < right_x:
                if fluid.y[i] < 2.:
                    indices.append(i)
                    x.append(fluid.x[i])
                    y.append(fluid.y[i])

        fig, ax = plt.subplots()
        # plt.scatter(boundary.x, boundary.y, s=1. * np.ones(len(boundary.x)), c=10 * np.ones(len(boundary.x)))
        plt.scatter(x, y, s=1. * np.ones(len(x)))
        plt.scatter(x_b, y_b, s=1. * np.ones(len(x_b)))

        x = []
        y = []
        normal_x = []
        normal_y = []
        indices = []
        normal_norm = fluid.normal_norm
        for i in range(len(fluid.x)):
            if fluid.x[i] > left_x and fluid.x[i] < right_x:
                if fluid.y[i] < 2.:
                    if normal_norm[i] > 1e-12:
                        indices.append(i)
                        x.append(fluid.x[i])
                        y.append(fluid.y[i])

        for i in indices:
            normal_x.append(fluid.normal[3*i])
            normal_y.append(fluid.normal[3*i+1])

        plt.quiver(x, y, normal_x, normal_y, 250. * np.ones(len(x)), scale=40)
        ax.set_aspect('equal', 'box')

        path = os.path.join(directory, self.folder_name)
        path = path + '/circle_geometry_normals_zoomed.png'
        plt.savefig(path, dpi=300)
        # plt.show()
        plt.clf()
        plt.close()
        # ================
        # Zoomed in figure
        # ================

        # When
        x = []
        y = []
        indices = []
        is_boundary = fluid.is_boundary
        for i in range(len(fluid.x)):
            if is_boundary[i] == 1:
                indices.append(i)
                x.append(fluid.x[i])
                y.append(fluid.y[i])

        fig, ax = plt.subplots()
        plt.scatter(fluid.x, fluid.y, s=1. * np.ones(len(fluid.x)),
                    c="blue")
        plt.scatter(x, y, s=1. * np.ones(len(x)), c="red")
        ax.set_aspect('equal', 'box')

        path = os.path.join(directory, self.folder_name)
        path = path + '/circle_geometry.png'
        plt.savefig(path, dpi=300)
        # plt.show()
        plt.clf()
        plt.close()

        # ================
        # Zoomed in figure
        # ================
        x = []
        y = []
        x_b = []
        y_b = []
        for i in range(len(fluid.x)):
            if fluid.x[i] > left_x and fluid.x[i] < right_x:
                if fluid.y[i] < 2.:
                    indices.append(i)
                    x.append(fluid.x[i])
                    y.append(fluid.y[i])

        fig, ax = plt.subplots()
        # plt.scatter(boundary.x, boundary.y, s=1. * np.ones(len(boundary.x)), c=10 * np.ones(len(boundary.x)))
        plt.scatter(x, y, s=1. * np.ones(len(x)), c="blue")
        plt.scatter(x_b, y_b, s=1. * np.ones(len(x_b)),
                    c="orange")

        # When
        x = []
        y = []
        indices = []
        is_boundary = fluid.is_boundary
        for i in range(len(fluid.x)):
            if fluid.x[i] > left_x and fluid.x[i] < right_x:
                if fluid.y[i] < 2.:
                    if is_boundary[i] == 1:
                        indices.append(i)
                        x.append(fluid.x[i])
                        y.append(fluid.y[i])

        plt.scatter(x, y, s=1. * np.ones(len(x)), c="red")

        ax.set_aspect('equal', 'box')

        path = os.path.join(directory, self.folder_name)
        path = path + '/circle_geometry_zoomed.png'
        plt.savefig(path, dpi=300)
        # plt.show()
        plt.clf()
        plt.close()
        # ================
        # Zoomed in figure
        # ================

        # dump the particle array after evaluation
        self.dump("test_circle_geometry_final_1")

    def test_circle_with_boundary_geometry(self):
        self.folder_name = "free_surface_identification_demonstration_output"
        self.dim = 2
        dx = 0.003
        self.fac = dx / 2
        self.kernel_factor = 2
        rho = 1000.
        m = rho * dx**2.
        h = 1.0 * dx
        self.h = h
        # load x, y from file
        path = os.path.abspath(__file__)
        directory = os.path.dirname(path)

        # create circle geometry
        x, y = np.mgrid[-0.1:0.1+1e-4:dx, -0.1:0.1+1e-4:dx]
        x = x.ravel()
        y = y.ravel()

        h = 1.*dx
        rho = 1000

        # remove particles outside the circle
        indices = []
        for i in range(len(x)):
            if np.sqrt(x[i]*x[i] + y[i]*y[i]) - 0.1 > 1e-10:
                indices.append(i)

        pa = get_particle_array(x=x, y=y, m=m, rho=rho, h=h, name="fluid")
        pa.remove_particles(indices)
        xf, yf = pa.x, pa.y

        xf, yf = remove_particles_inside_2(xf, yf)
        xb, yb = get_2d_block(dx, 0.3, 4. * dx)
        yb -= max(yb) - min(yf)
        yb -= dx
        fluid = get_particle_array_tvf_fluid(x=xf, y=yf, m=m, rho=rho,
                                             h=h, name="fluid")
        add_boundary_identification_properties(fluid)
        boundary = get_particle_array_tvf_fluid(x=xb, y=yb, m=m, rho=rho,
                                                h=h,
                                                name="boundary")
        add_boundary_identification_properties(boundary)

        self.pa_arrays = [fluid, boundary]

        # dump the particle array
        os.makedirs(directory + "/" + self.folder_name, exist_ok=True)
        self.dump("test_circle_with_boundary_geometry_final_0")

        a_eval = self._make_accel_eval(
            self.create_equations_single_fluid_with_boundary(
                fluid.name, boundary.name))

        # When
        a_eval.evaluate(0.0, 0.1)

        x = []
        y = []
        normal_x = []
        normal_y = []
        indices = []
        normal_norm = fluid.normal_norm
        for i in range(len(fluid.x)):
            if normal_norm[i] > 1e-12:
                indices.append(i)
                x.append(fluid.x[i])
                y.append(fluid.y[i])

        for i in indices:
            normal_x.append(fluid.normal[3*i])
            normal_y.append(fluid.normal[3*i+1])

        # ================
        # normals figure
        # ================
        fig, ax = plt.subplots()
        plt.scatter(boundary.x, boundary.y, s=1. * np.ones(len(boundary.x)), c=10 * np.ones(len(boundary.x)))
        plt.scatter(fluid.x, fluid.y, s=1. * np.ones(len(fluid.x)))
        plt.quiver(x, y, normal_x, normal_y, 250. * np.ones(len(x)), scale=40)
        ax.set_aspect('equal', 'box')

        path = os.path.join(directory, self.folder_name)
        path = path + '/circle_with_boundary_normals.png'
        plt.savefig(path, dpi=300)
        # plt.show()
        plt.clf()
        plt.close()
        # ================
        # normals figure
        # ================

        # ================
        # Zoomed in figure
        # ================
        x = []
        y = []
        x_b = []
        y_b = []
        left_x = 0.06
        right_x = 0.12
        for i in range(len(fluid.x)):
            if fluid.x[i] > left_x and fluid.x[i] < right_x:
                if fluid.y[i] < 2.:
                    indices.append(i)
                    x.append(fluid.x[i])
                    y.append(fluid.y[i])
        for i in range(len(boundary.x)):
            if boundary.x[i] > left_x and boundary.x[i] < right_x:
                x_b.append(boundary.x[i])
                y_b.append(boundary.y[i])

        fig, ax = plt.subplots()
        # plt.scatter(boundary.x, boundary.y, s=1. * np.ones(len(boundary.x)), c=10 * np.ones(len(boundary.x)))
        plt.scatter(x, y, s=1. * np.ones(len(x)))
        plt.scatter(x_b, y_b, s=1. * np.ones(len(x_b)))

        x = []
        y = []
        normal_x = []
        normal_y = []
        indices = []
        normal_norm = fluid.normal_norm
        for i in range(len(fluid.x)):
            if fluid.x[i] > left_x and fluid.x[i] < right_x:
                if fluid.y[i] < 2.:
                    if normal_norm[i] > 1e-12:
                        indices.append(i)
                        x.append(fluid.x[i])
                        y.append(fluid.y[i])

        for i in indices:
            normal_x.append(fluid.normal[3*i])
            normal_y.append(fluid.normal[3*i+1])

        plt.quiver(x, y, normal_x, normal_y, 250. * np.ones(len(x)), scale=40)
        ax.set_aspect('equal', 'box')

        path = os.path.join(directory, self.folder_name)
        path = path + '/circle_with_boundary_normals_zoomed.png'
        plt.savefig(path, dpi=300)
        # plt.show()
        plt.clf()
        plt.close()
        # ================
        # Zoomed in figure
        # ================

        # When
        x = []
        y = []
        indices = []
        is_boundary = fluid.is_boundary
        for i in range(len(fluid.x)):
            if is_boundary[i] == 1:
                indices.append(i)
                x.append(fluid.x[i])
                y.append(fluid.y[i])

        fig, ax = plt.subplots()
        plt.scatter(boundary.x, boundary.y, s=1. * np.ones(len(boundary.x)),
                    c="orange")
        plt.scatter(fluid.x, fluid.y, s=1. * np.ones(len(fluid.x)),
                    c="blue")
        plt.scatter(x, y, s=1. * np.ones(len(x)), c="red")
        ax.set_aspect('equal', 'box')

        path = os.path.join(directory, self.folder_name)
        path = path + '/circle_with_boundary.png'
        plt.savefig(path, dpi=300)
        # plt.show()
        plt.clf()
        plt.close()

        # ================
        # Zoomed in figure
        # ================
        x = []
        y = []
        x_b = []
        y_b = []
        for i in range(len(fluid.x)):
            if fluid.x[i] > left_x and fluid.x[i] < right_x:
                if fluid.y[i] < 2.:
                    indices.append(i)
                    x.append(fluid.x[i])
                    y.append(fluid.y[i])
        for i in range(len(boundary.x)):
            if boundary.x[i] > left_x and boundary.x[i] < right_x:
                x_b.append(boundary.x[i])
                y_b.append(boundary.y[i])

        fig, ax = plt.subplots()
        # plt.scatter(boundary.x, boundary.y, s=1. * np.ones(len(boundary.x)), c=10 * np.ones(len(boundary.x)))
        plt.scatter(x, y, s=1. * np.ones(len(x)), c="blue")
        plt.scatter(x_b, y_b, s=1. * np.ones(len(x_b)),
                    c="orange")

        # When
        x = []
        y = []
        indices = []
        is_boundary = fluid.is_boundary
        for i in range(len(fluid.x)):
            if fluid.x[i] > left_x and fluid.x[i] < right_x:
                if fluid.y[i] < 2.:
                    if is_boundary[i] == 1:
                        indices.append(i)
                        x.append(fluid.x[i])
                        y.append(fluid.y[i])

        plt.scatter(x, y, s=1. * np.ones(len(x)), c="red")

        ax.set_aspect('equal', 'box')

        path = os.path.join(directory, self.folder_name)
        path = path + '/circle_with_boundary_zoomed.png'
        plt.savefig(path, dpi=300)
        # plt.show()
        plt.clf()
        plt.close()
        # ================
        # Zoomed in figure
        # ================

        # dump the particle array after evaluation
        self.dump("test_circle_with_boundary_geometry_final_1")

    def test_sin_geometry(self):
        self.folder_name = "free_surface_identification_demonstration_output"
        self.dim = 2
        dx = 0.005
        self.fac = dx / 2
        self.kernel_factor = 3
        rho = 1000.
        m = rho * dx**2.
        h = 1.0 * dx
        self.h = h
        # load x, y from file
        path = os.path.abspath(__file__)
        directory = os.path.dirname(path)

        # data = np.loadtxt(os.path.join(directory, 'oscillating_plate.csv'), delimiter=',')
        filepath_tmp = os.path.join(directory, 'free_surface_data')
        filepath = os.path.join(filepath_tmp, 'dam_break_2d_16200.hdf5')

        data = load(filepath)
        # create sin geometry
        xf, yf = get_sin_geometry(dx, np.pi)
        xb, yb = get_2d_block(dx, np.pi, 4. * dx)
        yb -= max(yb) - min(yf)
        fluid = get_particle_array_tvf_fluid(x=xf, y=yf, m=m, rho=rho,
                                             h=h,
                                             name="fluid")
        add_boundary_identification_properties(fluid)
        boundary = get_particle_array_tvf_fluid(x=xb, y=yb, m=m, rho=rho,
                                                h=h,
                                                name="boundary")
        add_boundary_identification_properties(boundary)

        self.pa_arrays = [fluid, boundary]

        # dump the particle array
        os.makedirs(directory + "/" + self.folder_name, exist_ok=True)
        self.dump("test_sin_geometry_final_0")

        a_eval = self._make_accel_eval(
            self.create_equations_single_fluid_with_boundary(
                fluid.name, boundary.name))

        # When
        a_eval.evaluate(0.0, 0.1)

        x = []
        y = []
        normal_x = []
        normal_y = []
        indices = []
        normal_norm = fluid.normal_norm
        for i in range(len(fluid.x)):
            if normal_norm[i] > 1e-12:
                indices.append(i)
                x.append(fluid.x[i])
                y.append(fluid.y[i])

        for i in indices:
            normal_x.append(fluid.normal[3*i])
            normal_y.append(fluid.normal[3*i+1])

        # ================
        # normals figure
        # ================
        fig, ax = plt.subplots()
        plt.scatter(boundary.x, boundary.y, s=1. * np.ones(len(boundary.x)), c=10 * np.ones(len(boundary.x)))
        plt.scatter(fluid.x, fluid.y, s=1. * np.ones(len(fluid.x)))
        plt.quiver(x, y, normal_x, normal_y, 250. * np.ones(len(x)), scale=40)
        ax.set_aspect('equal', 'box')

        path = os.path.join(directory, self.folder_name)
        path = path + '/sin_geometry_normals_final.png'
        plt.savefig(path, dpi=300)
        # plt.show()
        plt.clf()
        plt.close()
        # ================
        # normals figure
        # ================

        # ================
        # Zoomed in figure
        # ================
        x = []
        y = []
        x_b = []
        y_b = []
        left_x = 2.17
        right_x = 3.5
        for i in range(len(fluid.x)):
            if fluid.x[i] > left_x and fluid.x[i] < right_x:
                if fluid.y[i] < 2.:
                    indices.append(i)
                    x.append(fluid.x[i])
                    y.append(fluid.y[i])
        for i in range(len(boundary.x)):
            if boundary.x[i] > left_x and boundary.x[i] < right_x:
                x_b.append(boundary.x[i])
                y_b.append(boundary.y[i])

        fig, ax = plt.subplots()
        # plt.scatter(boundary.x, boundary.y, s=1. * np.ones(len(boundary.x)), c=10 * np.ones(len(boundary.x)))
        plt.scatter(x, y, s=1. * np.ones(len(x)))
        plt.scatter(x_b, y_b, s=1. * np.ones(len(x_b)))

        x = []
        y = []
        normal_x = []
        normal_y = []
        indices = []
        normal_norm = fluid.normal_norm
        for i in range(len(fluid.x)):
            if fluid.x[i] > left_x and fluid.x[i] < right_x:
                if fluid.y[i] < 2.:
                    if normal_norm[i] > 1e-12:
                        indices.append(i)
                        x.append(fluid.x[i])
                        y.append(fluid.y[i])

        for i in indices:
            normal_x.append(fluid.normal[3*i])
            normal_y.append(fluid.normal[3*i+1])

        plt.quiver(x, y, normal_x, normal_y, 250. * np.ones(len(x)), scale=40)
        ax.set_aspect('equal', 'box')

        path = os.path.join(directory, self.folder_name)
        path = path + '/sin_normals_final_zoomed.png'
        plt.savefig(path, dpi=300)
        # plt.show()
        plt.clf()
        plt.close()
        # ================
        # Zoomed in figure
        # ================

        # When
        x = []
        y = []
        indices = []
        is_boundary = fluid.is_boundary
        for i in range(len(fluid.x)):
            if is_boundary[i] == 1:
                indices.append(i)
                x.append(fluid.x[i])
                y.append(fluid.y[i])

        fig, ax = plt.subplots()
        plt.scatter(boundary.x, boundary.y, s=1. * np.ones(len(boundary.x)),
                    c="orange")
        plt.scatter(fluid.x, fluid.y, s=1. * np.ones(len(fluid.x)),
                    c="blue")
        plt.scatter(x, y, s=1. * np.ones(len(x)), c="red")
        ax.set_aspect('equal', 'box')

        path = os.path.join(directory, self.folder_name)
        path = path + '/sin_geometry_boundary_particles_final.png'
        plt.savefig(path, dpi=300)
        # plt.show()
        plt.clf()
        plt.close()

        # ================
        # Zoomed in figure
        # ================
        x = []
        y = []
        x_b = []
        y_b = []
        for i in range(len(fluid.x)):
            if fluid.x[i] > left_x and fluid.x[i] < right_x:
                if fluid.y[i] < 2.:
                    indices.append(i)
                    x.append(fluid.x[i])
                    y.append(fluid.y[i])
        for i in range(len(boundary.x)):
            if boundary.x[i] > left_x and boundary.x[i] < right_x:
                x_b.append(boundary.x[i])
                y_b.append(boundary.y[i])

        fig, ax = plt.subplots()
        # plt.scatter(boundary.x, boundary.y, s=1. * np.ones(len(boundary.x)), c=10 * np.ones(len(boundary.x)))
        plt.scatter(x, y, s=1. * np.ones(len(x)), c="blue")
        plt.scatter(x_b, y_b, s=1. * np.ones(len(x_b)),
                    c="orange")

        # When
        x = []
        y = []
        indices = []
        is_boundary = fluid.is_boundary
        for i in range(len(fluid.x)):
            if fluid.x[i] > left_x and fluid.x[i] < right_x:
                if fluid.y[i] < 2.:
                    if is_boundary[i] == 1:
                        indices.append(i)
                        x.append(fluid.x[i])
                        y.append(fluid.y[i])

        plt.scatter(x, y, s=1. * np.ones(len(x)), c="red")

        ax.set_aspect('equal', 'box')

        path = os.path.join(directory, self.folder_name)
        path = path + '/sin_geometry_boundary_particles_final_zoomed.png'
        plt.savefig(path, dpi=300)
        # plt.show()
        plt.clf()
        plt.close()
        # ================
        # Zoomed in figure
        # ================

        # dump the particle array after evaluation
        self.dump("test_sin_geometry_final_1")


if __name__ == '__main__':
    test = TestBoundaryParticlesWithCosAngle()
    test.test_circle_geometry()
    test.test_circle_with_boundary_geometry()
    # test.test_sin_geometry()
    test.test_dam_break_geometry()
    test.test_dam_break_geometry_later()
