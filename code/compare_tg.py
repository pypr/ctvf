from matplotlib import pyplot as plt
import numpy as np
from os.path import join
import sys


def compare(dirs, labels=None, output=None, show=False):
    if labels is None:
        labels = [str(i) for i in range(len(dirs))]
        print({str(i): d for i, d in enumerate(dirs)})
    all_data = {labels[i]: np.load(join(d, 'results.npz'))
                for i, d in enumerate(dirs)}
    ax = plt.subplot(2, 2, 1)
    for name, data in all_data.items():
        ax.semilogy(data['t'], data['decay'], label=name)
    zero = labels[0]
    ax.semilogy(all_data[zero]['t'], all_data[zero]['decay_ex'], label='exact')
    plt.ylabel('decay')
    #plt.grid()
    plt.legend(fontsize=6)
    ax = plt.subplot(2, 2, 2)
    for name, data in all_data.items():
        plt.plot(data['t'], data['l1'], label=name)
    plt.ylabel('$L_1$')
    plt.grid()
    #plt.legend()
    ax = plt.subplot(2, 2, 3)
    for name, data in all_data.items():
        plt.plot(data['t'], data['linf'], label=name)
    plt.ylabel(r'$L_\infty$')
    plt.grid()
    #plt.legend()
    ax = plt.subplot(2, 2, 4)
    for name, data in all_data.items():
        plt.plot(data['t'], data['p_l1'], label=name)
    plt.ylabel('$L_1$ for $p$')
    plt.grid()
    plt.legend(fontsize=6)
    plt.tight_layout()

    if output is not None:
        plt.savefig(output + '.png', dpi=300)
        plt.savefig(output + '.pdf', bbox_inches='tight', pad_inches=0)

    if show:
        plt.show()


if __name__ == '__main__':
    compare(sys.argv[1:], show=True)
